const url = 'http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/dangnhap';
function checkDangNhapTroCapCovid(tk, mk) {
    var paramsString = `tk=${tk}&mk=${mk}`;
    return fetch(url,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: paramsString
        })
        .then((response) => response.json());
}

module.exports = checkDangNhapTroCapCovid;
