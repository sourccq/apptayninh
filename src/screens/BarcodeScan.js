import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import { StyleSheet, View, Platform, InteractionManager,Vibration,TouchableOpacity, Linking, PermissionsAndroid , Alert,Dimensions} from 'react-native';
import { Container, Header, Title,Content,Icon, Button, Card, CardItem, Text, Body, Form,Item,Input,Label,Right,Left  } from 'native-base';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import { Clipboard } from 'react-native';
import { NavigationActions, Navigation } from 'react-navigation';
import Sound from 'react-native-sound';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
const { width } = Dimensions.get('window')
const searchWidth = deviceWidth*0.6;

// Enable playback in silence mode
Sound.setCategory('Playback');
var whoosh = new Sound('beeps.mp3', Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    console.log('failed to load the sound', error);
    return;
  }
});
  // // loaded successfully
  // console.log('duration in seconds: ' + whoosh.getDuration() + 'number of channels: ' + whoosh.getNumberOfChannels());

  // // Play the sound with an onEnd callback
  // whoosh.play((success) => {
  //   if (success) {
  //     console.log('successfully finished playing');
  //   } else {
  //     console.log('playback failed due to audio decoding errors');
  //   }
  // });
// });



class BarcodeScan extends Component {
  componentDidMount(){
    const params = this.props.navigation.getParam('openQRScan',false);
    if(params==true){
      this.open_QR_Code_Scanner();
      
    }
    const didBlurSubscription = this.props.navigation.addListener(
      'willBlur',
      payload => {
        console.debug('willBlur', payload);
        // this.props.navigation.goBack('Home');
        // const defaultGetStateForAction = Navigation.router.getStateForAction;
        // console.log(defaultGetStateForAction);
        // navigateAction.reset([NavigationActions.navigate({ routeName: 'Home' })], 0);
      }
    );

    const didBlurSubscription2 = this.props.navigation.addListener(
      'didFocus',
      payload => {
        console.debug('didFocus', payload);
        // NavigationActions.reset({
        //   index: 1,
        //   actions: [
        //     NavigationActions.navigate({ routeName: 'Home' })
        //   ]
        // });
      }
    );
    // didBlurSubscription.remove();
    // const { navigation } = this.props;
    // navigation.addListener('willFocus', () => {
    //   InteractionManager.runAfterInteractions(() => this.props.setFocusedScreen(true));
    // });
    // navigation.addListener('willBlur', () => {
    //   InteractionManager.runAfterInteractions(() => this.props.setFocusedScreen(false));
    // });
   
  }
  componentWillUnmount(){
  }
  constructor() {
    super();
    this.state = {
      QR_Code_Value: '',
      Start_Scanner: false,
      DataDetail:[],
      kq: false,
      thongbaokq:'',
      sbn:'',
      clipboardContent: null,
    };
   
  }

  
  openLink_in_browser = () => {

    Linking.openURL(this.state.QR_Code_Value);

  }

  onQR_Code_Scan_Done = (QR_Code) => {
    whoosh.play();
    Vibration.vibrate(200);
    this.setState({ QR_Code_Value: QR_Code });
    this.onSearch(QR_Code);
    this.setState({ sbn: QR_Code });
    this.setState({ Start_Scanner: false });
  }

  open_QR_Code_Scanner=()=> {

    var that = this;

    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA, {
              'title': 'Camera ứng dụng không có quyền',
              'message': 'Ứng dụng cần sử dụng camera, '+
              'cần đồng ý truy cập camera.',
              buttonPositive: 'OK',
            }
          )
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            that.setState({ QR_Code_Value: '' });
            that.setState({ Start_Scanner: true });
          } else {
            Alert.alert(
                'Thông báo',
                'Không có quyền truy cập camera',
                [
                //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
                  {
                    text: 'Thoát',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'OK', onPress: () => console.log('OK Pressed')},
                ],
                {cancelable: false},
              );
          }
        } catch (err) {
          alert("Camera permission err", err);
          console.warn(err);
        }
      }  
      requestCameraPermission();
    } else {
      that.setState({ QR_Code_Value: '' });
      that.setState({ Start_Scanner: true });
    }
  }
  readFromClipboard = async () => {
    //To get the text from clipboard
    const clipboardContent = await Clipboard.getString();   
    this.setState({ clipboardContent }); 
  };
  onsearchbtn(text){
    const { sbn } = this.state;
    this.setState({sbn: text});
    let Value = text.text.length ;
    // console.log('___________________'); console.log('__#_#_#_########__#_#_##_#_');
    // this.readFromClipboard();
    // console.log(this.state.clipboardContent);
    // if(this.state.clipboardContent.length==13){
    //   this.setState({
    //     QR_Code_Value: this.state.clipboardContent
    //   });
    //   this.onSearch(this.state.clipboardContent);
    // } 
    // readFromClipboard = async () => {   
    //   const clipboardContent = await Clipboard.getString();   
    //   // this.setState({ clipboardContent });
    //   console.log('__#_#_#_########__#_#_##_#_');
    //   console.log(clipboardContent); 
    // };
    if(Value==13 || Value==25){
      this.setState({
        QR_Code_Value: text.text
      });
      this.onSearch(text.text);
    }
  }
  onSearch(value) {
        var paramsString = `mahs=${value}`;
        console.log(paramsString);
        url = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/tracuuhs';
        fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: paramsString
        }).then((response) => response.json())
        .then((responseJson) => {
          if (responseJson.length !== 0) {
            console.log(responseJson);
            console.log('___________________________');
            this.setState({
              DataDetail: responseJson,
              kq: true,
            });
            if(this.state.DataDetail[0].ThongTinHoSo != ''){
              this.setState({
                kq: false,
                thongbaokq: this.state.DataDetail[0].ThongTinHoSo,
              })
            }
            console.log('__In ra man hinh_____');
            console.log(this.state.DataDetail);
            console.log(this.state.DataDetail[0].ThongTinHoSo);
          }      
        })
        console.log("________________thông tin link _______________________");
        console.log(paramsString);
        console.log(url);

  }
  
  loadInBrowser = (url) => {
    Linking.openURL(url).catch(err => console.error("Couldn't load page", err));
  };

  render() {
    if (!this.state.Start_Scanner) {
      return (
    

        <Container
        removeClippedSubviews={false}
        style={styles.MainContainer}>
      <Header  searchBar style={styles.header}>
              <Left>
              <Button transparent onPress={() => this.props.navigation.navigate("Home")}>
              <Icon  name="md-home" style={{ color: 'white', fontSize:30,marginLeft:0,marginRight:0}} />
              </Button>
              </Left>
          <Body>
          <Item underline style={{backgroundColor:'#0E4AA3',width:searchWidth}}>
            <Icon name="md-search" style={{color:'white',paddingRight:0,paddingLeft:0, paddingTop:4}}  onPress={() => this.props.navigation.openDrawer()}/>
            <Input 
              autoFocus={false}
              placeholder="Nhập số biên nhận để tra cứu hồ sơ" 
              placeholderTextColor='#B6CAD1' 
              style={{fontSize:14,fontStyle:'italic',fontFamily:'roboto',paddingLeft:6,color: '#FFFFFF',}}
              // keyboardType='numeric'
              value={this.state.sbn}
              selectTextOnFocus={ true }
              removeClippedSubviews={false}
              onChangeText={text => this.onsearchbtn({text})}
              />
            {/* <Icon name="md-qr-scanner" style={{color:'white'}} onPress={() => this.props.navigation.openDrawer()}/> */}
          </Item>
          </Body>
          <Right>

          <Button transparent onPress={this.open_QR_Code_Scanner}>
              <Icon type="MaterialCommunityIcons" name="qrcode-scan" style={{ color: 'white'}} />
              {/* <Icon  name="md-qr-scanner" style={{ color: 'white', fontSize:28,marginRight:0}} /> */}
            </Button>    
            </Right>     
        </Header>
{/* 
          <Text style={{ fontSize: 22, textAlign: 'center' }}>React Native Scan QR Code Example</Text>
          <Text style={styles.QR_text}>
            {this.state.QR_Code_Value ? 'Scanned QR Code: ' + this.state.QR_Code_Value : ''}
          </Text> */}
        {this.state.kq == false ? 
            <Text style={styles.QR_text}>
             {this.state.thongbaokq}
            </Text>
        :
        <Container>
        {/* <Form>
            <Item floatingLabel>
              <Label>Nhập số biên nhận</Label>
              <Input />
            </Item>
          </Form> */}
        <Content padder>
          <Card>
            <CardItem header bordered>
            <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text>Xin chào Ông/Bà:   </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.DataDetail[0].TenKhachHang}</Text>
                   </View>
              </View>
              {/* <Text>Xin chào Ông/Bà: </Text>
              <Right>
              <Text>{this.state.DataDetail[0].TenKhachHang}</Text>
              </Right> */}
            </CardItem>
            <CardItem footer bordered>
            <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text> Mã hồ sơ:    </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.QR_Code_Value}</Text>
                   </View>
              </View>
            {/* <Text>
                Mã hồ sơ: 
                </Text>
                <Right>
              <Text>{this.state.QR_Code_Value}</Text>
              </Right> */}
            </CardItem>
            <CardItem footer bordered>
              <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text>
                                  Lĩnh vực: 
                                  </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.DataDetail[0].TenLinhVuc}</Text>
                   </View>
              </View>
{/* 
                <Right>
              
              </Right> */}
              {/* <Right></Right> */}
            </CardItem>
            <CardItem bordered>
              <Body>

                <Text>
                Thủ tục: {this.state.DataDetail[0].TenLoaiHoSo}
                </Text>
               
              </Body>
            </CardItem>


            <CardItem footer bordered>
            <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text>Cơ quan tiếp nhận:  </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.DataDetail[0].TenDonVi}</Text>
                   </View>
              </View>
{/* 
            <Text>Cơ quan tiếp nhận:  </Text>
                <Right>
              <Text>{this.state.DataDetail[0].TenDonVi}</Text>
              </Right> */}
            </CardItem>
            <CardItem footer bordered>
            <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text>Ngày Tiếp nhận:   </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.DataDetail[0].NgayNhan}</Text>
                   </View>
              </View>

            {/* <Text>
                Ngày Tiếp nhận: 
                </Text>
                <Right>
              <Text>{this.state.DataDetail[0].NgayNhan}</Text>
              </Right> */}
            </CardItem>

            <CardItem footer bordered>
            <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text>Ngày hẹn trả:   </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.DataDetail[0].NgayHenTra}</Text>
                   </View>
              </View>

            {/* <Text>
                Ngày hẹn trả: 
                </Text>
                <Right>
              <Text>{this.state.DataDetail[0].NgayHenTra}</Text>
              </Right> */}
            </CardItem>
            <CardItem footer bordered>
            <View style={{flex:1,flexDirection: 'row' }}>
                  <View style={{flex:1}}>
                  <Text>Trạng thái hồ sơ:    </Text>
                  </View>
                  <View style={{flex:1}}>
                  <Text style={{color:'black'}}>{this.state.DataDetail[0].TenTinhTrang}</Text>
                   </View>
              </View>
            {/* <Text>
                Trạng thái hồ sơ: 
                </Text>
                <Right>
              <Text>{this.state.DataDetail[0].TenTinhTrang}</Text>
              </Right> */}
            </CardItem>
              {this.state.DataDetail[0].KetQua != "" ?
            <CardItem>
            <View style={{flex:1,alignItems: 'center',flex: 1,justifyContent: 'center'}}>
                  <View style={{flex:1}}>
                  <Button success onPress={()=>{this.loadInBrowser(`${this.state.DataDetail[0].KetQua}`)}}>
                      <Text>Kết quả xử lý</Text>
                  </Button>
                  </View>
              </View>
            </CardItem> : null} 
 
          </Card>
        </Content>
      </Container>
  
        }

          {/* {this.state.QR_Code_Value.includes("http") ?
            <TouchableOpacity
              onPress={this.openLink_in_browser}
              style={styles.button}>
              <Text style={{ color: '#FFF', fontSize: 14 }}>Open Link in default Browser</Text>
            </TouchableOpacity> : null
          } */}

          {/* <TouchableOpacity
            onPress={this.open_QR_Code_Scanner}
            style={styles.button}>
            <Text style={{ color: '#FFF', fontSize: 14 }}>
              Open QR Scanner
            </Text>
          </TouchableOpacity> */}

        </Container>
      );
    }
    return (
      <View style={{ flex: 1 }}>

        <CameraKitCameraScreen
          actions={{ leftButtonText: 'Huỷ' }}
          onBottomButtonPressed={(event) => this.setState({ Start_Scanner: false })}
          showFrame={true}
          scanBarcode={true}
          laserColor={'#FF3D00'}
          frameColor={'#00C853'}
          colorForScannerFrame={'black'}
          onReadCode={event =>
            this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
          }
        />
               {/* <View style={{
                              marginBottom: 30,
                              flexDirection: 'row',
                              justifyContent: 'center',
                            }}>
                <Button transparent light onPress={() => this.setState({ Start_Scanner: false })}>
                <Text style={{
                              fontSize: 20,
                            }}>
                Huỷ</Text>
                </Button>
                </View> */}
                {/* <View style={{ backgroundColor: 'rgba(255, 255, 255, 0.8)',width:width ,height:50,justifyContent: 'center', alignItems: 'center'}}>
                <Text style={{fontSize: 20}}>Thoát</Text>
                </View> */}

      </View>
    );
  }
}
export default withNavigation(BarcodeScan);
const styles = StyleSheet.create({

  // MainContainer: {
  //   flex: 1,
  //  // paddingTop: (Platform.OS) === 'ios' ? 20 : 0,

  // },
  header:{
    backgroundColor: "#0E4AA3",
   
    paddingTop:0.1,
    height:50
  },
  QR_text: {
    color: '#000',
    fontSize: 19,
    padding: 8,
    marginTop: 12
  },
  button: {
    backgroundColor: '#2979FF',
    alignItems: 'center',
    padding: 12,
    width: 300,
    marginTop: 14
  },
  
});