import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Text,
    Dimensions,
    ImageBackground,
    Alert
  } from "react-native";
  import { Button,Container,Header,Left,Right,Icon,Item,Title,Body,Content,Picker,Form, Spinner} from 'native-base';
  import AsyncStorage from '@react-native-community/async-storage';


  const { width: deviceWidth } = Dimensions.get('window');
  const { height: deviceHeigh } = Dimensions.get('window');
  const imageWidth = deviceWidth ;
  const imageHeight = deviceHeigh/4;
  const buttonWidth = deviceWidth/3;
  
  
  
  

  export default class egov extends React.Component {


    componentDidMount(){
      this.fetchDonviEgov()
    }

    constructor(props) {
      super(props);
      this.state = {
        uri: undefined,
        arrUrl:[],
        loading:false,

      };
    }

    fetchDonviEgov = async () =>{
      this.setState({loading:true})
      serviceUrl = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/dvegov';
      try {
      await fetch(serviceUrl, {
        method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
      }).then(response => response.json())
      .then(responseData => {
        this.setState({arrUrl:responseData,loading:false}); 
      })
    }catch(error){
      console.log('==================================')
      console.log('KHONG FETCH DUOC DU LIEU' + error)
    }
  }

    _validateInput(){
      if(this.state.uri==null){
        Alert.alert('Vui lòng chọn máy chủ Egov')
        return false
      }else return true
    }
    _saveDataAndNavigate = async () => {
      try {
        await AsyncStorage.setItem('egovUri', this.state.uri);
        this.props.navigation.navigate('egov');
      } catch (error) {
        // Error saving data
      }
    };

    onValueChange(value) {
      this.setState({
        uri: value
      });
    }

    render() {
      if(this.state.arrUrl.length >0){
      return (
        <Container>
      
          <Header hasSegment style={{ backgroundColor: "#0E4AA3", paddingTop:0.1, height:40 }}>
              <Left>  
                <Icon  onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', fontSize:30}} />
              </Left>
              <Body>
                <Title style={{color:'white', alignSelf:'center'}}>Bkav Egov</Title>
              </Body>
              <Right>
              </Right>
          </Header>

          <Content>   
            <ImageBackground
              source={require("../assets/egov-bg.png") }
              style={styles.homeImage}
              resizeMode='cover'>
                <Text style={styles.textOverImg}>
                Hệ thống xử lý Văn Bản Điện Tử và Điều Hành
                </Text>
            </ImageBackground>

          {/* -------------------------BEGIN Picker-------------------- */}
            <Form>
              <Item picker>
                <Picker 
                  renderHeader={backAction =>
                    <Header style={{ backgroundColor: "#0E4AA3" }}>
                      <Left>
                        <Button transparent onPress={backAction}>
                          <Icon name="arrow-back" style={{ color: "#fff" }} />
                        </Button>
                      </Left>
                      <Body style={{ flex: 3 }}>
                        <Title style={{ color: "#fff" }}>Chọn Máy chủ</Title>
                      </Body>
                      <Right />
                    </Header>
                  }
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.uri}
                  onValueChange={this.onValueChange.bind(this)}
                >
                <Picker.Item label='Chọn máy chủ Egov kết nối' value={undefined}/>
                {this.state.arrUrl.map(item => {
                return <Picker.Item label={item.TenDonVi} value={item.EgovUrl} key={item.ID} />
                })}
                
                </Picker>
              </Item>
            </Form>
            {/* -------------------------END Picker-------------------- */}
            
            <View style={{height:60,width:deviceWidth, justifyContent:'center', flexDirection:"row", paddingTop:20}}>
              <Button block style={{width:100}} 
                      onPress={() => {
                        if(this._validateInput()){
                          this._saveDataAndNavigate()
                        }
                        }}>
                <Text style={{fontWeight:'bold', color:'white',fontFamily:'roboto'}}>KẾT NỐI</Text>
              </Button>
            </View>
          </Content>
        </Container>
        
      );
    }return(
      <Container>
        <Header hasSegment style={{ backgroundColor: "#0E4AA3", paddingTop:0.1, height:40 }}>
              <Left>  
                <Icon  onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', fontSize:30}} />
              </Left>
              <Body>
                <Title style={{color:'white', alignSelf:'center'}}>Bkav Egov</Title>
              </Body>
              <Right>
              </Right>
        </Header>
        <Spinner style={{color:"#0E4AA3"}}/>
      </Container>
    )
  }
  }

  const styles = StyleSheet.create({
    button: {
      width:buttonWidth,
    },
  
    homeImage: {
      backgroundColor:'transparent',
      justifyContent: 'flex-start',
      alignItems: 'center',
      width: imageWidth,
      height: imageHeight,
    },
    textOverImg:{
      color:'white',
      fontSize:16, 
      fontWeight:'bold',
      textAlign: 'center', 
      paddingTop:15, 
      fontFamily:'Roboto',
    },
  });