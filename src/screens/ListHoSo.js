import React from 'react';
import { View,FlatList,RefreshControl} from 'react-native';
import { Container,Title,Subtitle,Card,CardItem, Header,Spinner,Icon, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';

class ListHoSo extends React.Component {
  constructor(props) {
    super(props);
    this.page = 1;
    // this.dvid = navigation.getParam('DonViID');
    this.state = {
      loading: false, // user list loading
      isRefreshing: false, //for pull to refresh
      data: [], //user list
      error: '',
      iddv:'',
      idlv:'',
    }
  }
  componentWillMount() {
    console.log("___did mount");
    const { navigation } = this.props;
    const DonViID = navigation.getParam('DonViID');
    const LinhVucID = navigation.getParam('LinhVucID');
    this.setState({
      iddv:DonViID,
      idlv:LinhVucID,
    });
    this.fetchUser(LinhVucID,DonViID);
  }
  // componentDidUpdate() {
  //   return false;
  //   console.log('_________________');
  //   this.updateCount();
  // }

  // updateCount() {
  //   const { navigation } = this.props;
  //   const DonViID = navigation.getParam('DonViID');
  //   const TenDonVi = navigation.getParam('TenDonVi');
  //   const CapChaID = navigation.getParam('CapChaID');
  //   // this.setState({
  //   //   iddv:DonViID,
  //   // });
  //   console.log(DonViID);
  //   this.fetchUser(DonViID) 
  // }
  fetchUser(value1,value2) {
    //stackexchange User API url
    const url = `http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/geths34?iddm=${value1}&iddv=${value2}`;
     this.setState({ loading: true })
     fetch(url)
     .then(res => res.json())
     .then(res => {
        let data = res
        this.setState({ loading: false, data: data })
     }).catch(error => {
         this.setState({ loading: false, error: 'Something just went wrong' }),
         console.log('Loiiiiiiiiiiiiiii')
       });
       console.log(url);
       console.log(this.state.loading);
       console.log(this.state.data);
   };
   
  onRefresh() {
    // console.log("refresssssssssssssssssssss");
    this.setState({ isRefreshing: true }); // true isRefreshing flag for enable pull to refresh indicator
    const url = `http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/geths34?iddm=${this.state.idlv}&iddv=${this.state.iddv}`;
    fetch(url)
     .then(res => res.json())
     .then(res => {
        let data = res;
        this.setState({  isRefreshing: false, data: data });
     }).catch(error => {
         this.setState({  isRefreshing: false, error: 'Something just went wrong' })
       });
       console.log(this.state.data);
  };

   handleLoadMore = () => {
    // if (!this.state.loading) {
    //   this.page = this.page + 1; // increase page by 1
    //   this.fetchUser(); // method for API call 
    // }
  };
  // renderSeparator = () => {
  //   return (
  //     <View
  //       style={{
  //         height: 232,
  //         width: '100%',
  //         backgroundColor: '#CED0CE'
  //       }}
  //     />
  //   );
  // };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
     if (!this.state.loading) return null;
     return (
      <Spinner color='blue' />
     );
   };
  render() {
      const { navigation } = this.props;
      const TenLinhVuc = navigation.getParam('TenLinhVuc');
    if (this.state.loading) {
      return (
      <Container>
                <Header hasSegment style={{ backgroundColor: "#0E4AA3", paddingTop:0.1, height:50}}>
            <Left>
             <Icon onPress={() => this.props.navigation.goBack()} name="md-arrow-back" style={{ color: 'white', fontSize:40 }} />
            </Left>
        <Body >
            <Title style={{alignSelf:'center',color:'white'}}>{TenLinhVuc}</Title>
            <Subtitle style={{alignSelf:'center',color:'white'}}>Chọn Hồ sơ</Subtitle>
          </Body>
          <Right></Right>
        </Header>
      <Spinner color='blue' />
      </Container>
      );
    }
    return (
      <Container>
        <Header hasSegment style={{ backgroundColor: "#0E4AA3", paddingTop:0.1, height:50}}>
            <Left>
             <Icon onPress={() => this.props.navigation.goBack()} name="md-arrow-back" style={{ color: 'white', fontSize:40 }} />
            </Left>
        <Body >
            <Title style={{alignSelf:'center',color:'white'}}>{TenLinhVuc}</Title>
            <Subtitle style={{alignSelf:'center',color:'white'}}>Chọn Hồ sơ</Subtitle>
          </Body>
          <Right></Right>
        </Header>
        <Content refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }>
        <FlatList
          data={this.state.data}
          extraData={this.state}
          // refreshing={this.state.isRefreshing}
          // onRefresh={this.onRefresh}
          
          renderItem={({ item }) => (
            <TouchableOpacity  onPress={() => {
              /* 1. Navigate to the Details route with params */
              this.props.navigation.navigate('ViewNopHoSo', {
                LoaiHoSoID: item.LoaiHoSoID,
                LinhVucID: item.LinhVucID,
                DonVi: this.state.iddv,
              });
            }}>
              <Card>
              <CardItem header style={{height:35,paddingBottom:0.5}} >
                <Left ></Left>
              <Right>
              <Text>Mức độ {item.MucDo}</Text>
              </Right>

              </CardItem>
            <CardItem>
              <Left style={{flex:1}}>
            <Thumbnail square source={require("../assets/logotayninh.png")} />
            </Left>
            <Body style={{flex:5,marginLeft:10}}>
            <Text style={{alignSelf:'center'}}>{item.TenLoaiHoSo}</Text>
            </Body>
              <Right style={{flex:0.5}}>
                <Icon name="arrow-forward" />
              </Right>
             </CardItem>
              </Card>
            {/* <List>
             

            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require("../assets/logotayninh.png")} />
              </Left>
              <Body>
                <Text>{item.TenLoaiHoSo}</Text>
                <Text note numberOfLines={1}></Text>
              </Body>
              <Right>
              <Button transparent>
              <Icon name="arrow-forward" />
                </Button>
              </Right>
            </ListItem> 
          </List> */}
            </TouchableOpacity>
          )}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this.renderSeparator}
          ListFooterComponent={this.renderFooter.bind(this)}
          onEndReachedThreshold={0.4}
          onEndReached={this.handleLoadMore.bind(this)}
        />
        </Content>
      </Container>
    );
  }
    // render() {
    //   /* 2. Get the param, provide a fallback value if not available */
    //   const { navigation } = this.props;
    //   const DonViID = navigation.getParam('DonViID');
    //   const TenDonVi = navigation.getParam('TenDonVi');
    //   const CapChaID = navigation.getParam('CapChaID');
  
    //   return (
    //     <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    //       <Text>Danh sách loại hồ sơ</Text>
    //       <Text>itemId: {JSON.stringify(DonViID)}</Text>
    //       <Text>TenDonVi: {JSON.stringify(TenDonVi)}</Text>
    //       <Text>CapChaID: {JSON.stringify(CapChaID)}</Text>
    //       {/* <Button
    //         title="Go to Details... again"
    //         onPress={() =>
    //           this.props.navigation.push('Details', {
    //             itemId: Math.floor(Math.random() * 100),
    //           })}
    //       />
    //       <Button
    //         title="Go to Home"
    //         onPress={() => this.props.navigation.navigate('Home')}
    //       /> */}
    //       <Button
    //         title="Go back"
    //         onPress={() => this.props.navigation.goBack()}
    //       />
    //     </View>
    //   );
    // }
}
export default withNavigation(ListHoSo);