import React, { Component } from 'react';
import { View,FlatList,SectionList,RefreshControl,Alert} from 'react-native';
import { Container, Header,Spinner, Content, List, ListItem, Separator , Text, Left, Body, Right, Button } from 'native-base';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';
class tabThree extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = {
      loading: false, // user list loading
      isRefreshing: false, //for pull to refresh
      data: [], //user list
      error: ''
    }
  }
  componentWillMount() {
    this.fetchUser() //Method for API call
  }
  fetchUser() {
    //stackexchange User API url
     const url = `http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/getcapxa`;
     this.setState({ loading: true })
     fetch(url)
     .then(res => res.json())
     .then(res => {
        let data = res
        this.setState({ loading: false, data: data })
     }).catch(error => {
         this.setState({ loading: false, error: 'Something just went wrong' })
       });
   };
   
  onRefresh() {
    console.log("refresssssssssssssssssssss");
    this.setState({ isRefreshing: true }); // true isRefreshing flag for enable pull to refresh indicator
    const url = `http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/getcapxa`;
    fetch(url)
     .then(res => res.json())
     .then(res => {
        let data = res;
        this.setState({  isRefreshing: false, data: data });
     }).catch(error => {
         this.setState({  isRefreshing: false, error: 'Something just went wrong' })
       });
       console.log(this.state.data);
  };

   handleLoadMore = () => {
    // if (!this.state.loading) {
    //   this.page = this.page + 1; // increase page by 1
    //   this.fetchUser(); // method for API call 
    // }
  };
  // renderSeparator = () => {
  //   return (
  //     <View
  //       style={{
  //         height: 232,
  //         width: '100%',
  //         backgroundColor: '#CED0CE'
  //       }}
  //     />
  //   );
  // };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
     if (!this.state.loading) return null;
     return (
      <Spinner color='blue' />
     );
   };
  //  GetSectionListItem = (value1,value2,value3) => {
  //   //Function for click on an item
  //   // Alert.alert(item);
  //   this.props.navigation.navigate('ListLoaiHoSo', {
  //     DonViID: value1,
  //     TenDonVi: value2,
  //     CapChaID: value3,
  //   });
  //   console.log('_____________');
  //   console.log(value1);
  //   console.log(value2);
  //   console.log(value3);
  // };
  FlatListItemSeparator = () => {
    return (
      //Item Separator
      <View style={{height: 0.5, width: '100%', backgroundColor: '#C8C8C8'}}/>
    );
  };
  render() {
    var DemoData=[
      {
          id: 1,
          ten: 'TP Tây Ninh',
          data:[ {
              id: 12,
              ten: 'Phuong 1'},
              {
              id: 13,
              ten: 'Phuong 2'},
              {
              id: 14,
              ten: 'Phuong 3'}
        ]
      },
      {
        id: 3,
        ten: 'Hoà Thành',
        data:[ {
            id: 12,
            ten: 'Phuong 1'},
            {
            id: 13,
            ten: 'Phuong 2'},
            {
            id: 14,
            ten: 'Phuong 3'}
      ]
    }
    ];
    if (this.state.loading) {
      return (
      <Container>
      <Spinner color='blue' />
      </Container>
      );
    }

    return (
      <Container>
        <Content>
        <SectionList
          ItemSeparatorComponent={this.FlatListItemSeparator}
          sections={this.state.data}
          renderSectionHeader={({ section }) => (
            <Separator bordered style={{height:50}}>
            
            <Text style={{fontSize:15,fontWeight:'bold'}}> {section.TenDonVi} </Text>
            
            </Separator>
          )}
          renderItem={({ item }) => (
            <ListItem >
                <TouchableOpacity onPress={() => {
              /* 1. Navigate to the Details route with params */
              this.props.navigation.navigate('ListLoaiHoSo', {
                DonViID: item.DonViID,
                TenDonVi: item.TenDonVi,
                CapChaID: item.CapChaID,
              });
            }}>
               <Text
              //Item Separator View
              >
              {item.TenDonVi}
            </Text>
            </TouchableOpacity>
            </ListItem>
          
          )}
          keyExtractor={(item, index) => index}
        />
        </Content>
      </Container>
    );
  }
}
export default withNavigation(tabThree);