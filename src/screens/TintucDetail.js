import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    StatusBar,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Alert,
    WebView,
    AsyncStorage,
    ImageBackground,
    YellowBox,
    FlatList,
    RefreshControl
  } from "react-native";
  import { Button,Container,Header,Left,Right,Icon,Grid,Col,Row, Item,Input,Title,Body,Label, Content,Card, CardItem, Spinner} from 'native-base';
  import { TouchableOpacity } from "react-native-gesture-handler";
  import {withNavigation} from 'react-navigation';
  
  class TintucDetail extends React.Component {
    componentWillMount(){
      this._fetchMore(this.props.ID,this.trang);
    }
    constructor(props) {
      //constructor to set default state
      super(props);
      this.trang = 0
      this.state={
        dataList:[],
        loading:false,
        isRefreshing: false,
      }
    }
    _fetchMore(id,index){
      var bodyParams= `iddv=${id}&sotin=5&trang=${index}`;
      const serviceUrl = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/dstintuc';
      this.setState({loading:true})
      fetch(serviceUrl, {
        method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: bodyParams
      }).then(response => response.json())
      .then(responseData => {
        
        let listData = this.state.dataList;
        let data = listData.concat(responseData)
        this.setState({dataList:data,loading:false});
      })
      

    }
    handleLoadMore = () => {
      console.log('-------------BEGIN LOAD MORE-----------------')
      
      if (!this.state.loading) {
        this.trang=this.trang+1
        this._fetchMore(this.props.ID,this.trang);
      }
      console.log('-------------SO TRANG-----------------')
      console.log(this.trang)
      console.log(this.props.ID)
    };

    renderFooter = () => {
       if (!this.state.loading) return null;
       return (
         <Spinner color='#0E4AA3'/>
       );
     };

     onRefresh() {
       console.log('============BEGIN REFRESH==============')
       this.trang=0
      this._fetchTin(this.props.ID,this.trang);
      console.log(this.trang)
    }
    _fetchTin(id,index){
      var bodyParams= `iddv=${id}&sotin=5&trang=${index}`;
      const serviceUrl = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/dstintuc';
      this.setState({isRefreshing:true})
      fetch(serviceUrl, {
        method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: bodyParams
      }).then(response => response.json())
      .then(responseData => {
        this.trang = 0
        this.setState({dataList:responseData,isRefreshing:false});
      })
    }


    render() {
      if (this.state.loading && this.trang === 0) {
        return <View style={{
          width: '100%',
          height: '100%'
        }}><Spinner color='#0E4AA3'/></View>;
      }
      return (
      <View style={{ width: '100%', height: '100%', paddingLeft:8, paddingRight:8,paddingTop:5, paddingBottom:5}}>
      <FlatList
        data={this.state.dataList}
        extraData={this.state}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isRefreshing}
            onRefresh={this.onRefresh.bind(this)}
          />
        }
        renderItem={({ item }) => (
        <Card key={item.ID}
        style={{borderRadius:10}}
        >
             {/* Link url */}
           <TouchableOpacity onPress={() => this.props.navigation.navigate('Tinmoinhat',{itemID:item.ID})}>
           {/* Ảnh bìa */}
            <CardItem cardBody style={{borderTopLeftRadius:10, borderTopRightRadius:10}}> 
             <Image resizeMode='cover' source={{uri: item.HinhDaiDien}} style={{height: 180, width: null, flex: 1, borderRadius:10}}/>
        </CardItem>

            {/* Title */}
             <CardItem>
               <View>
                 <Text style={{fontWeight:'bold',fontSize:16,textAlign:'justify'}}>{item.TieuDe}</Text>
               </View>
             </CardItem>

             {/* Publish date */}
             <CardItem style={{borderBottomLeftRadius:10, borderBottomRightRadius:10}}>
             <Left>
             <Text style={{fontSize:12}}>{item.TenDonVi}</Text>
             </Left>
             <Body></Body>
             <Right>
             <Text style={{fontSize:12}}>{item.Ngay}</Text>
             </Right>
             <View>
                
              </View>
             </CardItem>

             </TouchableOpacity>

           </Card>  
        )}
        keyExtractor={(item, index) => index.toString()}
        onEndReachedThreshold={0.3}
        onEndReached={this.handleLoadMore.bind(this)}
        ListFooterComponent={this.renderFooter.bind(this)}
        />
         </View>
         
      );
            
            
    }
  
    
    
   
  }
  export default withNavigation(TintucDetail);

  