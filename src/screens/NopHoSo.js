/* eslint-disable no-use-before-define */
/* eslint-disable import/no-named-as-default */
/* eslint-disable import/no-named-as-default-member */
// eslint-disable-next-line spaced-comment
//// star....................
import React, { Component } from 'react';
import { StyleSheet,Text } from 'react-native';
import {
  Container,
  Header,
  Tab,
  Tabs,
  ScrollableTab,
  Left,
  Button,
  Icon,
  Item,
  Body,
  Input,
  Right,
  Title
} from 'native-base';
import Tab1 from './tabOne';
import Tab2 from './tabTwo';
import Tab3 from './tabThree';

// eslint-disable-next-line react/prefer-stateless-function
export default class NopHoSo extends Component {
  render() {
    return (
      <Container>
        {/* <Header hasTabs /> */}
        <Header hasSegment style={styles.header}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate('Home')}>
              <Icon
                name="md-home"
                ios="ios-home"
                android="md-home"
                style={{ color: 'white', fontSize: 30, marginLeft: 0, marginRight: 0 }}
              />
            </Button>
          </Left>
          <Body>
          <Title style={{alignSelf:'center',color:'white'}}>Nộp hồ sơ</Title>
          </Body>
          <Right></Right>
        </Header>
        <Tabs renderTabBar={() => <ScrollableTab />}>
          <Tab heading="Cấp Tỉnh">
            <Tab1 />
          </Tab>
          <Tab heading="Cấp Huyện">
            <Tab2 />
          </Tab>
          <Tab heading="Cấp Xã">
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  header: {
    backgroundColor: '#0E4AA3',
    paddingTop:0.1,
    height:50
  },
});
