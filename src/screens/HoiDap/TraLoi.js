import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  ScrollView,
  Alert,
  TouchableOpacity,SafeAreaView
} from 'react-native';
import { Rating, AirbnbRating } from 'react-native-elements';
const regex = /(<([^>]+)>)/ig;
function suahtml(s){
  s = s.replace(regex,'');
  return s;
}
export default class TraLoi extends Component {
  static navigationOptions = {
      header: null
  }
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      key: params.id,
      data: [],
      datatl: []
    };
  }
  componentDidMount() {
    this.getdata();
  }
  /*componentWillReceiveProps(nextProps) {
    if(nextProps.navigation.state.params.id!==this.state.key){
      this.setState({key: nextProps.navigation.state.params.id})
    }
    setTimeout(() => {      
      this.getdata();      
    }, 100);    
  } */
  getdata = () => {
    const urlh = `https://hoidap.tayninh.gov.vn/HoiDapTrucTuyenServices.asmx/ChiTietCauHoi?id=${this.state.key}`;
    const urltl = `https://hoidap.tayninh.gov.vn/HoiDapTrucTuyenServices.asmx/ChiTietCauTraLoi?id=${this.state.key}`;
    fetch(urlh)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          data: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      }) 
    fetch(urltl)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          datatl: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      }) 
  }
  ratingCompleted(rating,id){
    Alert.alert(
      'Thông báo',
      'Cảm ơn ý kiến đánh giá của bạn!',
      [
        { text: 'OK', onPress: () => console.log('OK Pressed') },
      ]
    );
    var paramsString = `idtr=${id}&rate=${rating}&code=i@lBavj3$79Rms84nd`;
    url = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/ratingtl';
    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: paramsString
    });
  }
  render() {
    const { navigate } = this.props.navigation;
    const hoi = this.state.data.map((item) => {
      return (
        <View style={{marginBottom:5}}>
          <Text style={{fontWeight: 'bold'}}>{item.TieuDeHoi}</Text>
          <Text style={{fontSize:10}}>(Người hỏi: {item.HoVaTen}, Điện thoại: {item.DienThoai}, Email: {item.Email})</Text>
          <Text style={{fontSize:12}}>{suahtml(item.NoiDungHoi)}</Text>
        </View>
      )
    })
    const traloi = this.state.datatl.map((item) => {
      return (
        <View style={{marginTop:12}}>
          <Text style={{fontWeight: 'bold'}}>Nội dung trả lời của {item.TenDonVi}</Text>
          <Text style={{fontSize:10}}>(Địa chỉ: {item.DiaChi}, Điện thoại: {item.DienThoai}, Email: {item.Email})</Text>
          <View style={{justifyContent: 'center'}}>
            <Text style={{flex:1,fontSize:10,marginTop:5,marginBottom:5,textAlign:'center'}}>Đánh giá câu trả lời:</Text>
          </View>
            <AirbnbRating
            ratingCount={5}
            size={35}
            defaultRating={0}
            onFinishRating={(rating) => this.ratingCompleted(rating,item.Id)}           
            reviews={["Rất không hài lòng","Không hài lòng","Bình thường","Hài lòng","Rất hài lòng"]}
            />          
          <Text style={{fontSize:12}}>{suahtml(item.NoiDungTraLoi)}</Text>
        </View>
      )
    })
    return (
		<SafeAreaView style={st.container}>
			<View style={st.thanh1}>
				<View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1}}>
				  <TouchableOpacity onPress={() => navigate('HoiDap')}>
					<Image style={st.icon_menu} source={require('../../assets/HKG/back.png')} />
				  </TouchableOpacity>					
				</View>
				<View style={{marginTop:6,flex:1}}>
					<Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Chi tiết hỏi đáp</Text>
				</View>
			</View>
      <ScrollView style={st.bao}>
      {hoi}
      <View
        style={{
          borderBottomColor: 'black',
          borderBottomWidth: 1,
        }}
      />
      {traloi}
      </ScrollView>
		</SafeAreaView>
    );
  }

}
const st = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ededed'
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
	  marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: 'white'
  },
  thanh1: {
		backgroundColor: '#3771c3',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
	},
});
