import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,Dimensions,
  Alert,Platform,
  TouchableOpacity,SafeAreaView,TextInput
} from 'react-native';
import { Container, Textarea, Content, Form, Item, Input, Label, Button, Icon} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import Modal from "react-native-modal";

const { width } = Dimensions.get('window');
export default class DatCauHoi extends Component {
  static navigationOptions = {
      header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      file: null,
      hoten: null,
      diachi: null,
      dienthoai: null,
      email: null,
      tieude: null,
      noidung:null,
      maxt:null,
      modalVisible: false,
      txtxt:null,
      dagui:null
    };
  }
  componentDidMount() {
    if(this.state.maxt === null){
      this.setState({ maxt: this.getRandomArbitrary(100000,999999) });
    }
  }
  pickSingle(cropit, circular=false, mediaType) {
    ImagePicker.openPicker({
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
    }).then(image => {
      this.setState({
        file: image
      });
    }).catch(e => {
      console.log(e);
    });
  }
  getma = () => {
    if(this.state.dagui === null){
      const paramsString = `noidung=Ma xac thuc cua ban la: ${this.state.maxt}&code=M2Fl)k3ml$snHA028Tbd&sdt=${this.state.dienthoai}`;
      fetch('http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/NhanTinDD', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: paramsString
      });
      this.setState({dagui: true});
    }
  }
  getRandomArbitrary(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }
  submitdata = () => {
    if(this.state.noidung === null){
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập nội dung',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]
      );
    } else
    if(this.state.tieude === null){
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập tiêu đề',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]
      );
    } else
    if(this.state.dienthoai === null){
      Alert.alert(
        'Thông báo',
        'Bạn chưa nhập số điện thoại',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]
      );
    } else {      
      this.setState({modalVisible: true});
      this.getma();
    }
  }
  xacthuc = () => {
    if(this.state.txtxt == this.state.maxt){
      this.handleUploadPhoto();
    } else {
      Alert.alert(
        'Thông báo',
        'Mã xác thực không đúng!',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]
      );
    }
  }
  handleUploadPhoto = () => {
    const data = new FormData();
    data.append('hoten', this.state.hoten); // you can append anyone.
    data.append('diachi', this.state.diachi);
    data.append('sdt', this.state.dienthoai);
    data.append('email', this.state.email);
    data.append('tieude', this.state.tieude);
    data.append('noidung', this.state.noidung);
    if(this.state.file != null){
      data.append('filedinhkem', {
      name: Platform.OS === "android" ? this.state.file.path.substring(this.state.file.path.lastIndexOf('/')+1) : this.state.file.filename,
      type:  this.state.file.mime, // or photo.type
      uri:
        Platform.OS === "android" ? this.state.file.path : this.state.file.path.replace("file://", "")
      });
    console.log("Pirnt_______________________");
    console.log(data);
    }
    fetch(`https://hoidap.tayninh.gov.vn/HoiDapTrucTuyenServices.asmx/DatCauHoi`, {
    method: 'post',
    body: data
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.trangthai === "thanhcong"){
        Alert.alert(
          'Thông báo',
          'Bạn đã gửi câu hỏi thành công',
          [
            { text: 'OK', onPress: () => this.props.navigation.navigate('HoiDap') },
          ]
        );
      } else {
        Alert.alert(
          'Thông báo',
          'Có lỗi trong quá trình xử lý',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
      }
    })
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
		<SafeAreaView style={st.container}>
      <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.setState({ modalVisible: null })}>
        <View style={st.modalContent}> 
          <Text>Một mã xác thực đã được nhắn đến số điện thoại của bạn, vui lòng nhập mã xác thực để gửi câu hỏi</Text> 
          <TextInput
            style={st.inputStyle} keyboardType='numeric'
            placeholder="Nhập mã xác thực"
            underlineColorAndroid="transparent"
            value={this.state.tk}
            onChangeText={text => this.setState({ txtxt: text })}
          //secureTextEntry
          />
          <TouchableOpacity style={st.bigButton} onPress={() => this.xacthuc()}>
            <Text style={st.buttonText}>XÁC THỰC</Text>
          </TouchableOpacity>
        </View>
      </Modal>
      <View style={st.thanh1}>
				<View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1}}>
				  <TouchableOpacity onPress={() => navigate('HoiDap')}>
					<Image style={st.icon_menu} source={require('../../assets/HKG/back.png')} />
				  </TouchableOpacity>					
				</View>
				<View style={{marginTop:6,flex:1}}>
					<Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Đặt câu hỏi</Text>
				</View>
			</View>
			<Container>
        <Content>
          <Form style={{paddingLeft:0,paddingRight:10,paddingBottom:10}}>
            <Item floatingLabel>
              <Label style={st.lbtxt}>Cá nhân/Tổ chức</Label>
              <Input value={this.state.hoten} onChangeText={text => this.setState({ hoten: text })}/>
            </Item>
            <Item floatingLabel>
              <Label style={st.lbtxt}>Địa chỉ</Label>
              <Input value={this.state.diachi} onChangeText={text => this.setState({ diachi: text })}/>
            </Item>
            <Item floatingLabel>
              <Label style={st.lbtxt}>Điện thoại</Label>
              <Input value={this.state.dienthoai} keyboardType='numeric' onChangeText={text => this.setState({ dienthoai: text })}/>
            </Item>
            <Item floatingLabel>
              <Label style={st.lbtxt}>Email</Label>
              <Input value={this.state.email} onChangeText={text => this.setState({ email: text })}/>
            </Item>
            <Item floatingLabel> 
              <Label style={st.lbtxt}>Tiêu đề câu hỏi</Label>
              <Input value={this.state.tieude} onChangeText={text => this.setState({ tieude: text })}/>
            </Item>
            <Textarea rowSpan={8} bordered placeholder="Nội dung câu hỏi" style={{fontSize:14,marginLeft:15}} value={this.state.noidung} onChangeText={text => this.setState({ noidung: text })}/>               
            <Button iconLeft transparent onPress={() => this.pickSingle(false)}>
              <Icon name='camera' />
              <Text style={{fontSize:14,marginLeft:10}}>{this.state.file !== null ? this.state.file.filename :'Đính kèm file'}</Text>
            </Button>         
            <Button block last style={{marginTop:5,marginLeft:15}} onPress={() => this.submitdata()}>
              <Text style={{color:'white'}}>Gửi câu hỏi</Text>
            </Button>
          </Form>
        </Content>
      </Container>      
		</SafeAreaView>
    );
  }

}
const st = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ededed'
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
	  marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: 'white'
  },
  thanh1: {
		backgroundColor: '#3771c3',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
  },
  lbtxt: {
    fontSize: 14
  },
  modalContent: {
    backgroundColor: '#f3f3f3',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  inputStyle: {
    height: 50,
    width: (width / 10) * 8,
    marginHorizontal: 1,
    backgroundColor: '#fff',
    marginBottom: 10,
    marginTop:10,
    borderRadius: 20,
    paddingLeft: 30
  },
  bigButton: {
    height: 50,
    borderRadius: 20,
    width: 200,
    backgroundColor: '#ea321a',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#fff',
    fontWeight: '400'
  }
});
