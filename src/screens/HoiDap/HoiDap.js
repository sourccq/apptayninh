import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
  RefreshControl,
  Alert,
  TouchableOpacity,SafeAreaView
} from 'react-native';
import { Icon} from 'native-base'
const regex = /(<([^>]+)>)/ig;
function suahtml(s){
  s = s.replace(regex,'');
  return s;
}
export default class HoiDap extends Component {
  static navigationOptions = {
      header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 1,
      refreshing: false,
      error: null,
      td: null
    };
  }
  componentDidMount() {
    this.makeRemoteRequest();
  }
  makeRemoteRequest = () => {
    const { page, seed } = this.state;
    const url = `https://hoidap.tayninh.gov.vn/HoiDapTrucTuyenServices.asmx/DanhSachCauHoi?pageRows=15&pageIndex=${page}`;
    this.setState({ loading: true });
    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res : [...this.state.data, ...res],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };
  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };  

  render() {
    const { navigate } = this.props.navigation;
    return (
		<SafeAreaView style={st.container}>
			<View style={st.thanh1}>
				<View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1}}>
				  <TouchableOpacity onPress={() => navigate('Home')}>
					{/* <Image style={st.icon_menu} source={require('../../assets/HKG/back.png')} /> */}
          <Icon  name="md-home" style={st.icon_menu} />
				  </TouchableOpacity>					
				</View>
				<View style={{marginTop:6,flex:1}}>
					<Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Hỏi đáp trực tuyến</Text>
				</View>
			</View>
			<FlatList
			  onRefresh={this.handleRefresh}
        refreshing={this.state.refreshing}
        onEndReached={this.handleLoadMore}
        onEndReachedThreshold={1}
        data={this.state.data}
        renderItem={({ item }) =>
				<TouchableOpacity onPress={() => this.props.navigation.navigate('TraLoi', { id: item.Id })}>
				  <View style={st.bao}>
					  <Text style={{fontWeight: 'bold'}}>{item.TieuDeHoi}</Text>
            <Text style={{fontSize:12}}>(Người hỏi: {item.HoVaTen}, Điện thoại: {item.DienThoai}, Email: {item.Email})</Text>
				  </View>
				</TouchableOpacity>
			  }
			/>
      <TouchableOpacity onPress={() => this.props.navigation.navigate('DatCauHoi')}>
          <Text style={{fontWeight: 'bold',textAlign: 'center',backgroundColor:'red',fontSize:16, padding:10,color:'white'}}>Đặt câu hỏi</Text>
			</TouchableOpacity>
		</SafeAreaView>
    );
  }

}
const st = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ededed'
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
	  marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: 'white'
  },
  thanh1: {
		backgroundColor: '#3771c3',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
    marginLeft: 5,
    color: 'white', 
    fontSize:30
	},
});
