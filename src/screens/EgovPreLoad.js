import React from 'react';
import {StyleSheet} from 'react-native';
import {Spinner,Container,Header,Content,Left,Right,Body,Icon} from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';


export default class EgovPreLoad extends React.Component {
  constructor(props) {
    super(props);
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const egovToken = await AsyncStorage.getItem('egovUri');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(egovToken ? 'egov' : 'egovConfig');
  };

  // Render any loading content that you like here
  render() {
    return (
      <Container>
        <Header hasTabs style={styles.header}>
          <Left>  
              <Icon  onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', fontSize:30}} />
          </Left>
          <Body></Body>
          <Right></Right>
        </Header>
        <Content>
          <Spinner/>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
   
  header:{
    backgroundColor: "#0E4AA3",
    height:40,
    justifyContent: 'center',
    paddingTop:0.1   
  },
  
});