import React, { Component } from "react";
import {Dimensions} from "react-native";
  import {Container,Header,Left,Right,Icon,Grid,Row,Title,Body,Spinner } from 'native-base';
  import WebView from 'react-native-webview';
  import AsyncStorage from '@react-native-community/async-storage';

  const { width: deviceWidth } = Dimensions.get('window');
  const { height: deviceHeigh } = Dimensions.get('window');
  const headerWidth = deviceWidth/2;

  export default class egov extends React.Component {
   
    componentDidMount(){
      this._retrieveData()
    }

    constructor(props) {
      super(props);
      this.state = {
        uri:''
      }
    }
    
    _retrieveData = async () => {
      try {
        const value = await AsyncStorage.getItem('egovUri');
        if (value !== null) {
          this.setState({uri:value})
        }
      } catch (error) {
        // Error retrieving data
      }
    }
    _deleteDataAndNavigate = async () => {
      try {
        await AsyncStorage.removeItem('egovUri')
        this.props.navigation.navigate('EgovPreLoad')
      } catch (error) {
         //Error retrieving data
      }
    }

    render() {
      if(this.state.uri != null){
        return (
          <Container>
            <Header hasSegment style={{ backgroundColor: "#0E4AA3",paddingTop:0.1,height:40 }}>
                <Left>
                  <Icon onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', marginRight: 15 }} />
                </Left>
                <Body>
                  <Title style={{color:'white', width:headerWidth}}>Bkav Egov</Title>
                </Body>
                <Right>
                  <Icon 
                  onPress={() => {
                    this._deleteDataAndNavigate()
                    //WebViewRef && WebViewRef.
                    //WebViewRef && WebViewRef.reload();
                    }} 
                  name="md-settings" 
                  style={{ color: 'white', marginRight: 15 }} />
                </Right>
              </Header>
            <Grid>

            {/* Webview */}
              <Row>
                <WebView
                      ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
                      source={{uri: this.state.uri}} 
                      startInLoadingState={true}
                              renderLoading={() => <Spinner color='#0E4AA3' style={{
                                position: 'absolute',
                                left: 0,
                                right: 0,
                                top: 0,
                                bottom: 0,
                                alignItems: 'center',
                                justifyContent: 'center'}}/>} 
                />
              </Row>
            </Grid>
          </Container>
        )
    }

  // -----------------------Render if uri is Null----------------------------
    return(
      <Container>
        <Header style={{ backgroundColor: "#0E4AA3",paddingTop:0.1,height:40 }}>
                <Left>
                  <Icon onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', marginRight: 15 }} />
                </Left>
                <Body>
                  <Title style={{color:'white', width:headerWidth}}>Bkav Egov</Title>
                </Body>
                <Right>
                  <Icon 
                  onPress={() => {this.props.navigation.navigate('eGov')}} 
                  name="md-settings" 
                  style={{ color: 'white', marginRight: 15 }} />
                </Right>
              </Header>
              <Spinner/>
      </Container>
    ) 
}
  }

  