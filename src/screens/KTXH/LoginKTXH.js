import React, { Component } from 'react';
import {
  StyleSheet,
  Dimensions,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,SafeAreaView,
  TouchableWithoutFeedback
} from 'react-native';
import getToken from '../../api/getToken';
import checkToken from '../../api/checkTokenKTXH';
import DefaultPreference from 'react-native-default-preference';
import checkDangnhapKTXH from '../../api/checkDangnhapKTXH';
import saveToken from '../../api/saveToken';
import dismissKeyboard from 'react-native/Libraries/Utilities/dismissKeyboard';

var tkp = '';
DefaultPreference.get('ToKenPush').then(function(value) {
  tkp = value;          
});

const { width } = Dimensions.get('window');
export default class LoginKTXH extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tk: '',
      mk: '',
      tendv: ''
    };
}
componentDidMount() {
           const { navigation } = this.props;
         navigation.addListener('willFocus', () =>
             this.setState({ focusedScreen: true}),
             getToken('ktxh')
    .then(token => checkToken(token))
    .then(res => {
      this.setState({ tendv: res.tendv });
    })
    .catch(err => console.log('LOI CHECK lỗi __________', err))
        );
         navigation.addListener('willBlur', () =>
             this.setState({ focusedScreen: true}),
             getToken('ktxh')
    .then(token => checkToken(token))
    .then(res => {
      this.setState({ tendv: res.tendv });
    })
    .catch(err => console.log('LOI CHECK lỗi __________', err))
    );
  
}
onSignIn() {
  const { tk, mk } = this.state;
  checkDangnhapKTXH(tk, mk)
    .then(res => {   
      if (res.thongbao === 'dung') {         
        var paramsString = `token=${res.token}&tkp=${tkp}`;
        url = 'https://ktxh.tayninh.gov.vn/apiservice.asmx/luutkp';
        fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
          body: paramsString
        })
        //global.onSignIn(res);
        saveToken('ktxh',res.token);
        this.props.navigation.navigate('HomeKTXH',{islogin: true});        
      } 
      if (res.thongbao === 'sai') {
        Alert.alert(
          'Thông báo',
          'Sai thông tin đăng nhập',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
      }
      if (res.thongbao === '0ld') {
        Alert.alert(
          'Thông báo',
          'Chức năng này chỉ dành cho lãnh đạo',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
      }
    })
    .then()
    .catch(err => { console.log('Lỗi'); console.log(err); });
}
  render() {
    const { navigate } = this.props.navigation;
    return (
      <TouchableWithoutFeedback style={{ flex: 1 }} onPress={dismissKeyboard}> 
      <SafeAreaView style={{flex:1}}>       
      <View style={styles.thanh1}>
          <View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1 }}>
            <TouchableOpacity onPress={() => navigate('HomeKTXH')}>
              <Image style={styles.icon_menu} source={require('../../assets/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{ marginTop: 5, flex: 1 }}>
            <Text style={{ textAlign: 'center', fontSize: 16, color: 'white', fontWeight: 'bold',marginLeft:-35,zIndex:0 }}>Đăng nhập</Text> 
          </View>
        </View>
        <View style={styles.container} >
			<View>
			  <View style={{marginTop:20, alignItems:'center'}}>
			  <Image
				source={require('../../assets/HKG/logotayninh.png')}
				style={[styles.icon]}
			  />
			  </View>
			  <View style={styles.header}>
				<Text style={{ color: 'blue', fontSize: 11, fontWeight: 'bold' }}>{this.state.tendv}</Text>
				<Text style={{ color: 'blue', fontSize: 11, fontWeight: 'bold' }}>HỆ THỐNG BÁO CÁO CHỈ TIÊU KINH TẾ XÃ HỘI</Text>
			  </View>
			</View>
			<View style={{ justifyContent: 'space-between', flex: 2 }}>
			  <View style={{ alignItems: 'center', marginTop: 25 }}>
				{/* <Text>Tên Đăng nhập</Text> */}
				<TextInput
				  style={styles.inputStyle}
				  placeholder="Nhập tên tài khoản"
				  underlineColorAndroid="transparent"
				  value={this.state.tk}
				  onChangeText={text => this.setState({ tk: text })}
				//secureTextEntry
				/>
				{/* <Text>Mật khẩu</Text> */}
				<TextInput
				  style={styles.inputStyle}
				  placeholder="Nhập mật khẩu"
				  underlineColorAndroid="transparent"
				  value={this.state.mk}
				  onChangeText={text => this.setState({ mk: text })}
				  secureTextEntry
				/>
				<TouchableOpacity style={styles.bigButton} onPress={this.onSignIn.bind(this)}>
				  <Text style={styles.buttonText}>ĐĂNG NHẬP</Text>
				</TouchableOpacity>
			  </View>
			  <View style={styles.footer}>
				<Text></Text>
				<Text></Text>
			  </View>
			</View>
        </View >        
      </SafeAreaView >
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f3f3f3',
  },
  icon: {
    // justifyContent: 'center',
    alignItems: 'center',
    width: 140,
    height: 140,
  	paddingTop: 15
  },
  header:
  {
    marginTop: 25,
    alignItems: 'center',
  },
  footer: {
    //flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  inputStyle: {
    height: 50,
    width: (width / 10) * 8,
    marginHorizontal: 1,
    backgroundColor: '#fff',
    marginBottom: 10,
    borderRadius: 20,
    paddingLeft: 30
  },
  bigButton: {
    height: 50,
    borderRadius: 20,
    width: 200,
    backgroundColor: '#94c3bf',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#fff',
    fontWeight: '400'
  },
  thanh1: {
		backgroundColor: '#94c3bf',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
	},
  footer: {
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
});
