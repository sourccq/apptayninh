/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Image,
  TouchableOpacity,
  Alert,SafeAreaView
} from 'react-native';
import Modal from "react-native-modal";
import ModalSelector from 'react-native-modal-selector';

import getToken from '../../api/getToken';
import folder from '../../assets/folder.png';
import file from '../../assets/file.png';
import back from '../../assets/back.png';

export default class DuyetCT extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      bcid: params.idbc,
      screen: params.screen,
      tenbc: params.tenbc,
      duyet: params.duyet,
      date: new Date(),
      data: [],
      modalVisible: false,
      did: '1',
      tk: null
    };
    //console.log(key);
  }
  timkiem = () =>{
    this.setState({modalVisible: true});
  }
  dong = () =>{
    this.setState({modalVisible: false});
  }
  duyetsl = () =>{
    var paramsString = `BC_ID=${this.state.bcid}&ten=${this.state.tenbc}&duyet=${this.state.did}&token=${this.state.tk}`;
    const url = `https://ktxh.tayninh.gov.vn/apiservice.asmx/DuyetSL`;
    fetch(url, {
      method: 'POST',
      headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
    },
    body: paramsString
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson[0].thongbao == "ok"){
        Alert.alert(
          'Thông báo',
          'Thực hiện thành công',
          [
            { text: 'OK', onPress: () => console.log('OK Pressed') },
          ]
        );
        this.setState({modalVisible: false});
      }
      else 
      Alert.alert(
        'Thông báo',
        'Lỗi',
        [
          { text: 'OK', onPress: () => console.log('OK Pressed') },
        ]
      );
    })
    .catch((err) => {
        console.log('Loi');
    });
  }
  getdata = () => {
    getToken('ktxh')
    .then(token => {
      this.setState({ tk: token });
      if (token.length > 0) {
        var paramsString = `BC_ID=${this.state.bcid}&token=${token}`;
        const url = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiSoLieuBaoCao`;
        fetch(url, {
          method: 'POST',
          headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
        })
        .then((response) => response.json())
        .then((responseJson) => {
            this.setState({
            data: responseJson
            });
        })
        .catch((err) => {
            console.log('Loi');
        });   
        console.log(url);
      }
    })
  } 
  componentDidMount() {   
    this.getdata();     
  }  
  render() {     
    const { navigate } = this.props.navigation;
    const btduyet = (
      <TouchableOpacity style={styles.btluuh} onPress={()=>this.timkiem()}>
      <Image style={styles.icon_check} source={require('../../assets/checkbox-icon.png')} />
      </TouchableOpacity>
    )
    const duyet = [
      { key: 1, label: 'Duyệt số liệu' },
      { key: 0, label: 'Trả lại' }
    ];
    const mainJSX = this.state.duyet == '-1' ? btduyet : null;
    return (
      <SafeAreaView style={styles.container}>
        <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.setState({ modalVisible: null })}>
          <View style={styles.modalContent}>
            <ModalSelector
              style={styles.combobox}
              data={duyet}
              initValue="Lựa chọn"
              onChange={(option)=>this.setState({did: option.key})}/>              
            <TouchableOpacity onPress={this.duyetsl}>
              <View style={styles.button}>
                <Text>Thực hiện</Text>
              </View> 
            </TouchableOpacity>
            <TouchableOpacity onPress={this.dong}>
              <View style={styles.button}>
                <Text>Bỏ qua</Text>
              </View> 
            </TouchableOpacity>
          </View>
        </Modal>       
      <View style={{
    height:35,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CFE3E2',
    borderWidth: 0.5
  }}>
    <TouchableOpacity style={styles.btback} onPress={() => navigate(this.state.screen)}>
      <Image style={styles.icon_alarm} source={back} />
    </TouchableOpacity>
    <View style={{flex:5, height:35, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}}><Text style={styles.txt_tieude}>Tên chỉ tiêu</Text></View>
    <View style={{flex:1, height: 35,alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>TH Kỳ</Text></View>
    <View style={{flex:1, height: 35, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Số liệu năm</Text></View>
    <View style={{flex:1, height: 35, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Đơn vị</Text></View>
      </View>
        <FlatList
  data={this.state.data}
  renderItem={({item}) => 
  <TouchableHighlight>
  <View style={{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }}>
    <View style={{borderBottomWidth: 0.3,flexDirection: 'row' }}>
    <View style={styles.cot_tieuchi}>
      {item.LCT_ID === 1 ?  <Image source={folder} style={styles.iconfolder0} />: null}
      {item.LCT_ID === 2 ?  <Image source={folder} style={styles.iconfolder1} />: null}
      {item.LCT_ID === 3 ?  <Image source={folder} style={styles.iconfolder2} />: null}
      {item.LCT_ID === 4 ?  <Image source={folder} style={styles.iconfolder3} />: null}
      {item.LCT_ID === 5 ?  <Image source={folder} style={styles.iconfolder4} />: null}
      {item.LCT_ID === 6 ?  <Image source={file} style={styles.iconfile1} />: null}
      {item.LCT_ID === 7 ?  <Image source={file} style={styles.iconfile2} />: null}
      <Text style={{marginRight: 50 ,fontSize: 15, }}>{item.CT_TEN}</Text>
    </View>
      <View style={styles.cot_solieu} ><Text style={styles.size_chu}>{item.DVT_TEN != null ? item.THK : null}</Text></View>
      <View style={styles.cot_solieu} ><Text style={styles.size_chu}>{item.DVT_TEN != null ? item.THKN : null}</Text></View>
      <View style={styles.cot_donnvi} ><Text style={styles.size_chu}>{item.DVT_TEN}</Text></View>
    </View>
  </View>
  </TouchableHighlight>
  }
/>
        {mainJSX}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  cot_tieuchi:{
    flex:5, 
    minHeight: 50,
    // justifyContent: 'center',
    alignItems:'center',
    borderRightWidth: 0.3,
    flexDirection: 'row',
  },
  cot_solieu:{
    flex:1,
    minHeight: 50,
    borderRightWidth: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cot_donnvi:{
    flex:1, 
    minHeight: 50,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 0.2
  },
  iconfile1:{
    width: 16,
    height: 16,
    marginLeft: 35,
    // marginTop: 5,
    marginRight: 5
  },
  iconfile2:{
    width: 16,
    height: 16,
    marginLeft: 40,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder0:{
    width: 16,
    height: 16,
    marginLeft: 5,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder1:{
    width: 16,
    height: 16,
    marginLeft: 15,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder2:{
    width: 16,
    height: 16,
    marginLeft: 20,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder3:{
    width: 16,
    height: 16,
    marginLeft: 25,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder4:{
    width: 16,
    height: 16,
    marginLeft: 30,
    // marginTop: 5,
    marginRight: 5
  },
  icon_alarm:{
    height: 30,
    width: 30,
  },
  btback: {
    zIndex: 2,
    alignItems: 'center',
    position: 'absolute',
    left: 3,
    top: 3,
  },
  txt_tieude:{
    color: 'blue', 
    fontSize: 13, 
    fontWeight: 'bold' 
  },
  size_chu:{
    fontSize: 15, 
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  combobox: { 
    height: 50,
    width: 300 
  },
  icon_check:{
    height: 50,
    width: 50,
  },
  btluuh: {
    alignItems: 'center',
    position: 'absolute',
    right: 5,
    bottom: 5,
  }
});
