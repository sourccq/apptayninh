import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    Button,SafeAreaView
} from 'react-native';

export default class Header extends Component {
    static navigationOptions = {
        drawerLabel: 'Header',
        // drawerIcon: () => (
        //   <Image
        //     source={require('./notif-icon.png')}
        //   />
        // ),
      };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <SafeAreaView style={st.thanhtrai}>
                <View style={st.thanh1}>
                    <View style={{ width: 35, height: 35, justifyContent: 'center' }}>
                        {/* <TouchableOpacity onPress={() => navigate('DrawerOpen')}>
                            <Image style={st.icon_thanh} source={require('../resources/icons-menu-filled.png')} />
                        </TouchableOpacity> */}
                        {/* <Button
                            onPress={() => this.props.navigation.navigate('DrawerOpen')}
                            title="Open Navigation"
                        /> */}
                    </View>
                    <View style={st.thanhphai}>
                        <TouchableOpacity onPress={() => navigate('Login')}>
                            <Image style={st.icon_thanh} source={require('../img/large-icon-user.png')} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigate('Home')}>
                            <Image style={st.icon_thanh} source={require('../img/home.png')} />
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}
const st = StyleSheet.create({
    thanh1: {
        flexDirection: 'row',
        padding: 0,
        backgroundColor: 'red',
        borderBottomWidth: 1,
        justifyContent: 'space-between',
    },
    thanhphai: {
        flexDirection: 'row',
        padding: 0,
        justifyContent: 'flex-end',
    },
    thanhtrai: {

    },
    icon_thanh: {
        width: 35,
        height: 35,
        marginTop: 10,
        marginRight: 10,
        marginLeft: 5,
    }
});


AppRegistry.registerComponent('header', () => Header);
