/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Image,
  TouchableOpacity,SafeAreaView
} from 'react-native';
import Modal from "react-native-modal";
import ModalSelector from 'react-native-modal-selector';

import folder from '../../assets/folder.png';
import file from '../../assets/file.png';
import backbt from '../../assets/back.png';

export default class ThongKeDP extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      date: new Date(),
      data: [],
      modalVisible: true,
      songanh: [],
      dv: '',
      kybc: [],
      ky: '',
      nam: [],
      nid: ''
    };
  }
  timkiem = () =>{
    this.setState({modalVisible: true});
  }
  componentDidMount() {      
    const urlnam = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiNam`;
    fetch(urlnam)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          nam: responseJson,
          nid:responseJson[0]['ten']
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
    const urlsn = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiDiaPhuong?nam=${this.state.date.getFullYear()}`;
    fetch(urlsn)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          songanh: responseJson,
          dv:responseJson[0]['DV_ID']
        });
        this.kybaocao(responseJson[0]['DV_ID']);
      })
      .catch((err) => {
        console.log('Loi');
      });
  }
  donvi = (nbc) => {
    this.setState({nid: nbc});
    const urlsn = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiDiaPhuong?nam=${nbc}`;
    fetch(urlsn)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          songanh: responseJson
        });        
      })
      .catch((err) => {
        console.log('Loi');
      });
  }
  kybaocao = (dvid) => {
    this.setState({dv: dvid});
    const urlkbc = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiKyBaoCao?year=${this.state.nid}&dv=${dvid}`;
    fetch(urlkbc)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          kybc: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
  }
  xemtk = () =>{
    this.setState({ modalVisible: false });
    const url = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiSoLieuTongHop?LSL_ID=4STANDARD&DV_ID=${this.state.dv}&SL_NAM=${this.state.nid}&KBC_ID=${this.state.ky}`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });   
      console.log(url); 
  }
  render() {     
    return (
      <SafeAreaView style={styles.container}>
        <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.setState({ modalVisible: null })}>
          <View style={styles.modalContent}>
          <ModalSelector
              style={styles.combobox}
              data={this.state.nam}
              initValue="Chọn năm"
              keyExtractor= {item => item.id}
              labelExtractor= {item => item.ten}
              onChange={(option)=>this.donvi(option.id)}/> 
          <ModalSelector
              style={styles.combobox}
              data={this.state.songanh}
              initValue="Chọn đơn vị"
              keyExtractor= {item => item.DV_ID}
              labelExtractor= {item => item.DV_TEN}
              onChange={(option)=>this.kybaocao(option.DV_ID)}/> 
          <ModalSelector
              style={styles.combobox}
              data={this.state.kybc}
              initValue="Chọn kỳ báo cáo"
              keyExtractor= {item => item.id}
              labelExtractor= {item => item.ten}
              onChange={(option)=>this.setState({ky: option.id})}/>            
            <TouchableOpacity onPress={this.xemtk}>
              <View style={styles.button}>
                <Text>Xem</Text>
              </View> 
            </TouchableOpacity>
          </View>
        </Modal>       
      <View style={{
    height:60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#CFE3E2',
    borderWidth: 0.5
  }}>
    <TouchableOpacity style={styles.btluuh} onPress={() => this.props.navigation.navigate('HomeKTXH')}>
      <Image style={styles.icon_alarm} source={backbt} />
    </TouchableOpacity>
    <View style={{flex:4, height: 60, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}}><Text style={styles.txt_tieude}>Tên chỉ tiêu</Text></View>
    <View style={{flex:1, height: 60,alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>So với năm trước (%)</Text></View>
    <View style={{flex:1, height: 60, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Thực hiện</Text><Text style={styles.txt_tieude}>{this.state.date.getFullYear()-1}</Text></View>
    <View style={{flex:1, height: 60, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Số thực hiện</Text></View>
    <View style={{flex:1, height: 60, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Đơn vị</Text></View>
      </View>
        <FlatList
  data={this.state.data}
  renderItem={({item}) => 
  <TouchableHighlight>
  <View style={{
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }}>
    <View style={{borderBottomWidth: 0.3,flexDirection: 'row' }}>
    <View style={styles.cot_tieuchi}>
      {item.LCT_ID === 1 ?  <Image source={folder} style={styles.iconfolder0} />: null}
      {item.LCT_ID === 2 ?  <Image source={folder} style={styles.iconfolder1} />: null}
      {item.LCT_ID === 3 ?  <Image source={folder} style={styles.iconfolder2} />: null}
      {item.LCT_ID === 4 ?  <Image source={folder} style={styles.iconfolder3} />: null}
      {item.LCT_ID === 5 ?  <Image source={file} style={styles.iconfile1} />: null}
      {item.LCT_ID === 6 ?  <Image source={file} style={styles.iconfile2} />: null}
      <Text style={{marginRight: 50 ,fontSize: 15, }}>{item.CT_TEN}</Text>
    </View>
      <View style={styles.cot_solieu} ><Text style={styles.size_chu}>{item.DVT_TEN != null ? item.SLK_GIATRI ==='0' ? (parseFloat(item.BCSLK_GIATRI)/parseFloat(item.SLK_GIATRI))*100: 0 : null}</Text></View>
      <View style={styles.cot_solieu} ><Text style={styles.size_chu}>{item.DVT_TEN != null ? item.SLK_GIATRI : null}</Text></View>
      <View style={styles.cot_solieu} ><Text style={styles.size_chu}>{item.DVT_TEN != null ? item.BCSLK_GIATRI : null}</Text></View>
      <View style={styles.cot_donnvi} ><Text style={styles.size_chu}>{item.DVT_TEN}</Text></View>
    </View>
  </View>
  </TouchableHighlight>
  }
/>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  cot_tieuchi:{
    flex:4, 
    minHeight: 50,
    // justifyContent: 'center',
    alignItems:'center',
    borderRightWidth: 0.3,
    flexDirection: 'row',
  },
  cot_solieu:{
    flex:1,
    minHeight: 50,
    borderRightWidth: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cot_donnvi:{
    flex:1, 
    minHeight: 50,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 0.2
  },
  iconfile1:{
    width: 16,
    height: 16,
    marginLeft: 25,
    // marginTop: 5,
    marginRight: 5
  },
  iconfile2:{
    width: 16,
    height: 16,
    marginLeft: 30,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder0:{
    width: 16,
    height: 16,
    marginLeft: 5,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder1:{
    width: 16,
    height: 16,
    marginLeft: 15,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder2:{
    width: 16,
    height: 16,
    marginLeft: 20,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder3:{
    width: 16,
    height: 16,
    marginLeft: 25,
    // marginTop: 5,
    marginRight: 5
  },
  icon_alarm:{
    marginTop:10,
    height: 35,
    width: 35,
  },
  btluuh: {
    zIndex: 2,
    alignItems: 'center',
    position: 'absolute',
    left: 3,
    top: 3,
  },
  txt_tieude:{
    color: 'blue', 
    fontSize: 13, 
    fontWeight: 'bold' 
  },
  size_chu:{
    fontSize: 15, 
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  combobox: { 
    height: 50,
    width: 300 
  }
});
