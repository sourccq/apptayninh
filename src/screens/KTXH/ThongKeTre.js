/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableHighlight,
  Image,
  TouchableOpacity,SafeAreaView
} from 'react-native';
import Modal from "react-native-modal";
import ModalSelector from 'react-native-modal-selector';

import folder from '../../assets/folder.png';
import file from '../../assets/file.png';
import backbt from '../../assets/back.png';

export default class ThongKeTre extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      modalVisible: true,
      nam: [],
      nid: '',
      loai: 0
    };
  }
  timkiem = () =>{
    this.setState({modalVisible: true});
  }
  componentDidMount() {      
    const urlnam = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiNam`;
    fetch(urlnam)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          nam: responseJson,
          nid:responseJson[0]['id']
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
  }
  xemtk = () =>{
    this.setState({ modalVisible: false });
    const url = `https://ktxh.tayninh.gov.vn/apiservice.asmx/TKTre?nam=${this.state.nid}&loai=${this.state.loai}`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          data: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });   
  }
  render() {     
    const duyet = [
        { key: 1, label: 'Sở, ban ngành' },
        { key: 0, label: 'UBND Huyện' }
      ];
    return (
      <SafeAreaView style={styles.container}>
        <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.setState({ modalVisible: null })}>
          <View style={styles.modalContent}>   
            <ModalSelector
                style={styles.combobox}
                data={this.state.nam}
                initValue="Chọn năm"
                keyExtractor= {item => item.id}
                labelExtractor= {item => item.ten}
                onChange={(option)=>this.setState({nid: option.id})}/>  
            <ModalSelector
                style={styles.combobox}
                data={duyet}
                initValue="Lựa chọn"
                onChange={(option)=>this.setState({loai: option.key})}/>               
            <TouchableOpacity onPress={this.xemtk}>
              <View style={styles.button}>
                <Text>Xem</Text>
              </View> 
            </TouchableOpacity>
          </View>
        </Modal>       
      <View style={{
        height:50,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CFE3E2',
        borderWidth: 0.5
      }}>
        <TouchableOpacity style={styles.btluuh} onPress={() => this.props.navigation.navigate('HomeKTXH')}>
          <Image style={styles.icon_alarm} source={backbt} />
        </TouchableOpacity>
        <View style={{flex:5, height: 50, alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}}><Text style={styles.txt_tieude}>Đơn vị</Text></View>
        <View style={{flex:1, height: 50,alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Nộp báo cáo</Text></View>
        <View style={{flex:1, height: 50,alignItems: 'center',justifyContent: 'center',borderRightWidth: 0.3}} ><Text style={styles.txt_tieude}>Trễ</Text></View>
          </View>
            <FlatList
      data={this.state.data}
      renderItem={({item}) => 
      <TouchableHighlight>
      <View style={{
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <View style={{borderBottomWidth: 0.3,flexDirection: 'row' }}>
        <View style={styles.cot_tieuchi}>
          {item.cap === "1" ?  <Image source={folder} style={styles.iconfolder0} />: null}
          {item.cap === "2" ?  <Image source={file} style={styles.iconfile1} />: null}
          <Text style={{marginRight: 50 ,fontSize: 15, }}>{item.ten}</Text>
        </View>
          <View style={styles.cot_solieu} ><Text style={styles.size_chu}>{item.nop != "0" ? 'X': '' }</Text></View>
          <View style={styles.cot_solieu} ><Text style={styles.size_chu2}>{item.tre != "0" ? 'X': '' }</Text></View>
        </View>
      </View>
      </TouchableHighlight>
      }
    />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  cot_tieuchi:{
    flex:5, 
    minHeight: 50,
    // justifyContent: 'center',
    alignItems:'center',
    borderRightWidth: 0.3,
    flexDirection: 'row',
  },
  cot_solieu:{
    flex:1,
    minHeight: 50,
    borderRightWidth: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cot_donnvi:{
    flex:1, 
    minHeight: 50,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 0.2
  },
  iconfile1:{
    width: 16,
    height: 16,
    marginLeft: 35,
    // marginTop: 5,
    marginRight: 5
  },
  iconfile2:{
    width: 16,
    height: 16,
    marginLeft: 40,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder0:{
    width: 16,
    height: 16,
    marginLeft: 5,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder1:{
    width: 16,
    height: 16,
    marginLeft: 15,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder2:{
    width: 16,
    height: 16,
    marginLeft: 20,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder3:{
    width: 16,
    height: 16,
    marginLeft: 25,
    // marginTop: 5,
    marginRight: 5
  },
  iconfolder4:{
    width: 16,
    height: 16,
    marginLeft: 30,
    // marginTop: 5,
    marginRight: 5
  },
  icon_alarm:{
    marginTop:5,
    height: 35,
    width: 35,
  },
  btluuh: {
    zIndex: 2,
    alignItems: 'center',
    position: 'absolute',
    left: 3,
    top: 3,
  },
  txt_tieude:{
    color: 'blue', 
    fontSize: 13, 
    fontWeight: 'bold' 
  },
  size_chu:{
    fontSize: 15, 
  },
  size_chu2:{
    fontSize: 15,
    color: 'red' 
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  combobox: { 
    height: 50,
    width: 300,
  }
});
