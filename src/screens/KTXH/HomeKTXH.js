import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  SafeAreaView
} from 'react-native';
import Modal from "react-native-modal";
import getToken from '../../api/getToken';
import checkToken from '../../api/checkTokenKTXH';
import saveToken from '../../api/saveToken';
import { Icon } from 'native-base';

export default class HomeKTXH extends React.Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    this.state = {
      tb: 0,
      id: false,
      modalVisible: false,
    };
  }
  componentDidMount() {
    //get thong tin dang nhap
    setTimeout(() => {
      getToken('ktxh')
        .then(token => {
          if (token.length > 0) {
            var paramsString = "token="+ token + "&duyet=-1&trang=0";
            const url = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiSoLieuChoDuyet`;
            this.setState({ loading: true });
            fetch(url, {
                method: 'POST',
                headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: paramsString
            })
            .then(res => res.json())
            .then(res => {
                this.setState({
                tb: res.length
                });
            })
            .catch(error => {
                console.log('Loi');
            });
          }
          checkToken(token)
            .then(res => {
              if (res.thongbao === 'dung') {
                //global.onSignIn(res);
                this.setState(previousState => {
                  return { id: res };
                });
                // this.setState({ id: res });
              } else {
                this.setState(previousState => {
                  return { id: false };
                });
              }
              this.setState({ tendv: res.tendv });
            })
            .catch(err => console.log('LOI CHECK lỗi __________', err));
        })
    }, 100);
  }
  componentWillReceiveProps() {
    getToken('ktxh')
      .then(token => checkToken(token))
      .then(res => {
        if (res.thongbao === 'dung') {
          this.setState(previousState => {
            return { id: res };
          });
        } else {
          this.setState(previousState => {
            return { id: false };
          });
        }
      })
      .catch(err => console.log('LOI CHECK lỗi __________', err));
  }
  onSignOut() {
    getToken('ktxh')
    .then(token => {  
      const paramsString = `token=${token}`;
      fetch('https://ktxh.tayninh.gov.vn/apiservice.asmx/deletetoken', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
      },
      body: paramsString
      });    
    })
    .catch(err => console.log('LOI xoa token', err)); 
    this.setState({ id: false });
    this.setState({ active: 'Home' });
    this.setState({ modalVisible: false });
    saveToken('ktxh','');
    this.props.navigation.navigate('Home', {islogin: false});
    console.log('+++++++++++');
    console.log();
  }
  mo = () =>{
    this.setState({modalVisible: true});
  }
  doimk = () =>{
    this.setState({modalVisible: false});
    this.props.navigation.navigate('ChangePasswordKTXH');
  }
  render() {
    const { id } = this.state;
    const { navigate } = this.props.navigation;
    const viewbtnLogin = (
      <View style={st.thanhphai}>
        <TouchableOpacity onPress={() => navigate('LoginKTXH')}>
          <Image style={st.icon_thanh} source={require('../../assets/large-icon-user.png')} />
        </TouchableOpacity>
      </View>
    );
    const viewbtnUser = (
      <View style={{ flex: 1 }}>
        <TouchableOpacity onPress={this.mo}>
          <Text style={st.text_thanh}>{id ? id.Ten : ''}</Text>
        </TouchableOpacity>        
      </View>
    );

    const mainJSX = this.state.id ? viewbtnUser : viewbtnLogin;
    return (
      <SafeAreaView
      style={{ flex: 1 }}>
        <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.setState({ modalVisible: null })}>
          <View style={st.modalContent}>            
            <TouchableOpacity onPress={this.doimk}>
              <View style={st.button}>
                <Text>Đổi mật khẩu</Text>
              </View> 
            </TouchableOpacity>
            <TouchableOpacity onPress={this.onSignOut.bind(this)}>
              <View style={st.button}>
                <Text>Thoát</Text>
              </View> 
            </TouchableOpacity>
          </View>
        </Modal>
        <View style={{ flex: 1, justifyContent: 'space-between' }}>

          <View >
            <View style={st.thanh1}>
              <View style={{ width: 35, height: 35, justifyContent: 'center', zIndex: 1 }}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Home')}>
                  <Icon style={st.icon_menu} name="md-home" />
                </TouchableOpacity>
              </View>
              {mainJSX}
            </View>
            <View style={st.thanh2}>
              <Image style={st.icon_logo} source={require('../../assets/HKG/logotayninh.png')} />
            </View>
            <View style={st.thanh2}>
              <Text style={{ color: 'blue', fontSize: 11, fontWeight: 'bold' }}>{this.state.tendv}</Text>
            </View>
            <View style={st.thanh2}>
              <Text style={{ color: 'blue', fontSize: 11, fontWeight: 'bold' }}>HỆ THỐNG BÁO CÁO CHỈ TIÊU KINH TẾ XÃ HỘI</Text>
            </View>
          </View>

          <View >
            <View style={st.thanh2}>
              <TouchableOpacity onPress={() => navigate('ToanTinh',{a:'s'})}>
                <Image style={st.icon} source={require('../../assets/Toantinh.png')} />
                <Text style={st.textDS}>Số Liệu</Text>
                <Text style={st.textDS2}>Toàn Tỉnh</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('ThongKe',{a:'s'})}>
                <Image style={st.icon} source={require('../../assets/Nganh.png')} />
                <Text style={st.textDS}>Số Liệu</Text>
                <Text style={st.textDS2}>Ngành</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('ThongKeDP',{a:'s'})}>
                <Image style={st.icon} source={require('../../assets/DiaPhuong.png')} />
                <Text style={st.textDS}>Số Liệu</Text>
                <Text style={st.textDS2}>Địa Phương</Text>
              </TouchableOpacity>              
            </View>
            <View style={st.thanh2}>
              <TouchableOpacity onPress={() => navigate('ThongKeTre',{a:'s'})}>
                <Image style={st.icon} source={require('../../assets/file.png')} />
                <Text style={st.textDS}>Thống Kê</Text>
                <Text style={st.textDS2}>Nộp Báo Cáo</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('DSBaoCao',{duyet:-1})}>
                <Image style={st.icon} source={require('../../assets/XemDuyetBapCao.png')} />
                <Text style={st.textDS}>Duyệt Số Liệu</Text>
                <Text style={st.tbnb}>{this.state.tb}</Text>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('DSBaoCao',{duyet:-2})}>
                <Image style={st.icon} source={require('../../assets/DanhSachBaoCao.png')} />
                <Text style={st.textDS}>Danh Sách</Text>
                <Text style={st.textDS2}>Báo Cáo</Text>
              </TouchableOpacity>              
            </View>
          </View>
          <View >
            <View style={st.footer}>
              <Text></Text>
              <Text></Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}
const st = StyleSheet.create({
  thanh1: {
    flexDirection: 'row',
    padding: 0,
    backgroundColor: '#94c3bf',
    // height: 55,
    justifyContent: 'space-between',
    paddingTop:0,
  },
  thanhphai: {
    flexDirection: 'row',
    padding: 0,
    // backgroundColor: 'red',
    // borderBottomWidth: 1,
    // height: 55,
    justifyContent: 'flex-end',
  },
  thanhtrai: {
    // flexDirection: 'row',
    // padding: 0,
    // backgroundColor: 'red',
    // borderBottomWidth: 1,
    // height: 55,
    // justifyContent: 'flex-end',
  },
  thanh2: {
    flexDirection: 'row',
    justifyContent: 'center',
    padding: 0,
  },
  icon: {
    width: 65,
    height: 65,
    marginTop: 15,
    marginRight: 25,
    marginLeft: 25,
    marginBottom: 1
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginRight: 10,
    marginTop: 5,
    marginBottom: 3,
  },
  text_thanh: {
    marginTop: 10,
    marginBottom: 8,
    color: '#FFF',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: -35,
    zIndex: 0,
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginLeft: 10,
    marginTop: 5,
    marginBottom: 3,
    color: 'white'
  },
  icon_logo: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 5,
    marginTop: 10,
    height: 120,
    width: 120,
  },
  textDS: {
    marginTop: 0,
    marginLeft: 0,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 10,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },
  tbnb: {
    width: 20,
    height: 20,
    marginTop: -85,
    marginLeft: 75,
    color: 'white',
    backgroundColor: '#94c3bf',
    fontWeight: 'bold',
    fontSize: 10,
    borderRadius: 10,
    overflow: 'hidden',
    textAlign: 'center',
    paddingTop: 3,
  },
  textDS2: {
    marginTop: 0,
    marginLeft: 0,
    color: 'blue',
    fontWeight: 'bold',
    fontSize: 10,
    justifyContent: 'center',
    textAlign: 'center'
  },
  footer: {
    marginTop: 20,
    marginBottom: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  button: {
    backgroundColor: 'lightblue',
    padding: 12,
    margin: 16,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});
