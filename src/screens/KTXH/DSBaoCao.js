import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  Image,Alert,SafeAreaView,
  ActivityIndicator
} from 'react-native';
import getToken from '../../api/getToken';

const newLocal = true;

export default class DetailMeeting extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    //console.log(params);
    this.state = {
        duyet: params.duyet,
        data: null,
    };
  }
  componentDidMount() {
    this.getdata(); 
  }
  getdata = () => {
    getToken('ktxh')
    .then(token => {      
      if(token.length > 0){
        var paramsString = "token="+ token + "&duyet="+ this.state.duyet +"&trang=0";
        const url = `https://ktxh.tayninh.gov.vn/apiservice.asmx/HienThiSoLieuChoDuyet`;
        this.setState({ loading: true });
        fetch(url, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
        })
        .then(res => res.json())
        .then(res => {
            this.setState({
            data: res.sort(function(a,b){return b.BC_ID - a.BC_ID})
            });
        })
        .catch(error => {
            console.log('Loi');
        });
    } else {
        Alert.alert(
          'Thông báo',
          'Bạn cần đăng nhập trước để xem',
          [
            { text: 'OK', onPress: () => this.props.navigation.navigate('HomeKTXH')},
          ]
        );
      }
      
    })
  }
  render() {
    const { navigate } = this.props.navigation;    
    if (!this.state.data) {
      return (
        <ActivityIndicator
          animating={true}
          style={st.indicator}
          size="large"
          color="#94c3bf"
        />
      );
    }
    return (
      <SafeAreaView style={{flex:1}}>
        <View style={st.thanh1}>
          <View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1 }}>
            <TouchableOpacity onPress={() => navigate('HomeKTXH')}>
            <Image style={st.icon_menu} source={require('../../assets/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{marginTop:5,flex:1}}>
            <Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Danh sách báo cáo</Text>
          </View>
        </View>
        <View style={st.container}>
            <FlatList
                data={this.state.data}
                renderItem={({ item }) =>
                <TouchableOpacity onPress={() => navigate('DuyetCT', { idbc: item.BC_ID , tenbc: item.BC_NGUOIGUI, duyet: item.BC_PHEDUYET ,screen: 'DSBaoCao'})}>
                    <View style={st.bao}>
                        <View style={st.bt}>
                            <View style={st.nd}>
                            <View style={st.bnd}>
                                <Text style={item.BC_PHEDUYET =='1' ? st.td : item.BC_PHEDUYET =='0' ? st.tdd : st.tdb}>{item.BC_TIEUDE}</Text>
                            </View>
                            </View>
                        </View>
                        <View style={st.bct}>
                        <View style={st.ct}>
                            <View style={st.ct2}>
                            <View style={st.ct3}>
                                <Text style={st.txct}>Số/Ký hiệu: {item.BC_SOHIEU}</Text>
                                <Text style={st.txct}>Ngày gửi: {item.NgayGui}</Text>
                            </View>
                            <View style={st.ct3}>
                                <Text style={st.txct}>Kỳ báo cáo: {item.KBC_ID1}</Text>
                                <Text style={st.txct}>Trạng thái: {item.BC_PHEDUYET =='1' ? 'Đã duyệt' : item.BC_PHEDUYET =='0' ? 'Không duyệt' : 'Chờ duyệt'}</Text>
                            </View>
                            </View>
                            <View>
                            <Text style={st.txct}>Đơn vị gửi BC: {item.BC_NGUOIGUI}</Text>
                            </View>
                        </View>
                        </View>
                    </View>  
                </TouchableOpacity> 
                }
            />
        </View>
      </SafeAreaView>
    );
  }
}

const st = StyleSheet.create({
  indicator: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    height: 80,
  },
  container: {
    flex: 1,
  },
  icon: {
    width: 25,
    height: 25
  },
  bao: {
    flexDirection: 'column',
    padding: 5,
    borderBottomWidth: 0.5
  },
  bct: {
    flexDirection: 'row',
  },
  bt: {
    flexDirection: 'row',
  },
  nd: {
    flex:1,
    marginLeft: 5,
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  tdb: {
    fontWeight: 'bold',
    color: 'blue'
  },
  tdd: {
    fontWeight: 'bold',
    color: 'red'
  },
  ct: {
    marginTop: 0,
    flexDirection: 'column',
    flex: 1
  },
  ct2: {
    flexDirection: 'row',

  },
  ct3: {
    flex: 2,
  },
  txct: {
    marginLeft: 5,
    marginTop: 5,
    color: 'black',
    fontSize:10
  },
  ngoai: {
    flexDirection: 'column',
    flex: 1
  },
  activetxna: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#ff9688',
    marginLeft: 1,
    marginRight: 1,
  },
  txnn: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#ec6060',
    marginLeft: 1,
    marginRight: 1,
  },
  thanh1: {
		backgroundColor: '#94c3bf',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
    icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
  }
});
