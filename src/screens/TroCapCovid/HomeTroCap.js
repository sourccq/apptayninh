import React, { Component } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  SafeAreaView,
  TouchableWithoutFeedback,
  ImageBackground,
} from "react-native";
import {
  Container,
  Button,
  Text,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Title,
  StyleProvider,
  Content,
  Grid,
  Col,
  Row,
  Input,
  Item,
  Form,
  Label,
  Footer,
  FooterTab,
  Spinner,
  Toast
} from "native-base";
import { FlatGrid,SectionGrid  } from "react-native-super-grid";
import dangkytrocap from "../../assets/trocap_icon/dangkytrocap.png";
import trocapdaphat from "../../assets/trocap_icon/trocapdaphat.png";
import thongketrocap from "../../assets/trocap_icon/thongketrocap.png";
import timkiemtrocap from "../../assets/trocap_icon/timkiemtrocap.png";
import canbocapphat from "../../assets/trocap_icon/canbocapphat.png";
import huongdansudung from "../../assets/trocap_icon/huongdansudung.png";

const { width: deviceWidth } = Dimensions.get("window");
const { height: deviceHeigh } = Dimensions.get("window");
const imageWidth = deviceWidth;
const imageHeight = deviceHeigh / 4;
const buttonWidth = deviceWidth / 3;

export default class HomeTroCap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tk: "",
      mk: "",
    };
  }
  componentDidMount() {}

  showToast(){
    Toast.show({
      text: "Chức năng này đang được phát triển",
      buttonText: "OK",
      type: "danger" ,
      position: "bottom"
    })
  }

  render() {
    const items = [
      {
        name: "Đăng ký trợ cấp",
        code: "#DC6201",
        image: dangkytrocap,
        screen: "DangKyTroCap",
        active:true
      },
      {
        name: "Tra cứu", 
        code: "#E05B49",
        image: timkiemtrocap,
        screen: "TraCuuCongDan",
        active:true
      },
      {
        name: "Trợ cấp đã phát",
        code: "#01A78A",
        image: trocapdaphat,
        screen: "CongKhaiDaCap",
        active:true
      },
      {
        name: "Hướng dẫn",
        code: "#E27C3E",
        image: huongdansudung,
        screen: "HuongDanSuDung",
        active:true
      },
      {
        name: "Cấp phát",
        code: "#9245B1",
        image: canbocapphat,
        screen: "CheckLogin",
        active:true
      }, 
      {
        name: "Thống kê",
        code: "#FCB740",
        image: thongketrocap,
        screen: "Home",
        active:false
      }, 
    

        // { name: "NEPHRITIS", code: "#27ae60" },
        // { name: "BELIZE HOLE", code: "#2980b9" },
        // { name: "WISTERIA", code: "#8e44ad" }, 
      //   { name: "MIDNIGHT BLUE", code: "#2c3e50" },
      //   { name: "SUN FLOWER", code: "#f1c40f" },
      //   { name: "CARROT", code: "#e67e22" },
      //   { name: "ALIZARIN", code: "#e74c3c" },
      //   { name: "CLOUDS", code: "#ecf0f1" },
      //   { name: "CONCRETE", code: "#95a5a6" },
      //   { name: "ORANGE", code: "#f39c12" },
      //   { name: "PUMPKIN", code: "#d35400" },
      //   { name: "POMEGRANATE", code: "#c0392b" },
      //   { name: "SILVER", code: "#bdc3c7" },
      //   { name: "ASBESTOS", code: "#7f8c8d" },
    ];
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header hasSegment style={{ backgroundColor: "#4286f4", height: 40,paddingTop:0.1 }}>
          <Left style={{ flex: 1, paddingLeft: 10 }}>
            <Icon
              name="ios-home"
              style={{ color: "white" ,fontSize:35}}
              onPress={() => navigate("Home")}
            />
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ color: "white", alignSelf: "center" }}>
              {/* Đăng nhập */}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>

        <Content contentContainerStyle={{ flex: 1 }}>
          <ImageBackground
            source={require("../../assets/trongdong.jpg")}
            style={{ width: deviceWidth, height: deviceHeigh }}
            resizeMode="cover"
            //blurRadius={5} 
          >
            <Image
              style={styles.icon_menu}
              source={require("../../assets/home_icon/teamwork.png")}
            />
            <Text style={styles.heading}>HỆ THỐNG</Text>
            <Text style={styles.subHeading}>QUẢN LÝ TRỢ CẤP</Text>

            <SectionGrid
              itemDimension={deviceHeigh/8}
             
              sections={[
                {
                  title: 'Công dân',
                  data: items.slice(0, 4),
                },
                {
                  title: 'Cán bộ cấp phát',  
                  data: items.slice(4, 6),
                },
              ]}
              items={items}
              style={styles.gridView}
              //  staticDimension={300}
              // fixed
              // spacing={20}
              renderItem={({ item,section, index }) => (
                <TouchableOpacity onPress={() => item.active ? navigate(item.screen) :this.showToast()}>
                  <View 
                    style={[
                      styles.itemContainer,
                      { backgroundColor: item.code},
                    ]}
                  >
                    <Image
                      source={item.image}
                      style={[
                        {width: 60,
                        height: 60,
                        alignSelf: "center"},
                        item.active ? null : {opacity:0.2} 
                      ]}
                    />
                    <Text style={styles.itemName}>{item.name}</Text>
                  </View>
                </TouchableOpacity>
              )}
              renderSectionHeader={({ section }) => (
                <Text style={styles.sectionHeader}>{section.title}</Text>
              )}
            />
          </ImageBackground>
        </Content>
        {/* <Footer style={styles.footer}></Footer> */}
        <Footer hasSegment style={{ backgroundColor: "#4286f4", height: 40 }}>
          <Left></Left>
          <Body style={{ flex: 3, justifyContent: "center" }}>
            <Title style={{ color: "white", fontSize: 13, fontWeight: "bold" }}>
              CỔNG THÔNG TIN TỈNH TÂY NINH
            </Title>
          </Body>
          <Right></Right>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  icon_menu: {
    width: deviceHeigh/7,
    height: deviceHeigh/7,
    alignSelf: "center",
    // marginBottom: 5,
    // marginRight: 10,
    // marginLeft: 5,
    marginTop: 15,
  },

  
  heading: {
    fontSize: 25 , 
    fontWeight: "800",
    // marginBottom: 10,
    // marginTop: 10,
    alignSelf: "center",
    color: "#4286f4",
    fontFamily: "Roboto",
  },
  subHeading: {
    fontSize: 25,
    fontWeight: "800",
    // marginBottom: 10,
    marginTop: 5,
    alignSelf: "center",
    color: "#4286f4",
    fontFamily: "Roboto",
  },
  footer: {
    backgroundColor: "white",
    elevation: 3,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    height: 60,
    padding: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },

  gridView: {
    marginTop: 20,
    flex: 1, 
    flexDirection:"column",
    // marginBottom:120 
  }, 
  itemContainer: {
    justifyContent: "center",
    borderRadius: 10,
    padding: 10,
    height: deviceHeigh/8, 
  },
  itemName: { 
    fontSize: 12,
    color: "#fff",
    fontWeight: "500",
    alignSelf: "center",
  },
  itemCode: {
    fontWeight: "600",
    fontSize: 12,
    color: "#fff",
  },
  homeImage: {
    backgroundColor: "transparent",
    justifyContent: "flex-start",
    alignItems: "center",
    width: imageWidth,
    height: imageHeight,
  },
  textOverImg: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center",
    paddingTop: 20,
    fontFamily: "Roboto",
  },
  sectionHeader: {
    flex: 1,
    fontSize: 15,
    fontWeight: '600', 
    alignItems: 'center',
    backgroundColor: '#4286f4',
    color: 'white', 
    paddingLeft: 20, 
    paddingTop:5, 
    paddingBottom:5,  
    width:deviceWidth/2.5, 
    borderBottomRightRadius:20,
    borderTopRightRadius:20
   },
});
