import React, { Component } from "react";
import {
  StyleSheet,
  Dimensions,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Alert,
  SafeAreaView,
  TouchableWithoutFeedback,
} from "react-native";
import {
  Container,
  Button,
  Text,
  Header,
  Left,
  Body,
  Right,
  Icon,
  Title,
  StyleProvider,
  Content,
  Grid,
  Col,
  Row,
  Input,
  Item,
  Form,
  Label,
  Footer,
  FooterTab,
  Spinner,
} from "native-base";
import getToken from "../../api/getToken";
import DefaultPreference from "react-native-default-preference";
import checkDangNhapTroCapCovid from "../../api/checkDangNhapTroCapCovid";
import saveToken from "../../api/saveToken";
import dismissKeyboard from "react-native/Libraries/Utilities/dismissKeyboard";

var tkp = "";
DefaultPreference.get("ToKenPush").then(function (value) {
  tkp = value;
});

const { width } = Dimensions.get("window");
export default class LoginTroCap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tk: "",
      mk: "",
    };
  }
  componentDidMount() {}

  onSignIn() {
    const { tk, mk } = this.state;
    checkDangNhapTroCapCovid(tk, mk)
      .then((res) => {
        if (res.thongbao === "dung") {
          saveToken("covid", res.token);
          this.props.navigation.navigate("troCapCovidNavigator", {
            islogin: true,
          });
        }
        if (res.thongbao === "sai") {
          Alert.alert("Thông báo", "Sai thông tin đăng nhập", [
            { text: "OK", onPress: () => console.log("OK Pressed") },
          ]);
        }
      })
      .then()
      .catch((err) => {
        console.log("Lỗi");
        console.log(err);
      });
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container> 
        <Header hasSegment style={{ backgroundColor: "#4286f4",height:40 ,paddingTop:0.1 }}>
          <Left style={{ flex: 1, paddingLeft: 10 }}>
            <TouchableOpacity  onPress={() => navigate("HomeTroCap")}>
            <Icon
              name="md-arrow-back"
              style={{ color: "white", fontSize:35 }} 
             
            />
            </TouchableOpacity>
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={{ color: "white", alignSelf: "center" }}>
              {/* Đăng nhập  */}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>
        <Content padder style={styles.content}>
          <Image
            style={styles.icon_menu}
            source={require("../../assets/home_icon/trocapcovid.png")}
          />
          <Text style={styles.heading}>Đăng nhập hệ thống</Text>
          <Form>
            <Item>
              <Icon active name="ios-call" style={{ color: "#4286f4" }} />
              <Input
                placeholder="Số điện thoại"
                value={this.state.tk}
                onChangeText={(text) => this.setState({ tk: text })}
              />
            </Item>
            <Item>
              <Icon active name="ios-eye" style={{ color: "#4286f4" }} />
              <Input
                secureTextEntry={true}
                placeholder="Mật khẩu"
                value={this.state.mk}
                onChangeText={(text) => this.setState({ mk: text })}
              />
            </Item>
          </Form>

          <Button
            iconLeft
            primary
            style={{
              marginTop: 25,
              backgroundColor: "#4286f4",
              alignSelf: "center",
            }}
            onPress={this.onSignIn.bind(this)}
          >
            <Icon name="ios-log-in" />
            <Text>Đăng nhập</Text>
          </Button>
        </Content>
        {/* <Footer style={styles.footer}></Footer> */}
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    alignItems: "center",
    backgroundColor: "#f3f3f3",
  },
  icon: {
    // justifyContent: 'center',
    alignItems: "center",
    width: 140,
    height: 140,
    paddingTop: 15,
  },
  header: {
    marginTop: 25,
    alignItems: "center",
  },
  footer: {
    //flex: 1,
    alignItems: "center",
    justifyContent: "flex-end",
  },
  inputStyle: {
    height: 50,
    width: (width / 10) * 8,
    marginHorizontal: 1,
    backgroundColor: "#fff",
    marginBottom: 10,
    borderRadius: 20,
    paddingLeft: 30,
  },
  bigButton: {
    height: 50,
    borderRadius: 20,
    width: 200,
    backgroundColor: "#94c3bf",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    color: "#fff",
    fontWeight: "400",
  },
  thanh1: {
    backgroundColor: "#94c3bf",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingTop: 5,
  },
  icon_menu: {
    width: 80,
    height: 80,
    alignSelf: "center",
    // marginBottom: 5,
    // marginRight: 10,
    // marginLeft: 5,
  },
  // footer: {
  //   marginTop: 20,
  //   marginBottom: 10,
  //   alignItems: "center",
  //   justifyContent: "flex-end",
  // },
  topMargin: {
    marginTop: 25,
  },
  content: {
    padding: 10,
    backgroundColor: "white",
  },
  heading: {
    fontSize: 25,
    fontWeight: "bold",
    marginBottom: 30,
    marginTop: 25,
    alignSelf: "center",
    color: "#4286f4",
  },
  footer: {
    backgroundColor: "white",
    elevation: 3,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.2 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
    height: 60,
    padding: 5,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
