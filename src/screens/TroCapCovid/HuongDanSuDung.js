import React, { Component } from "react";
import { Image,Dimensions } from "react-native";
import {
  Container,
  Header,
  View,
  DeckSwiper,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Icon,
  Button,
  Title,
  Right,
  Content,
  Footer,
} from "native-base";


const { width: deviceWidth } = Dimensions.get("window");
const { height: deviceHeight } = Dimensions.get("window");

const cards = [
  {
    text: "I. Đăng ký trợ cấp",
    name: "Bước 1: Chọn chức năng đăng ký trợ cấp",
    image: require("../../assets/hdsd_trocap/buoc1.jpg"),
  }, 
  {
    text: "I. Đăng ký trợ cấp",
    name: "Bước 2: Điền đầy đủ thông tin đăng ký", 
    image: require("../../assets/hdsd_trocap/buoc2.jpg"),
  },
  {
    text: "I. Đăng ký trợ cấp",
    name: "Bước 3 + 4: Chụp ảnh hoặc tải lên bản đề nghị và chọn gửi",
    image: require("../../assets/hdsd_trocap/buoc34.jpg"),
  },
  {
    text: "I. Đăng ký trợ cấp",
    name: "Hệ thống thông báo gửi hồ sơ thành công",
    image: require("../../assets/hdsd_trocap/buoc5.jpg"),
  },
  {
    text: "II. Chức năng tra cứu",
    name: "Điền đầy đủ thông tin và bấm tra cứu",
    image: require("../../assets/hdsd_trocap/buoc6.jpg"),
  },
  {
    text: "III. Xem danh sách trợ cấp đã phát",
    name: "Điền đầy đủ thông tin và bấm xem",
    image: require("../../assets/hdsd_trocap/buoc7.jpg"),
  },
];
export default class HuongDanSuDung extends Component {
  render() {
    return (
      <Container>
        <Header
          searchBar
          style={{ backgroundColor: "#4286f4", height: 40, paddingTop: 0.1 }}
        >
          <Left>
            <Icon
              name="md-arrow-back"
              style={{ color: "white", marginLeft: 10 }}
              onPress={() => this.props.navigation.goBack()}
            />
          </Left>
          <Body>
            <Title style={{color:"white"}}>Hướng dẫn</Title>
          </Body>
          <Right></Right>
        </Header>
        <Content contentContainerStyle={{ flex: 1 }}>
          <DeckSwiper
            ref={(c) => (this._deckSwiper = c)}
            dataSource={cards}
            renderEmpty={() => (
              <View style={{ alignSelf: "center" }}>
                <Text>Trở về</Text>
              </View>
            )}
            renderItem={(item) => (
              <Card>
                <CardItem>
                  <Left>
                    {/* <Thumbnail source={item.image} /> */}
                    <Body>
                      <Title style={{alignSelf:"center", color:"black"}}>{item.text}</Title>
                      {/* <Text note>{item.name}</Text> */}
                    </Body>
                  </Left> 
                </CardItem>
                <CardItem cardBody>
                  <Image style={{ height: deviceHeight/1.5, flex: 1 }} source={item.image}  resizeMode={"contain"}/>
                </CardItem>
                <CardItem>
                  <Icon name="ios-book" style={{ color: "#ED4A6A" }} />
                  <Text>{item.name}</Text>
                </CardItem>
              </Card>
            )}
          />
        </Content>

        <Footer style={{ backgroundColor: "transparent" }}>
          <View
            style={{
              flexDirection: "row",
              flex: 1,
              position: "absolute",
              bottom: 5,
              left: 0,
              right: 0,
              justifyContent: "space-between",
              padding: 15,
            }}
          >
            <Button iconLeft onPress={() => this._deckSwiper._root.swipeLeft()}>
              <Icon name="ios-arrow-back" />
              <Text>Quay lại</Text>
            </Button>
            <Button
              iconRight
              onPress={() => this._deckSwiper._root.swipeRight()}
            >
              <Text>Tiếp theo</Text>
              <Icon name="ios-arrow-forward" />
            </Button>
          </View>
        </Footer>
      </Container>
    );
  }
}
