import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Image,
  Text,
  FlatList,
  Alert,
  Linking,
  Dimensions,
} from "react-native";
import {
  Container,
  Content,
  Icon,
  Thumbnail,
  Header,
  Title,
  Left,
  Right,
  Body,
  Spinner,
  List,
  ListItem,
  Card,
  CardItem,
  Button,
  ActionSheet,
  Root,
  Item,
  Input,
  Picker,
  Form,
} from "native-base";
import { withNavigationFocus } from "react-navigation";

const { width: deviceWidth } = Dimensions.get("window");
const { height: deviceHeigh } = Dimensions.get("window");
const imageWidth = deviceWidth;
const imageHeight = deviceHeigh * 0.3;
const imageWidth1 = (deviceWidth / 17) * 15;
const searchHeight = deviceHeigh * 0.1;
const searchWidth = deviceWidth * 0.6;

export default class CongKhaiDaCap extends Component {
  static navigationOptions = {
    header: null,
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
      refreshing: false,
      error: null,
      td: null,
      followings: null,
      feeds: null,
      onScroll: true,
      clicked: null,
      timkiem: null,
      iddt: null,
      hoten: null,
      diachi: null,
      iddt: null,
      sodt: null,
      sogt: null,
      iddvhuyen: null,
      iddvxa: null,
      iddvap: null,
      diachi: null,
      tinhtrang: null,
      ghichu: "",
      selected2: null,
      capHuyen: [],
      capXa: [],
      capAp: [],
      doiTuong: [],
      isSelectHoTen: false,
      isSelectSoGiayTo: false,
      isSelectSoDienThoai: false,
      selectedValue: null,
    };
  }

  componentDidMount() {
    this._layDanhSachHuyen();
  }

  makeRemoteRequest = (iddv) => {
    if (iddv != null) {
      const { page, seed } = this.state;
      const url = `http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/DS_NguoiDan?iddv=${iddv}&timkiem=&trang=${page}&token=`;
      this.setState({ loading: true });
      fetch(url)
        .then((res) => res.json())
        .then((res) => {
          this.setState({
            data: page === 0 ? res : [...this.state.data, ...res],
            error: res.error || null,
            loading: false,
            refreshing: false,
          });
        })
        .catch((error) => {
          this.setState({ error, loading: false });
        });
    } else Alert.alert("Hãy điền đầy đủ thông tin");
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 0,
        refreshing: true,
      },
      () => {
        this.makeRemoteRequest(this.state.iddvxa);
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => {
        this.makeRemoteRequest(this.state.iddvxa);
      }
    );
  };

  onValueChange(idHuyen) {
    this.setState({ iddvhuyen: idHuyen });
    this._layDanhSachXa(idHuyen);
  }
  onValueChange2(idXa) {
    this.setState({
      iddvxa: idXa,
    });
    this._layDanhSachAp(idXa);
  }
  onValueChange3(idAp) {
    this.setState({
      iddvap: idAp,
    });
  }

  _layDanhSachHuyen() {
    var paramsString = "";
    fetch("http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/Huyen", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      },
      body: paramsString,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ capHuyen: responseJson });
      });
  }

  _layDanhSachXa(idHuyen) {
    var paramsString = "mahuyen=" + idHuyen;
    fetch("http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/Xa", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      },
      body: paramsString,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ capXa: responseJson });
      });
  }

  _layDanhSachAp(idXa) {
    var paramsString = "maxa=" + idXa;
    fetch("http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/Ap", {
      method: "POST",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded;charset=UTF-8",
      },
      body: paramsString,
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({ capAp: responseJson });
      });
  }

  currencyFormat(num) {
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " đồng";
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header searchBar style={{ backgroundColor: "#4286f4", height: 40, paddingTop:0.1  }}>
          <Left>
            <Icon
              name="md-arrow-back"
              style={{ color: "white", marginLeft: 10 }}
              onPress={() => this.props.navigation.goBack()}
            />
          </Left>
          <Body>
            <Title style={{color:"white"}}>Đã nhận trợ cấp</Title>
          </Body>
          <Right></Right>
        </Header>
        <View
          // padder
          style={{ flex: 1, paddingLeft:10, paddingRight:10 }}
          // contentContainerStyle={{ flex: 1 }}
          removeClippedSubviews={false}
        >
          {/* Search feild */}

          {/* Search Result */}
          <FlatList
            style={{ marginTop: 20 }}
            ListHeaderComponent={() => (
              <>
                <Form
                  style={{
                    paddingLeft: 0,
                    paddingRight: 10,
                    paddingBottom: 10,
                  }}
                >
                  <Item Picker>
                    <Icon active name="ios-home" />
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.iddvhuyen}
                      onValueChange={this.onValueChange.bind(this)}
                    >
                      <Picker.Item label="Chọn huyện" value={null} />
                      {this.state.capHuyen.map((item, key) => {
                        return (
                          <Picker.Item
                            label={item.TenDonVi}
                            value={item.ma}
                            key={key}
                          />
                        );
                      })}
                    </Picker>
                  </Item>
                  <Item Picker>
                    <Icon active name="ios-map" />
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.iddvxa}
                      onValueChange={this.onValueChange2.bind(this)}
                    >
                      <Picker.Item label="Chọn xã" value={null} />
                      {this.state.capXa.map((item, key) => {
                        return (
                          <Picker.Item
                            label={item.TenDonVi}
                            value={item.ma}
                            key={key}
                          />
                        );
                      })}
                    </Picker>
                  </Item>

                  {/* <Item Picker>
                    <Icon active name="md-map" />
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="arrow-down" />}
                      style={{ width: undefined }}
                      selectedValue={this.state.iddvap}
                      onValueChange={this.onValueChange3.bind(this)}
                    >
                      <Picker.Item label="Chọn ấp" value={null} />
                      {this.state.capAp.map((item, key) => {
                        return (
                          <Picker.Item
                            label={item.TenDonVi}
                            value={item.ma}
                            key={key}
                          />
                        );
                      })}
                    </Picker>
                  </Item> */}
                </Form>
                <Button
                  block
                  last
                  style={{
                    color: "#4286f4",
                    width: "50%",
                    alignSelf: "center",
                  }}
                  onPress={() => this.makeRemoteRequest(this.state.iddvxa)}
                >
                  <Text style={{ color: "white" }}>Xem danh sách</Text>
                </Button>
              </>
            )}
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={0.5}
            data={this.state.data}
            renderItem={({ item }) => {
              if (item.SoTien != null) {
                return (
                  <Card>
                    <CardItem header>
                      <Left>
                        <Thumbnail
                          source={require("../../assets/home_icon/trocapcovid.png")}
                        />
                        <Body>
                          <Text
                            style={{
                              color: "#4286f4",
                              fontWeight: "bold",
                              fontSize: 18,
                            }}
                          >
                            {item.HoTen}
                          </Text>
                          <Text note>{item.DiaChi}</Text>
                        </Body>
                      </Left>
                    </CardItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-card"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>CMND: {item.SoGT}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-call"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>Điện thoại: {item.SoDT}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-briefcase"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>Nghề nghiệp: {item.NgheNghiep}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="logo-usd"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>Số tiền: {this.currencyFormat(item.SoTien)}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-time"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>
                          Thời gian phát: {item.Gio} ngày {item.Ngay}
                        </Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-clipboard"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>Đợt phát: {item.TenDot}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>
                  </Card>
                );
              }
            }}
          />
        </View>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  title: {
    fontSize: 15,
    color: "#ffff",
    alignSelf: "center",
  },
});
