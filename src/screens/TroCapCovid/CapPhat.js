import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  Alert,
  Platform,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import {
  Container,
  Textarea,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  Header,
  Left,
  Right,
  Body,
  Title,
} from "native-base";
import ImagePicker from "react-native-image-crop-picker";
import getToken from "../../api/getToken";

export default class CapPhat extends Component {
  static navigationOptions = {
    header: null,
  };
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      idnd: params.idnd,
      file: null,
      hoten: params.hoten,
      sotien: params.sotien,
      ghichu: "",
      token: null,
    };
  }
  componentDidMount() {
    getToken("covid").then((token) => {
      this.setState({ token: token });
    });
  }

  pickSingle(cropit, circular = false, mediaType) {
    ImagePicker.openCamera({
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: "MediumQuality",
      includeExif: true,
    })
      .then((image) => {
        this.setState({
          file: image,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }
  handleUploadPhoto = () => {
    if (this.state.file === null) {
      return Alert.alert("Hãy thêm hình ảnh kèm theo");
    }

    const data = new FormData();
    data.append("token", this.state.token);
    data.append("idd", "1");
    data.append("idnd", this.state.idnd); // you can append anyone.
    data.append("sotien", this.state.sotien);
    data.append("ghichu", this.state.ghichu);
    if (this.state.file != null) {
      data.append("filedinhkem", {
        name:
          Platform.OS === "android"
            ? this.state.file.path.substring(
                this.state.file.path.lastIndexOf("/") + 1
              )
            : this.state.file.filename,
        type: this.state.file.mime, // or photo.type
        uri:
          Platform.OS === "android"
            ? this.state.file.path
            : this.state.file.path.replace("file://", ""),
      });
      console.log("Pirnt_______________________");
      console.log(data);
    }
    fetch(
      `http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/Them_TroCap`,
      {
        method: "post", 
        body: data,
      }
    )
      .then((response) => response.json())
      .then((responseJson) => {
        if (responseJson.trangthai === "thanhcong") {
          Alert.alert("Thông báo", "Lưu thông tin thành công", [
            {
              text: "OK",
              onPress: () =>
                this.props.navigation.navigate("DanhSachChoTroCap"),
            },
          ]);
        } else {
          Alert.alert("Thông báo", "Có lỗi trong quá trình xử lý", [
            { text: "OK", onPress: () => console.log("OK Pressed") },
          ]);
        }
      });
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header
          style={{ backgroundColor: "#4286f4", height: 40, paddingTop: 0.1 }}
        >
          <Left style={{ flex: 1 }}>
            <Icon
              name="ios-arrow-back"
              style={{ color: "white", marginLeft: 10 }}
              onPress={() => navigate("DanhSachChoTroCap", { refresh: true })}
            />
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={st.title}>Cấp phát</Title>
          </Body>
          <Right style={{ flex: 1 }}></Right>
        </Header>
        <Content padder>
          <Form style={{ paddingLeft: 0, paddingRight: 10, paddingBottom: 10 }}>
            <Item>
              <Icon active name="ios-contact" />
              <Input
                disabled
                value={this.state.hoten}
                placeholder="Họ và tên"
              />
            </Item>
            <Item>
              <Icon active name="logo-usd" />
              <Input
                keyboardType="numeric"
                value={this.state.sotien}
                onChangeText={(text) => this.setState({ sotien: text })}
                placeholder="Số tiền"
              />
            </Item>
            <Item>
              <Icon active name="ios-create" />
              <Input
                value={this.state.ghichu}
                onChangeText={(text) => this.setState({ ghichu: text })}
                placeholder="Ghi chú"
              />
            </Item>
            <Button iconLeft transparent onPress={() => this.pickSingle(false)}>
              <Icon active name="md-camera" />
              <Text style={{ fontSize: 15, marginLeft: 10 }}>
                {this.state.file !== null
                  ? this.state.file.filename
                  : "Thêm hình ảnh"}
              </Text>
            </Button>
          </Form>
          <Button
            iconLeft
            block
            last
            style={{ marginTop: 5, alignSelf: "center", width: "50%" }}
            onPress={() => this.handleUploadPhoto()}
          >
            <Icon name="ios-send" />
            <Text style={{ color: "white" }}> Lưu thông tin</Text>
          </Button>
        </Content>
      </Container>
    );
  }
}
const st = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: "white",
  },
  thanh1: {
    backgroundColor: "#3771c3",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingTop: 5,
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginBottom: 5,
    marginRight: 10,
    marginLeft: 5,
  },
  lbtxt: {
    fontSize: 14,
  },
  title: {
    fontSize: 15,
    color: "#ffff",
    alignSelf: "center",
  },
});
