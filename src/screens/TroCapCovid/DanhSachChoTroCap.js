import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Image,
  Text,
  FlatList,
  Alert,
  Linking,
  Dimensions,
} from "react-native";
import {
  Container,
  Content,
  Icon,
  Thumbnail,
  Header,
  Title,
  Left,
  Right,
  Body,
  Spinner,
  List,
  ListItem,
  Card,
  CardItem,
  Button,
  ActionSheet,
  Root,
  Item,
  Input,
} from "native-base";
import getToken from "../../api/getToken";
import removeToken from "../../api/removeToken";
import { withNavigationFocus } from "react-navigation";

const { width: deviceWidth } = Dimensions.get("window");
const { height: deviceHeigh } = Dimensions.get("window");
const imageWidth = deviceWidth;
const imageHeight = deviceHeigh * 0.3;
const imageWidth1 = (deviceWidth / 17) * 15;
const searchHeight = deviceHeigh * 0.1;
const searchWidth = deviceWidth * 0.6;

export default class DanhSachChoTroCap extends Component {
  static navigationOptions = {
    header: null,
    // tabBarIcon: ({ focused, tintColor }) => {
    //   return <Icon name={"ios-exit"} size={30} style={{ color: tintColor }} />;
    // },
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      page: 0,
      refreshing: false,
      error: null,
      td: null,
      followings: null,
      feeds: null,
      onScroll: true,
      clicked: null,
    };
  }

  componentDidMount() {
    this.makeRemoteRequest();
    const { navigation } = this.props;
    this.focusListener = navigation.addListener("didFocus", () => {
      this.handleRefresh();
    });
  }
  componentWillUnmount() {
    this.focusListener.remove();
  }

  // componentDidUpdate(prevProps) {
  //   if (prevProps.isFocused !== this.props.isFocused) {
  //     // Use the `this.props.isFocused` boolean
  //     // Call any action
  //     if(this.props.isFocused){
  //       this.handleRefresh();
  //     }
  //   }
  // }

  makeRemoteRequest = () => {
    getToken("covid").then((token) => {
      const { page, seed } = this.state;
      const url = `http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/DS_Nhan?token=${token}&timkiem=&trang=${page}`;
      this.setState({ loading: true });
      fetch(url)
        .then((res) => res.json())
        .then((res) => {
          this.setState({
            data: page === 0 ? res : [...this.state.data, ...res],
            error: res.error || null,
            loading: false,
            refreshing: false,
          });
        })
        .catch((error) => {
          this.setState({ error, loading: false });
        });
    });
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 0,
        refreshing: true,
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1,
      },
      () => {
        this.makeRemoteRequest();
      }
    );
  };

  logOut() {
    removeToken("covid");
  }

  currencyFormat(num) {
    return num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,") + " đồng";
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <Container>
        <Header searchBar style={{ backgroundColor: "#4286f4", height: 40, paddingTop:0.1 }}>
          <Left>
            <Icon
              name="md-arrow-back"
              style={{ color: "white", marginLeft: 10 }}
              onPress={() => navigate("HomeTroCap")}
            /> 
          </Left>
          <Body>
            <Item style={{ width: searchWidth, borderBottomWidth: 0 }}>
              <Icon
                name="ios-search"
                style={{
                  color: "white",
                  paddingRight: 0,
                  paddingLeft: 0,
                  paddingTop: 4,
                  fontSize: 24,
                }}
              />
              <Input
                placeholder="Tìm kiếm nhanh"
                placeholderTextColor="#B6CAD1"
                style={{
                  fontSize: 14,
                  fontStyle: "italic",
                  fontFamily: "roboto",
                  paddingLeft: 6,
                  color: "#FFFFFF",
                }}
                onFocus={() => this.props.navigation.navigate("TimKiem")}
              />
            </Item>
          </Body>
          <Right>
            <Icon
              name="ios-log-out"
              style={{ color: "white", marginRight: 10 }}
              onPress={() => {
                this.logOut(), this.props.navigation.navigate("CheckLogin");
              }}
            />
          </Right>
        </Header>
        <Content
          padder
          style={{ flex: 1 }}
          contentContainerStyle={{ flex: 1 }}
          removeClippedSubviews={false}
        >
          <FlatList
            onRefresh={this.handleRefresh}
            refreshing={this.state.refreshing}
            onEndReached={this.handleLoadMore}
            onEndReachedThreshold={1}
            data={this.state.data}
            renderItem={({ item }) => {
              if (item.DaTroCap === false) {
                return (
                  <Card>
                    {/* <Icon active name="ios-contact" /> */}
                    <CardItem header>
                      <Left>
                        <Thumbnail
                          source={require("../../assets/home_icon/trocapcovid.png")}
                        />
                        <Body>
                          <Text
                            style={{
                              color: "#4286f4",
                              fontWeight: "bold",
                              fontSize: 18,
                            }}
                          >
                            {item.HoTen}
                          </Text>
                          <Text note>{item.DiaChi}</Text>
                        </Body>
                      </Left>
                    </CardItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-card"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>CMND: {item.SoGT}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="ios-call"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>Điện thoại: {item.SoDT}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    {/* <ListItem icon>
                      <Left>
                      <Icon active name="ios-map" />
                      </Left>
                      <Body>
                      <Text>Địa chỉ: {item.DiaChi}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem> */}

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="md-list"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>Nhóm: {item.TenDT}</Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <ListItem icon>
                      <Left>
                        <Icon
                          active
                          name="logo-usd"
                          style={{ color: "#4286f4" }}
                        />
                      </Left>
                      <Body>
                        <Text>
                          Số tiền: {this.currencyFormat(item.TienTroCap)}
                        </Text>
                      </Body>
                      <Right></Right>
                    </ListItem>

                    <CardItem footer>
                      <Body>
                        <Button
                          success
                          full
                          onPress={() =>
                            navigate("CapPhat", {
                              idnd: item.key,
                              sotien: item.TienTroCap.toString(),
                              hoten: item.HoTen,
                            })
                          }
                        >
                          <Text style={{ color: "white", fontWeight: "bold" }}>
                            Cấp phát
                          </Text>
                        </Button>
                      </Body>
                    </CardItem>
                  </Card>
                );
              } else return null;
            }}
          />
        </Content>
      </Container>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
  },
  title: {
    fontSize: 15,
    color: "#ffff",
    alignSelf: "center",
  },
});
