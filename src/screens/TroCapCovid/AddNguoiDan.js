import React, { Component } from "react";
import {
  Text,
  View,
  StyleSheet,
  Image,
  Alert,
  Platform,
  TouchableOpacity,
  SafeAreaView,
} from "react-native";
import {
  Container,
  Textarea,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Icon,
  Picker,
  Spinner,
  Header,
  Left,
  Right,
  Body,
  Title,
} from "native-base";
import getToken from "../../api/getToken";

export default class AddNguoiDan extends Component {
  // Icon cho footer
  static navigationOptions = {
    header: null,
    // tabBarIcon: ({ focused, tintColor }) => {
    //   return (
    //     <Icon name={"ios-create"} size={45} style={{ color: tintColor }} />
    //   );
    // },
  };

  // constructor
  constructor(props) {
    super(props);
    this.state = {
      iddt: null,
      hoten: null,
      diachi: null,
      iddt: null,
      sodt: null,
      sogt: null,
      iddvhuyen: null,
      iddvxa: null,
      diachi: null,
      tinhtrang: null,
      ghichu: "",
      selected2: null,
      capHuyen: [],
      capXa: [],
      doiTuong: [],
    };
  }

  componentDidMount() {
    this._layDanhSachHuyen();
    this._layDanhSachDoiTuong();
  }

  _submitAddNguoiDan(iddt, hoten, sodt, sogt, iddv, diachi, ghichu) {
    if (
      iddt != null &&
      hoten != null &&
      hoten.length > 0 &&
      sodt != null &&
      sodt.length === 10 &&
      sogt != null &&
      sogt.length >= 9 &&
      iddt != null &&
      diachi != null &&
      diachi.length > 0
    ) {
      getToken("covid")
        .then((token) => {
          if (token.length > 0) {
            var paramsString =
              "token=" +
              token +
              "&iddt=" +
              iddt +
              "&hoten=" +
              hoten +
              "&sodt=" +
              sodt +
              "&sogt=" +
              sogt +
              "&iddv=" +
              iddv +
              "&diachi=" +
              diachi +
              "&ghichu=" +
              ghichu +
              "&toado=";
            fetch(
              "http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/Them_NguoiDan",
              {
                method: "POST",
                headers: {
                  "Content-Type":
                    "application/x-www-form-urlencoded;charset=UTF-8",
                },
                body: paramsString,
              }
            )
              .then((response) => response.json())
              .then((responseJson) => {
                if (responseJson.thongbao === "thanhcong") {
                  // TODO
                  this._resetTextInputs();
                  Alert.alert("Thêm thông tin thành công");
                } else if (responseJson.thongbao === "trung") {
                  // TODO
                  this._resetTextInputs();
                  Alert.alert("Thông tin đã tồn tại trong hệ thống");
                }
              });
          }
        })
        .catch((err) => console.log("LOI", err));
    } else {
      Alert.alert("Hãy điền đầy đủ thông tin");
    }
  }

  _layDanhSachHuyen() {
    getToken("covid")
      .then((token) => {
        if (token.length > 0) {
          var paramsString = "cap=0";
          fetch(
            "http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/DonVi",
            {
              method: "POST",
              headers: {
                "Content-Type":
                  "application/x-www-form-urlencoded;charset=UTF-8",
              },
              body: paramsString,
            }
          )
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({ capHuyen: responseJson });
            });
        }
      })
      .catch((err) => console.log("LOI", err));
  }

  _layDanhSachXa(idHuyen) {
    getToken("covid")
      .then((token) => {
        if (token.length > 0) {
          var paramsString = "cap=" + idHuyen;
          fetch(
            "http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/DonVi",
            {
              method: "POST",
              headers: {
                "Content-Type":
                  "application/x-www-form-urlencoded;charset=UTF-8",
              },
              body: paramsString,
            }
          )
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({ capXa: responseJson });
            });
        }
      })
      .catch((err) => console.log("LOI", err));
  }

  _layDanhSachDoiTuong() {
    getToken("covid")
      .then((token) => {
        if (token.length > 0) {
          var paramsString = "";
          fetch(
            "http://motcua-service.tayninh.gov.vn/ServiceTroCap.asmx/DoiTuong",
            {
              method: "POST",
              headers: {
                "Content-Type":
                  "application/x-www-form-urlencoded;charset=UTF-8",
              },
              body: paramsString,
            }
          )
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({ doiTuong: responseJson });
            });
        }
      })
      .catch((err) => console.log("LOI", err));
  }

  _resetTextInputs() {
    this.setState({
      hoten: null,
      sodt: null,
      sogt: null,
      diachi: null,
      ghichu: "",
    });
  }

  onValueChange2(idXa) {
    this.setState({
      iddvxa: idXa,
    });
  }

  onValueChange3(idDT) {
    this.setState({
      iddt: idDT,
    });
  }

  onValueChange(idHuyen) {
    this.setState({ iddvhuyen: idHuyen });
    this._layDanhSachXa(idHuyen);
  }

  // render
  render() {
    const { navigate } = this.props.navigation;
    if (this.state.capHuyen.length > 0) {
      return (
        <Container>
          <Header style={{ backgroundColor: "#4286f4",height:40 }}>
            <Left style={{ flex: 1 }}></Left>
            <Body style={{ flex: 1 }}>
              <Title style={st.title}>Thêm mới</Title>
            </Body>
            <Right style={{ flex: 1 }}></Right>
          </Header>
          <Content padder>
            <Form
              style={{ paddingLeft: 0, paddingRight: 10, paddingBottom: 10 }}
            >
              <Item>
                <Icon active name='ios-contact' />
                <Input
                  value={this.state.hoten}
                  onChangeText={(text) => this.setState({ hoten: text })}
                  placeholder='Họ và tên'
                />
              </Item>
              <Item>
                <Icon active name='ios-card' />
                <Input
                  keyboardType="numeric"
                  value={this.state.sogt}
                  onChangeText={(text) => this.setState({ sogt: text })}
                  placeholder='CMND/CCCC'
                />
              </Item>
              <Item>
                <Icon active name='ios-call' />
                <Input
                  keyboardType="numeric"
                  value={this.state.sodt}
                  onChangeText={(text) => this.setState({ sodt: text })}
                  placeholder='Điện thoại'
                />
              </Item> 
              <Item>
                <Icon active name='ios-map' />
                <Input
                  value={this.state.diachi}
                  onChangeText={(text) => this.setState({ diachi: text })}
                  placeholder='Số nhà, đường...'
                />
              </Item>
              <Item Picker>
                <Icon active name='ios-home' />
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.iddvhuyen}
                  onValueChange={this.onValueChange.bind(this)}
                >
                  <Picker.Item label="Chọn huyện" value={null} />
                  {this.state.capHuyen.map((item, key) => {
                    return (
                      <Picker.Item
                        label={item.TenDonVi}
                        value={item.DonViID} 
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </Item>
              <Item Picker>
                <Icon active name='md-map' />
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.iddvxa}
                  onValueChange={this.onValueChange2.bind(this)}
                >
                  <Picker.Item label="Chọn xã" value={null} />
                  {this.state.capXa.map((item, key) => {
                    return (
                      <Picker.Item
                        label={item.TenDonVi}
                        value={item.DonViID}
                        key={key}
                      />
                    );
                  })}
                </Picker>
              </Item>

              <Item Picker>
                <Icon active name='md-list' /> 
                <Picker
                  mode="dropdown"
                  iosIcon={<Icon name="arrow-down" />}
                  style={{ width: undefined }}
                  selectedValue={this.state.iddt}
                  onValueChange={this.onValueChange3.bind(this)}
                >
                  <Picker.Item label="Phân loại đối tượng" value={null} />

                  {this.state.doiTuong.map((item, key) => {
                    return (
                      <Picker.Item
                        label={item.TenDT}
                        value={item.ID}
                        key={key}
                      />
                    );
                  })}
                </Picker> 
              </Item>
              <Item>
                <Icon active name='ios-create' /> 
                <Input
                  value={this.state.ghichu}
                  onChangeText={(text) => this.setState({ ghichu: text })}
                  placeholder="Ghi chú"
                />
              </Item>
            </Form>
            <Button
              block
              last
              style={{ color: "#4286f4", width: "50%", alignSelf: "center" }}
              onPress={() =>
                this._submitAddNguoiDan(
                  this.state.iddt,
                  this.state.hoten,
                  this.state.sodt,
                  this.state.sogt,
                  this.state.iddvxa,
                  this.state.diachi,
                  this.state.ghichu
                )
              }
            >
              <Icon name="ios-send" />
              <Text style={{ color: "white" }}>Lưu thông tin</Text>
            </Button>
          </Content>
        </Container>
      );
    } else {
      return (
        <Container>
          <Header style={{ backgroundColor: "#4286f4" }}>
            <Left style={{ flex: 1 }}></Left>
            <Body style={{ flex: 1 }}>
              <Title style={st.title}>Thêm mới</Title>
            </Body>
            <Right style={{ flex: 1 }}></Right>
          </Header>
          <Spinner />
        </Container>
      );
    }
  }
}

// Styles
const st = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ededed",
  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
    marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    padding: 5,
    marginBottom: 5,
    backgroundColor: "white",
  },
  thanh1: {
    backgroundColor: "#3771c3",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingTop: 5,
  },
  icon_menu: {
    width: 30,
    height: 30,
    marginBottom: 5,
    marginRight: 10,
    marginLeft: 5,
  },
  lbtxt: {
    fontSize: 14,
  },
  title: {
    fontSize: 15,
    color: "#ffff",
    alignSelf: "center",
  },
});
