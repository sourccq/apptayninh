import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  Text,
  StatusBar,
  Dimensions,
  ScrollView,
  TouchableHighlight,
  Alert,
  AsyncStorage,
  Platform,
  PermissionsAndroid,
} from "react-native";
import {
  Button,
  Container,
  Header,
  Left,
  Right,
  Icon,
  Grid,
  Col,
  Row,
  Item,
  Input,
  Title,
  Body,
  Picker,
  Spinner,
} from "native-base";
import WebView from "react-native-webview";

const { width: deviceWidth } = Dimensions.get("window");
const { height: deviceHeigh } = Dimensions.get("window");
const imageWidth = deviceWidth;
const imageHeight = deviceHeigh / 4;
const searchHeight = deviceHeigh / 20;
const headerWidth = deviceWidth / 2;

export default class DangKyTroCap extends React.Component {
  constructor(props) {
    //constructor to set default state
    super(props);
  }

  componentDidMount() {
    var that = this;

    if (Platform.OS === "android") {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
              title: "Camera ứng dụng không có quyền",
              message:
                "Ứng dụng cần sử dụng camera, " + "cần đồng ý truy cập camera.",
              buttonPositive: "OK",
            }
          );
        } catch (err) {
          alert("Camera permission err", err);
          console.warn(err);
        }
      }
      requestCameraPermission();
    }
  }

  render() {
    let WebViewRef;
    const uriParam = `http://nophoso.dichvucong.tayninh.gov.vn/NopHoSoZaLo/TroCap?donvi=41&loaihs=15736&linhvuc=10792`;
    return (
      <Container>
        <Header
          hasSegment
          style={{ backgroundColor: "#4286f4", paddingTop: 0.1, height: 40 }} 
        >
          <Left>
            <Icon
              onPress={() => this.props.navigation.navigate("HomeTroCap")}
              name="md-arrow-back"
              style={{ color: "white", fontSize: 40 }}
            />
          </Left>
          <Body>
            <Title
              style={{
                alignSelf: "center",
                color: "white",
                width: headerWidth,
              }}
            >
              Đăng ký trợ cấp
            </Title>
          </Body>
          <Right></Right>
        </Header>

        <Grid>
          {/* Webview */}
          <Row>
            <WebView
              ref={(WEBVIEW_REF) => (WebViewRef = WEBVIEW_REF)}
              source={{ uri: uriParam }}
              allowsInlineMediaPlayback
              startInLoadingState={true}
              renderLoading={() => (
                <Spinner
                  color="#0E4AA3"
                  style={{
                    position: "absolute",
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                />
              )}
            />
          </Row>
        </Grid>
      </Container>
    );
  }
}
