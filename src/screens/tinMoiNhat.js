import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    StatusBar,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Alert,
    AsyncStorage,
    CookieManager
  } from "react-native";
  import { Button,Container,Header,Left,Right,Icon,Grid,Col,Row, Item,Input,Title,Body, Picker,Spinner } from 'native-base';
  import WebView from 'react-native-webview';

  const { width: deviceWidth } = Dimensions.get('window');
  const { height: deviceHeigh } = Dimensions.get('window');
  const imageWidth = deviceWidth ;
  const imageHeight = deviceHeigh/4;
  const searchHeight = deviceHeigh/20;
  const headerWidth = deviceWidth/2;

  export default class Tinmoinhat extends React.Component {
    componentDidMount(){
      const idParam = this.props.navigation.getParam('itemID','');
      this._fetchHtmlContent(idParam);

    }
    constructor(props) {
        //constructor to set default state
        super(props);
        
        this.state = {
          htmlContent:'',
          title:''
        };
        
        
      };
      _fetchHtmlContent(id){
        var bodyParams= `id=${id}`;
        var urlService = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/tintuc';
        fetch(urlService, {
          method: 'POST',
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
            },
            body: bodyParams
        }).then(response => response.json())
        .then(responseData => {
         this.setState({htmlContent:responseData[0].NoiDung})
         this.setState({title:responseData[0].TieuDe})
        })
      }
    
      render(){
        return (
            <Container>
            <Header hasSegment style={{ backgroundColor: "#0E4AA3",paddingTop:0.1, height:40}}>
            <Left>
             <Icon onPress={() => this.props.navigation.navigate('tintucFollow')} name="md-arrow-back" style={{ color: 'white', marginRight: 15, fontSize:35 }} />
            </Left>
            <Body>
              <Title style={{color:'white', alignSelf:'center'}}></Title>
            </Body>
            <Right>
            </Right>
          </Header>
          
            <Grid style={{marginLeft:5,marginRight:5}}> 
                        
                  <Text style={{fontWeight:'bold', textAlign:'justify'}}>{this.state.title}</Text>
                <Row>
                <WebView
                        originWhitelist={['*']}
                        source={{html: this.state.htmlContent}}  
                        startInLoadingState={true}
                        renderLoading={() => <Spinner color='#0E4AA3' style={{
                          position: 'absolute',
                          left: 0,
                          right: 0,
                          top: 0,
                          bottom: 0,
                          alignItems: 'center',
                          justifyContent: 'center'}}/>}
                        ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
                        
                       
                       
                />
                </Row>

            </Grid>


            </Container>
        )

      }
        





  }