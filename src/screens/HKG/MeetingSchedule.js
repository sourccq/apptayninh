import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,SafeAreaView
} from 'react-native';
import { Agenda } from 'react-native-calendars';

const { height } = Dimensions.get('window');

export default class MeetingSchedule extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    this.state = {
      items: {},
      date: new Date(),
      refresh: false,
      dataSource: [],
      idcn: params.idtv
    };
  }
  render() {
	const { navigate } = this.props.navigation;
    return (
		<SafeAreaView style={{flex:1}}>
			<View style={styles.thanh1}>
				<View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1 }}>
				  <TouchableOpacity onPress={() => navigate('HomeHKG')}>
					<Image style={styles.icon_menu} source={require('../../assets/HKG/back.png')} />
				  </TouchableOpacity>
				</View>
        <View style={{marginTop:6,flex:1}}>
					<Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Lịch họp</Text>
				</View>
			</View>
			<Agenda
				onDayPress={this.onDayPress}
				items={this.state.items}         
				loadItemsForMonth={this.loadItems.bind(this)}
				selected={Date.getDate}
				renderItem={this.renderItem.bind(this)}
				renderEmptyDate={this.renderEmptyDate.bind(this)}
        rowHasChanged={this.rowHasChanged.bind(this)}        
				theme={{
				  agendaDayTextColor: '#eb432d',
				  agendaDayNumColor: '#eb432d',
				  agendaTodayColor: '#eb432d',
				  agendaKnobColor: '#eb432d'
				}}
			  
			/>
		</SafeAreaView>
    );
  }
  loadItems(day) {
    setTimeout(() => {
      const url = `https://hkg.tayninh.gov.vn/services/WebService.asmx/getngay?nam=${day.year}&thang=${day.month}&idtv=${this.state.idcn}`;
      console.log(url);
      fetch(url)
        .then((response) => response.json())
        .then((responseJson) => {
          // console.log('Mảng trong fetch');
          // console.log(responseJson);
          // return responseJson;
          this.setState({
            items: responseJson
          });
        });
      setTimeout(() => {
        // console.log('Mang sau khi get');
        // console.log(this.state.dataSource);
        //this.state.items = this.state.dataSource;
        const newItems = {};
        Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
        this.setState({
          items: newItems
        });
        // console.log(newItems);
      }, 2000);
    }, 1000);
    console.log(`Load Items for ${day.year}-${day.month}`);
  }
  
  renderItem(item) {
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity style={styles.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.ID,screen: 'MeetingSchedule' })}>
        <View style={[styles.item,]}>
          <Text onlayout={this.handleTexlayout}>{item.TenCH}</Text>
        </View>
      </TouchableOpacity >
    );
  }

  renderEmptyDate() {
    return (
      <View style={styles.emptyDate}><Text>Không có lịch họp</Text></View>
    );
  }

  rowHasChanged(r1, r2) {
    return r1.name !== r2.name;
  }

  timeToString(time) {
    const date = new Date(time);
    return date.toISOString().split('T')[0];
  }
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17,
  },
  emptyDate: {
    height: 15,
    flex: 1,
    paddingTop: 40
  },
	thanh1: {
		backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
	},
});
