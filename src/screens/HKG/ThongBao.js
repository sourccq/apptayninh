import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    View,
    TouchableOpacity,
    Image,
    TextInput,
    Dimensions,
    Text,
    FlatList,
    Alert,SafeAreaView
} from 'react-native';

const { height } = Dimensions.get('window');

export default class Search extends Component {
  static navigationOptions = {
    header: null
  }
    constructor(props) {
        super(props);
        const { params } = this.props.navigation.state;
        this.state = {
          loading: false,
          data: [],
          page: 0,
          error: null,
          refreshing: false,
          idcn: params.idtv,
          txtSearch: '',
        };
    }
    componentDidMount() {
      this.makeRemoteRequest();
    }
    onSearch() {
      const { txtSearch } = this.state;
      this.makeRemoteRequest();
    }
    makeRemoteRequest = () => {
      const { page, seed } = this.state;
      const url = `https://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=${this.state.idcn}&page=${page}&ngay=0&thang=0&nam=0&loai=2&txts=${this.state.txtSearch}`;
      this.setState({ loading: true });
      fetch(url)
        .then(res => res.json())
        .then(res => {
          this.setState({
            data: page === 0 ? res : [...this.state.data, ...res],
            error: res.error || null,
            loading: false,
            refreshing: false
          });
        })
        .catch(error => {
          this.setState({ error, loading: false });
        });
    };
    handleRefresh = () => {
      this.setState(
        {
          page: 0,
          refreshing: true
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };
  
    handleLoadMore = () => {
      this.setState(
        {
          page: this.state.page + 1
        },
        () => {
          this.makeRemoteRequest();
        }
      );
    };  
    render() {
        const { navigate } = this.props.navigation;
        return (
          <SafeAreaView style={{flex:1}}>
            <View style={styles.container}>
                <View style={styles.wrapper}>
                    <View style={styles.row1}>
                    <TouchableOpacity onPress={() => navigate('HomeHKG')}>
                      <Image style={styles.icon_menu} source={require('../../assets/HKG/back.png')} />
                    </TouchableOpacity>
                    </View>
                    <TextInput
                        style={styles.textInput}
                        placeholder="Nhập tên thông báo"
                        underlineColorAndroid="transparent"
                        value={this.state.txtSearch}
                        onChangeText={text => this.setState({ txtSearch: text })}
                        onSubmitEditing={this.onSearch.bind(this)}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <FlatList
                        onRefresh={this.handleRefresh}
                        refreshing={this.state.refreshing}
                        onEndReached={this.handleLoadMore}
                        onEndReachedThreshold={0.5}
                        data={this.state.data}
                        renderItem={({ item }) =>
                        <TouchableOpacity style={styles.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.key ,screen: 'ThongBao'})}>
                            <View style={styles.bao}>
                                <View style={styles.bn}>
                                    <View style={styles.thu}>
                                        <Text style={styles.textw}>{item.Gio}</Text>
                                        <Text style={styles.textw}>Thứ {item.Thu}</Text>
                                    </View>
                                    <View style={styles.ngay}>
                                        <Text style={styles.textb}>{item.Ngay}</Text>
                                        <Text style={styles.textb}>Tháng {item.Thang}</Text>
                                    </View>
                                    <View style={styles.nam}>
                                        <Text style={styles.textw}>{item.Nam}</Text>
                                    </View>
                                </View>
                                <View style={styles.nd}>
                                    <View style={styles.bnd}>
                                        <Text style={item.TrangThai =='Chờ họp' ? styles.td : item.TrangThai =='Tạm hoãn' ? styles.tdd : styles.tdb}>{item.TenCH}</Text>
                                    </View>
                                    <View style={styles.bct}>
                                        <View style={styles.ct}>
                                            <Image style={styles.icon} source={require('../../assets/HKG/company.png')} />
                                            <Image style={styles.icon} source={require('../../assets/HKG/marker.png')} />
                                            <Image style={styles.icon} source={require('../../assets/HKG/info.png')} />
                                        </View>
                                        <View style={styles.ct}>
                                            <Text style={styles.txct}>{item.DonViToChuc}</Text>
                                            <Text style={styles.txct}>{item.DiaDiem}</Text>
                                            <Text style={styles.txct}>{item.TrangThai}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity> 
                        }
                    />
                </View>
            </View>
        </SafeAreaView>
        );
    }
    
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
  },    
  wrapper: { 
      backgroundColor: 'red', 
      paddingTop: 5, 
      paddingLeft: 5,
      paddingRight:5,
      paddingBottom:5,
      justifyContent: 'space-around',
      flexDirection: 'row'
  },
  row1: {justifyContent: 'space-between' },
  textInput: { 
      height: 25, 
      backgroundColor: '#FFF', 
      paddingLeft: 5,
      paddingVertical: 0,
      flex:1,
      marginLeft:10,
  },
  titleStyle: { color: '#FFF', fontFamily: 'Avenir', fontSize: 20 },
  bao: {
      flexDirection: 'row',
      padding: 5,
      borderBottomWidth: 0.5
    },
    bct: {
      flexDirection: 'row',
    },
    nd: {
      marginLeft: 10,
      flex: 1
    },
    td: {
      fontWeight: 'bold',
      color: 'black'
    },
    td: {
      fontWeight: 'bold',
      color: 'black'
    },
    tdb: {
      fontWeight: 'bold',
      color: 'blue'
    },
    tdd: {
      fontWeight: 'bold',
      color: 'red'
    },
    bn: {
      marginTop: 2
    },
    bnd: {
      minHeight: 30
    },
    thu: {
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      overflow:'hidden',
      height: 40,
      width: 60,
      backgroundColor: 'red',
    },
    ngay: {
      height: 40,
      width: 60,
      backgroundColor: 'white',
    },
    nam: {
      borderBottomLeftRadius: 5,
      borderBottomRightRadius: 5,
      overflow:'hidden',
      height: 20,
      width: 60,
      backgroundColor: 'green',
    },
    textw: {
      textAlign: 'center',
      color: 'white'
    },
    textb: {
      textAlign: 'center',
      color: 'black'
    },
    icon: {
      width: 20,
      height: 20,
      marginTop: 5
    },
    ct: {
      marginTop: 0
    },
    txct: {
      marginLeft: 5,
      marginTop: 10,
      color: 'black',
      fontSize:12,
    },
    icon_menu: {
      width: 30,
      height: 30,
      marginTop: -2
    },
});


AppRegistry.registerComponent('ThongBao', () => ThongBao);