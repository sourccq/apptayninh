import React, { Component } from 'react';
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Image,
	TouchableOpacity,SafeAreaView,  Alert,
} from 'react-native';
import getToken from '../../api/getToken';
export default class HopTrongNgay extends Component {
	static navigationOptions = {
		header: null
	}
  constructor(props) {
		super(props);
		const { params } = this.props.navigation.state;
    this.state = {
      date: new Date(),
	  	idcn: params.idtv,
			loading: false,
			data: [],
			page: 0,
			error: null,
			refreshing: false,
    };
  }
  componentDidMount() {
	getToken('hkg')
    .then(token => {
	  if(token.length > 0){
		this.makeRemoteRequest();
	  }else{
		this.props.navigation.navigate('LoginHKG', {islogin: false})
		// Alert.alert(
		// 	'Thông báo',
		// 	'Bạn cần đăng nhập trước để xem',
		// 	[
		// 	  { text: 'Đồng ý', onPress: () => this.props.navigation.navigate('HomeHKG', {islogin: false}), cancelable: true },
		// 	]
		//   );
	  }
	})
	  
	}
	makeRemoteRequest = () => {
		const { page, seed } = this.state;
		const url = `https://hkg.tayninh.gov.vn/services/WebService.asmx/getmobile?idtv=${this.state.idcn}&page=${this.state.page}&ngay=${this.state.date.getUTCDate()}&thang=${this.state.date.getMonth() + 1}&nam=${this.state.date.getFullYear()}&loai=1&txts=`;
		this.setState({ loading: true });
		fetch(url)
			.then(res => res.json())
			.then(res => {
				this.setState({
					data: page === 0 ? res : [...this.state.data, ...res],
					error: res.error || null,
					loading: false,
					refreshing: false
				});
			})
			.catch(error => {
				this.setState({ error, loading: false });
			});
	};
	handleRefresh = () => {
		this.setState(
			{
				page: 0,
				refreshing: true
			},
			() => {
				this.makeRemoteRequest();
			}
		);
	};

	handleLoadMore = () => {
		this.setState(
			{
				page: this.state.page + 1
			},
			() => {
				this.makeRemoteRequest();
			}
		);
	};  
  render() {
		const { navigate } = this.props.navigation;
		const viewnoitem = (
      <View style={{ alignItems: 'center', marginTop: 20 }}>
      <Text style={{ fontSize: 18, }}>Không có cuộc họp trong ngày</Text>
      </View>
    );
    const viewwithitem = (
      <FlatList
			onRefresh={this.handleRefresh}
			refreshing={this.state.refreshing}
			onEndReached={this.handleLoadMore}
			onEndReachedThreshold={0.5}
			data={this.state.data}
			renderItem={({ item }) =>
			<TouchableOpacity style={st.btnSignInStyle} onPress={() => navigate('DetailMeeting', { idcuochop: item.key, screen: 'DayMeeting' })}>
				<View style={st.bao}>
				<View style={st.bn}>
					<View style={st.thu}>
					<Text style={st.textw}>{item.Gio}</Text>
					<Text style={st.textw}>Thứ {item.Thu}</Text>
					</View>
					<View style={st.ngay}>
					<Text style={st.textb}>{item.Ngay}</Text>
					<Text style={st.textb}>Tháng {item.Thang}</Text>
					</View>
					<View style={st.nam}>
					<Text style={st.textw}>{item.Nam}</Text>
					</View>
				</View>
				<View style={st.nd}>
					<View style={st.bnd}>
					<Text style={item.TrangThai =='Chờ họp' ? st.td : item.TrangThai =='Tạm hoãn' ? st.tdd : st.tdb}>{item.TenCH}</Text>
					</View>
					<View style={st.bct}>
					<View style={st.ct}>
						<Image style={st.icon} source={require('../../assets/HKG/company.png')} />
						<Image style={st.icon} source={require('../../assets/HKG/marker.png')} />
						<Image style={st.icon} source={require('../../assets/HKG/info.png')} />
					</View>
					<View style={st.ct}>
						<Text style={st.txct}>{item.DonViToChuc}</Text>
						<Text style={st.txct}>{item.DiaDiem}</Text>
						<Text style={st.txct}>{item.TrangThai}</Text>
					</View>
					</View>
				</View>
				</View>
			</TouchableOpacity>
			}
		/>
    );
    const mainJSX = this.state.data ? viewwithitem : viewnoitem;
    return (
		<SafeAreaView style={{flex:1}}>
			<View style={st.thanh1}>
				<View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1}}>
				  <TouchableOpacity onPress={() => navigate('HomeHKG')}>
					<Image style={st.icon_menu} source={require('../../assets/HKG/back.png')} />
				  </TouchableOpacity>					
				</View>
				<View style={{marginTop:6,flex:1}}>
					<Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Cuộc họp trong ngày</Text>
				</View>
			</View>
			{mainJSX}
		</SafeAreaView>
    );
  }

}
const st = StyleSheet.create({
  container: {
    flex: 1,
  },
  thanhphai: {
    flexDirection: 'row',
    padding: 0,
    justifyContent: 'flex-end',
  },
  thanhtrai: {

  },
  icon_thanh: {
    width: 30,
    height: 30,
    marginTop: 10,
	marginBottom: 10,
    marginRight: 10,
    marginLeft: 5,
  },
  bao: {
    flexDirection: 'row',
    padding: 5,
    borderBottomWidth: 0.5
  },
  bct: {
    flexDirection: 'row',
  },
  nd: {
    marginLeft: 10,
    flex: 1
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  tdb: {
	fontWeight: 'bold',
	color: 'blue'
  },
  tdd: {
	fontWeight: 'bold',
	color: 'red'
  },
  bn: {
    marginTop: 2
  },
  bnd: {
    minHeight: 30
  },
  thu: {
    borderTopLeftRadius: 5,
		borderTopRightRadius: 5,
		overflow:'hidden',
    height: 40,
    width: 60,
    backgroundColor: 'red',
  },
  ngay: {
    height: 40,
    width: 60,
    backgroundColor: 'white',
  },
  nam: {
    borderBottomLeftRadius: 5,
		borderBottomRightRadius: 5,
		overflow:'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'green',
  },
  textw: {
    textAlign: 'center',
    color: 'white'
  },
  textb: {
    textAlign: 'center',
    color: 'black'
  },
  icon: {
    width: 20,
    height: 20,
    marginTop: 5
  },
  ct: {
    marginTop: 0
  },
  txct: {
		marginLeft: 5,
		marginTop: 10,
		color: 'black',
		fontSize:12,
	},
  icon_menu: {
    width: 30,
    height: 30,
    marginTop: 20,
		marginBottom: 0,
    marginRight: 10,
    marginLeft: 5,
	},
	thanh1: {
		backgroundColor: 'red',
    justifyContent: 'space-between',
		flexDirection: 'row',
		paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
	},
});
