import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
  Linking,SafeAreaView,Platform,
} from 'react-native';
import TabBar from './TabBar';
import {  Container, Header, Content, Card, CardItem, Body, Form, Item, Input, Label, Button, Icon,Left} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import RNCalendarEvents from 'react-native-calendar-events';
import getToken from '../../api/getToken';

const newLocal = true;

export default class DetailMeeting extends Component {
  static navigationOptions = {
    header: null
  }
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
    //console.log(params);
    this.state = {
      key: params.idcuochop,
      screen: params.screen,
      dataHop: [],
      dataDV: [],
      dataFile: [],
      dataYkien:[],
      viewtp: true,
      viewyk: false,
      viewtl: false,
      active: 'ThanhPhan',
      file: null,
      tieude: null,
      noidung:null,
      imageview: null,
      checkstyk: 1,
    };
  }
  getdata = () => {
    const url = `https://hkg.tayninh.gov.vn/services/WebService.asmx/getcthop?idh=${this.state.key}`;
    fetch(url)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataHop: responseJson
        });
        // console.log('___________+++++++____+_+_+_+');
        // console.log(this.state.dataHop[0].TT);
        this.setState({
          checkstyk:this.state.dataHop[0].TT
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
    const urltp = `https://hkg.tayninh.gov.vn/services/WebService.asmx/gettphop?idh=${this.state.key}`;
    fetch(urltp)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataDV: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
    const urltl = `https://hkg.tayninh.gov.vn/services/WebService.asmx/gettlhop?idh=${this.state.key}`;
    fetch(urltl)
      .then((response) => response.json())
      .then((responseJson) => {
        this.state.mang = responseJson;
        this.setState({
          dataFile: responseJson
        });
      })
      .catch((err) => {
        console.log('Loi');
      });
      console.log('chay toi day');
            getToken('hkg').then(token => {
              if(token.length > 0){
                console.log('+++++ data +++');
                console.log(this.state.key);
                var paramsString = "token="+ token + "&idh=" + this.state.key;
                console.log(paramsString);
                var urlson = 'https://hkg.tayninh.gov.vn/Services/WebService.asmx/Get_YKien';
                fetch(urlson, {
                  method: 'POST',
                  headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
                },
                body: paramsString
                })
                .then((response) => response.json())
                .then((responseJson) => {
                  this.state.mang = responseJson;
                  this.setState({
                    dataYKien: responseJson
                  });
                  console.log('+++++ data +++');
                  console.log(responseJson);
                })
                .catch((err) => {
                  console.log('Loi _______________');
                });
              }});
  }
  componentDidMount() {
    this.getdata();
  }
  _BamTP() {
    console.log(this.state.active);
    console.log("___________");
    this.setState({
      viewtp: true,
      viewyk:false,
      viewtl: false,
      active: 'ThanhPhan',
    });
    console.log(this.state.active);
  }
  _BamTL() {
    console.log(this.state.active);
    console.log("___________");
    this.setState({
      viewtp: false,
      viewyk: false,
      viewtl: true,
      active: 'TaiLieu',
    });
    console.log(this.state.active);
  }
  _BamYKien() {
    console.log(this.state.active);
    console.log("___________");
    this.setState({
      active: 'YKien',
      viewtp: false,
      viewtl:false,
      viewyk: true,
    });
    console.log(this.state.active);
  }
  taifile(key) {
    getToken('hkg')
    .then(token => {
      if(token.length > 0){
      var paramsString = "id="+ key +"&token="+ token;
      fetch("https://hkg.tayninh.gov.vn/services/WebService.asmx/CodeFile", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.thongbao == 'dung'){
          var link = 'https://hkg.tayninh.gov.vn/services/WebService.asmx/download?code='+responseJson.code;
          Linking.canOpenURL(link).then(supported => {
            if (supported) {
              Linking.openURL(link);
            } else {
              console.log("Don't know how to open URI: " + link);
            }
          });
        }        
      })
      .catch((err) => {
        console.log('Loi');
      });
    } else {
      Alert.alert(
        'Thông báo',
        'Bạn phải đăng nhập để lấy tài liệu',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }
    })
    .catch(err => console.log('LOI CHECK LOGIN', err));
  }
  taifileYK(key) {
    console.log('Tai file......');
    getToken('hkg')
    .then(token => {
      if(token.length > 0){
      var paramsString = "idykien="+ key +"&token="+ token;
      fetch("https://hkg.tayninh.gov.vn/services/WebService.asmx/CodeFileGY", {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
      })
      .then((response) => response.json())
      .then((responseJson) => {
        if(responseJson.thongbao == 'dung'){
          var link = 'https://hkg.tayninh.gov.vn/services/WebService.asmx/downloadgy?code='+responseJson.code;
          Linking.canOpenURL(link).then(supported => {
            if (supported) {
              Linking.openURL(link);
            } else {
              console.log("Don't know how to open URI: " + link);
            }
          });
        }        
      })
      .catch((err) => {
        console.log('Loi');
      });
    } else {
      Alert.alert(
        'Thông báo',
        'Bạn phải đăng nhập để lấy tài liệu',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      )
    }
    })
    .catch(err => console.log('LOI CHECK LOGIN', err));
  }
  baohop (){
  //   RNCalendarEvents.findEventById('151').then(
  //     id => {
  //        console.log(id)
  //     }
  // );
  var timestar=null;
  var timefn=null;
  if(Platform.OS === "android" ){
    timestar= this.state.dataHop[0].Nam+'-'+this.state.dataHop[0].Thang+'-'+this.state.dataHop[0].Ngay+'T'+this.state.dataHop[0].Gio+':00.000Z';
    timefn= this.state.dataHop[0].NamK+'-'+this.state.dataHop[0].ThangK+'-'+this.state.dataHop[0].NgayK+'T'+this.state.dataHop[0].GioK+':00.000Z';
  }else{
    timestar= this.state.dataHop[0].Nam+'-'+this.state.dataHop[0].Thang+'-'+this.state.dataHop[0].Ngay+'T'+this.state.dataHop[0].Gio+':00.000GMT+7';
    timefn= this.state.dataHop[0].NamK+'-'+this.state.dataHop[0].ThangK+'-'+this.state.dataHop[0].NgayK+'T'+this.state.dataHop[0].GioK+':00.000GMT+7';
  }
    RNCalendarEvents.authorizationStatus();
    RNCalendarEvents.authorizeEventStore();
    setTimeout(() => {
      RNCalendarEvents.saveEvent(this.state.dataHop[0].TenCH, {
      location: this.state.dataHop[0].DDHop + ', '+this.state.dataHop[0].TenDonVi,
      notes: this.state.dataHop[0].NoiDung,
      startDate: timestar,
      endDate: timefn,
      // startDate: this.state.dataHop[0].Nam+'-'+this.state.dataHop[0].Thang+'-'+this.state.dataHop[0].Ngay+'T'+this.state.dataHop[0].Gio+':00.000Z',
      // endDate: this.state.dataHop[0].NamK+'-'+this.state.dataHop[0].ThangK+'-'+this.state.dataHop[0].NgayK+'T'+this.state.dataHop[0].GioK+':00.000Z',
      alarms: [{
        date: -30 // or absolute date - iOS Only
      }]
    })
    .then(id => {
      Alert.alert(
        'Thông báo',
        'Đã lưu vào lịch của bạn',
        [
        {text: 'OK', onPress: () => console.log('OK Pressed' + id)},
        ],
        { cancelable: false }
      )
    })
    .catch(error => {
      Alert.alert(
        'Thông báo',
        'Vui lòng khởi động lại ứng dụng',
        [
        {text: 'OK', onPress: () => console.log('____OK___ '+ error)},
        ],
        { cancelable: false }
      )
    })}, 200);
  }
  pickSingle(cropit, circular=false, mediaType) {
    ImagePicker.openPicker({
      cropping: cropit,
      cropperCircleOverlay: circular,
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      compressVideoPreset: 'MediumQuality',
      includeExif: true,
    }).then(image => {
      this.setState({
        imageview: {uri: image.path, width: image.width, height: image.height, mime: image.mime},
        file: image
      });
      console.log(image);
    }).catch(e => {
      console.log(e);
    });
  }
  handleUploadPhoto = () => {
    const data = new FormData();
    getToken('hkg').then(token => {
      if(token.length > 0){
        data.append('token', token);
        data.append('idhop', this.state.key);
      data.append('tieude', this.state.tieude);
      data.append('noidung', this.state.noidung);
      console.log("______________________________________");
    if(this.state.file != null){
      data.append('filedinhkem', {
      name: Platform.OS === "android" ? this.state.file.path.substring(this.state.file.path.lastIndexOf('/')+1) : this.state.file.filename,
      type:  this.state.file.mime, // or photo.type
      uri:
        Platform.OS === "android" ? this.state.file.path : this.state.file.path.replace("file://", "")
      });
    }
    
    fetch(`https://hkg.tayninh.gov.vn/Services/WebService.asmx/themgopy`, {
    method: 'post',
    body: data
    })
    .then((response) => response.json())
    .then((responseJson) => {
      if(responseJson.trangthai === "thanhcong"){
        Alert.alert(
          'Thông báo',
          'Bạn đã gửi ý kiến thành công',
          [
            { text: 'OK', onPress: () => console.log('OK Gửi thành công') },
          ]
        );
      } else {
        Alert.alert(
          'Thông báo',
          'Có lỗi trong quá trình xử lý',
          [
            { text: 'OK', onPress: () => console.log(responseJson + data.tieude + data.noidung) },
          ]
        );
      }
    })
    }});
    //get lai ý kiến
    getToken('hkg').then(token => {
      if(token.length > 0){
        console.log('+++++ data +++');
        console.log(this.state.key);
        var paramsString = "token="+ token + "&idh=" + this.state.key;
        console.log(paramsString);
        var urlson = 'https://hkg.tayninh.gov.vn/Services/WebService.asmx/Get_YKien';
        fetch(urlson, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
        },
        body: paramsString
        })
        .then((response) => response.json())
        .then((responseJson) => {
         
          this.setState({
            dataYKien: responseJson
          });
          console.log('Đã feachhhhhhhhhhhhhh');
          console.log('+++++ data +++');
          console.log(responseJson);
        })
        .catch((err) => {
          console.log('Loi _______________');
        });
      }});
  };
  renderImage(image) {
    return <Image style={{width: 100, height: 100, resizeMode: 'contain'}} source={image} />
  };
  render() {
    const { navigate } = this.props.navigation;
    const viewthanhphan = (
      <ScrollView>
        <FlatList
          data={this.state.dataDV}
          renderItem={({ item }) =>
            <View style={st.bao}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image style={st.icon} source={require('../../assets/HKG/organizationwf.png')} />
                <Text style={st.txct}>{item.Ten}</Text>
              </View>
            </View>
          }
        />
      </ScrollView>
    );
    
    const viewykien = (
      <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 1, marginTop:2}}>
        <Text style={st.txndsubmit}>Ý kiến đã gửi</Text>
        <ScrollView>
          {this.state.checkstyk === 0 ? <View></View> :
          <View>      
            <Form >
              <Item floatingLabel> 
                <Label style={st.lbtxt}>Tiêu đề</Label>
                <Input value={this.state.tieude} onChangeText={text => this.setState({ tieude: text })}/>
              </Item>
              <Item floatingLabel> 
                <Label style={st.lbtxt}>Nội dung</Label>
                <Input value={this.state.noidung} onChangeText={text => this.setState({ noidung: text })}/>
              </Item>
              <Button iconLeft transparent primary onPress={() => this.pickSingle(false)}>
                <Icon name='camera' />
                <Text style={{fontSize:14}}>{this.state.file !== null ? this.state.file.filename :'Đính kèm file'}</Text>
              </Button> 
              <View style={{flex: 1,
                            justifyContent: 'center',
                            alignItems: 'center'}}>
              {this.state.imageview ? this.renderImage(this.state.imageview) : null}
              </View>
              <Button block last style={st.submit} onPress={() => this.handleUploadPhoto()}>
                <Text style={{color:'white'}}>Gửi ý kiến</Text>
              </Button>
            </Form>
        </View> 
}
          <FlatList
            data={this.state.dataYKien}
            renderItem={({ item }) =>
            <View style={{padding:10}}>           
              <Card>
                <CardItem header bordered>
                <Left>
                <Icon active name="star" style={{fontSize: 15, color: 'red'}}/>
                <Body>
                <Text style={{fontSize: 15, color: 'red', fontWeight: 'bold',}}>{item.TieuDe}</Text>
                </Body>
                </Left>
                  
                </CardItem>
                <CardItem >
                  <Body>
                    <Text>
                    {item.NoiDung}
                    </Text>
                  </Body>
                </CardItem>
                { item.FileTL === '' ? <Text style={{ fontstyle: 'italic'}}> {item.FileTL}</Text> :
                <CardItem footer bordered button onPress={() => this.taifileYK(item.ID)}>
                <Icon active name="attach" style={{fontSize: 20, color: 'red'}}/>
                  <Text style={{ fontstyle: 'italic'}}> {item.FileTL}</Text>
                </CardItem>
                }
              </Card>
            </View>
            }
          />
        </ScrollView>
        </View>
      </View>
      // <TabBar bgNavBar="white" bgNavBarSelector="white" stroke="skyblue">
      //       <TabBar.Item
      //           icon={require('../../assets/HKG/fileformatpdf.png')}
      //           selectedIcon={require('../../assets/HKG/fileformatpdf.png')}
      //           title="Tab1"
      //           screenBackgroundColor={{ backgroundColor: '#008080' }}
      //       >
      //         <View>
      //             {/*Page Content*/}
      //         </View>
      //       </TabBar.Item>
      //       <TabBar.Item
      //           icon={require('../../assets/HKG/fileformatpdf.png')}
      //           selectedIcon={require('../../assets/HKG/fileformatpdf.png')}
      //           title="Tab2"
      //           screenBackgroundColor={{ backgroundColor: '#F08080' }}
      //       >
      //           <View>
      //               {/*Page Content*/}
      //           </View>
      //       </TabBar.Item>


      //     </TabBar>
    );
    const viewtailieu = (
      <ScrollView>
      <FlatList
        data={this.state.dataFile}
        renderItem={({ item }) =>
          <View style={st.bao}>
            <TouchableOpacity onPress={this.taifile.bind(this,item.ID)}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image style={st.icon} source={require('../../assets/HKG/fileformatpdf.png')} />
                <Text style={st.txct}>{item.FileTL}</Text>
              </View>
            </TouchableOpacity>
          </View>
        }
      />
    </ScrollView>
    );
    const mainJSX = this.state.viewtp ? viewthanhphan : this.state.viewyk ? viewykien: viewtailieu;
    return (
      <SafeAreaView style={{flex:1}}>
        <View style={st.thanh1}>
          <View style={{ width: 35, height: 35, justifyContent: 'center',zIndex:1 }}>
            <TouchableOpacity onPress={() => navigate(this.state.screen)}>
            <Image style={st.icon_menu} source={require('../../assets/HKG/back.png')} />
            </TouchableOpacity>
          </View>
          <View style={{marginTop:6,flex:1}}>
            <Text style={{textAlign:'center',fontSize:16,color:'white',fontWeight:'bold',marginLeft:-35,zIndex:0}}>Chi tiết cuộc họp</Text>
          </View>
        </View>
        <View style={st.container}>
          <View style={st.ngoai}>
            <View>
            { this.state.dataHop.map((item, key)=>(
                <View style={st.bao}>
                  <View style={st.bt}>
                    <View style={st.bn}>
                      <View style={st.thu}>
                        <Text style={st.textw}>Thứ {item.Thu}</Text>
                      </View>
                      <View style={st.ngay}>
                        <Text style={st.textb}>{item.Ngay}</Text>
                        <Text style={st.textb}>Tháng {item.Thang}</Text>
                      </View>
                      <View style={st.nam}>
                        <Text style={st.textw}>{item.Nam}</Text>
                      </View>
                    </View>
                    <View style={st.nd}>
                      <View style={st.bnd}>
                        <Text style={st.td}>{item.TenCH}</Text>
                      </View>
                    </View>
                  </View>
                  <View style={st.bct}>
                    <View style={st.ct}>
                      <View style={st.ct2}>
                        <View style={st.ct3}>
                          <Text style={st.txct}>Giờ họp: {item.Gio}</Text>
                          <Text style={st.txct}>Loại hình họp: {item.TenLHH}</Text>
                        </View>
                        <View style={st.ct3}>
                          <Text style={st.txct}>Lĩnh vực họp: {item.TenLV}</Text>
                          <Text style={st.txct}>Hình thức họp: {item.TenHT}</Text>
                        </View>
                      </View>
                      <View>
                        <Text style={st.txct}>Địa điểm họp: {item.DDHop}</Text>
                        <Text style={st.txct}>Trạng thái: {item.TrangThai}</Text>
                        <Text style={st.txnd}>Nội dung cuộc họp</Text>
                        <Text style={st.txct}>{item.NoiDung}</Text>
                      </View>
                      <View style={st.ct4}>
                        <TouchableOpacity style={st.tou} onPress={this._BamTP.bind(this)}>
                          <Text style={[st.txna, (this.state.active === 'TaiLieu' ? st.activetxna : this.state.active === 'YKien'? st.activetxna:{})]}>Thành phần</Text>
                        </TouchableOpacity >
                        <TouchableOpacity style={st.tou} onPress={this._BamTL.bind(this)}>
                          <Text style={[st.txna, (this.state.active === 'ThanhPhan' ? st.activetxna : this.state.active === 'YKien'? st.activetxna:{})]}>Tài liệu</Text>
                        </TouchableOpacity >
                        <TouchableOpacity style={st.tou} onPress={this._BamYKien.bind(this)}>
                          <Text style={[st.txna, (this.state.active === 'ThanhPhan' ? st.activetxna : this.state.active === 'TaiLieu'? st.activetxna:{})]}>Ý Kiến</Text>
                        </TouchableOpacity >
                      </View>
                    </View>
                  </View>
                </View>)
            )}
            </View>
            {mainJSX}       
          </View>
        </View>
        <TouchableOpacity style={st.btluuh} onPress={this.baohop.bind(this)}>
          <Image style={st.icon_alarm} source={require('../../assets/HKG/bell-icon.png')} />
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}

const st = StyleSheet.create({
  container: {
    flex: 1,
  },
  icon: {
    width: 25,
    height: 25
  },
  bao: {
    flexDirection: 'column',
    padding: 5,
    borderBottomWidth: 0.5
  },
  bct: {
    flexDirection: 'row',
  },
  bt: {
    flexDirection: 'row',
  },
  nd: {
    flex:1,
    marginLeft: 10,
  },
  td: {
    fontWeight: 'bold',
    color: 'black'
  },
  bn: {
    marginTop: 2
  },
  thu: {
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    overflow:'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'red',
  },
  ngay: {
    height: 40,
    width: 60,
    backgroundColor: 'white',
  },
  nam: {
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    overflow:'hidden',
    height: 20,
    width: 60,
    backgroundColor: 'green',
  },
  textw: {
    textAlign: 'center',
    color: 'white'
  },
  textb: {
    textAlign: 'center',
    color: 'black'
  },
  ct: {
    marginTop: 0,
    flexDirection: 'column',
    flex: 1
  },
  ct2: {
    flexDirection: 'row',

  },
  ct3: {
    flex: 2,
  },
  ct4: {
    flexDirection: 'row',
  },
  txct: {
    marginLeft: 5,
    marginTop: 5,
    color: 'black',
    fontSize:10
  },
  txnd: {
    marginTop: 5,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 5,
    fontWeight: 'bold',
    color: 'black',
    backgroundColor: '#80ceff'
  },
  ngoai: {
    flexDirection: 'column',
    flex: 1
  },
  txna: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    backgroundColor: '#fb1c1c',
    marginLeft: 1,
    marginRight: 1,
  },
  activetxna: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#ff9688',
    marginLeft: 1,
    marginRight: 1,
  },
  txnn: {
    textAlign: 'center',
    padding: 5,
    color: 'white',
    flex: 1,
    backgroundColor: '#ec6060',
    marginLeft: 1,
    marginRight: 1,
  },
  tou: {
    flex: 1,
    marginTop: 5,
  },
  thanh1: {
		backgroundColor: 'red',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingTop:5,
	},
	icon_menu: {
		width: 30,
		height: 30,
		marginBottom: 5,
		marginRight: 10,
		marginLeft: 5,
  },
  icon_alarm:{
    height: 55,
    width: 55,
  },
  btluuh: {
    alignItems: 'center',
    position: 'absolute',
    right: 5,
    bottom: 5,
  },
  submit:{
    marginRight:50,
    marginLeft:50,
    paddingTop:10,
    paddingBottom:10,
    backgroundColor:'#ff0000',
    borderRadius:30,
    borderWidth: 1,
    borderColor: '#fff',
  },
  submitText:{
      color:'#fff',
      textAlign:'center',
  },
  txndsubmit: {
    marginTop:8,
    paddingTop: 2,
    paddingBottom: 2,
    paddingLeft: 5,
    fontWeight: 'bold',
    color: 'black',
    backgroundColor: '#80ceff'
  },
});
