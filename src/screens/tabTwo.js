import React, { Component } from 'react';
import { View,FlatList,RefreshControl} from 'react-native';
import { Container, Icon,Spinner, Content, List, ListItem, Thumbnail, Text, Left, Body, Right, Button } from 'native-base';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';
class tabTwo extends Component {
  constructor(props) {
    super(props);
    this.page = 1;
    this.state = {
      loading: false, // user list loading
      isRefreshing: false, //for pull to refresh
      data: [], //user list
      error: ''
    }
  }
  componentWillMount() {
    this.fetchUser() //Method for API call
  }
  fetchUser() {
    //stackexchange User API url
     const url = `http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/getdonvi?capdo=2&capcha=0`;
     this.setState({ loading: true })
     fetch(url)
     .then(res => res.json())
     .then(res => {
        let data = res
        this.setState({ loading: false, data: data })
     }).catch(error => {
         this.setState({ loading: false, error: 'Something just went wrong' })
       });
   };
   
  onRefresh() {
    console.log("refresssssssssssssssssssss");
    this.setState({ isRefreshing: true }); // true isRefreshing flag for enable pull to refresh indicator
    const url = `http://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/getdonvi?capdo=2&capcha=0`;
    fetch(url)
     .then(res => res.json())
     .then(res => {
        let data = res;
        this.setState({  isRefreshing: false, data: data });
     }).catch(error => {
         this.setState({  isRefreshing: false, error: 'Something just went wrong' })
       });
       console.log(this.state.data);
  };

   handleLoadMore = () => {
    // if (!this.state.loading) {
    //   this.page = this.page + 1; // increase page by 1
    //   this.fetchUser(); // method for API call 
    // }
  };
  // renderSeparator = () => {
  //   return (
  //     <View
  //       style={{
  //         height: 232,
  //         width: '100%',
  //         backgroundColor: '#CED0CE'
  //       }}
  //     />
  //   );
  // };
  renderFooter = () => {
    //it will show indicator at the bottom of the list when data is loading otherwise it returns null
     if (!this.state.loading) return null;
     return (
      <Spinner color='blue' />
     );
   };
  render() {
    if (this.state.loading) {
      return (
      <Container>
      <Spinner color='blue' />
      </Container>
      );
    }
    return (
      <Container>
        <Content refreshControl={
            <RefreshControl
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh.bind(this)}
            />
          }>
        <FlatList
          data={this.state.data}
          extraData={this.state}
          // refreshing={this.state.isRefreshing}
          // onRefresh={this.onRefresh}
          
          renderItem={({ item }) => (
            <TouchableOpacity onPress={() => {
              /* 1. Navigate to the Details route with params */
              this.props.navigation.navigate('ListLoaiHoSo', {
                DonViID: item.DonViID,
                TenDonVi: item.TenDonVi,
                CapChaID: item.CapChaID,
              });
            }}>
            <List>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require("../assets/logotayninh.png")} />
              </Left>
              <Body>
                <Text>{item.TenDonVi}</Text>
                <Text note numberOfLines={1}></Text>
              </Body>
              <Right>
              <Button transparent onPress={() => {
              /* 1. Navigate to the Details route with params */
              this.props.navigation.navigate('ListLoaiHoSo', {
                DonViID: item.DonViID,
                TenDonVi: item.TenDonVi,
                CapChaID: item.CapChaID,
              });
            }}>
              <Icon name="arrow-forward" />
                </Button>
              </Right>
            </ListItem>
          </List>
          </TouchableOpacity>
          )}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={this.renderSeparator}
          ListFooterComponent={this.renderFooter.bind(this)}
          onEndReachedThreshold={0.4}
          onEndReached={this.handleLoadMore.bind(this)}
        />
        </Content>
      </Container>
    );
  }
}
export default withNavigation(tabTwo);