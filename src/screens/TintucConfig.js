import React, { Component } from "react";
import { Dimensions, Alert,Image} from "react-native";
import { Container, Header, Left, Body, Right, Button, Icon, Title, Segment, Content, Text,Card, CardItem, View,CheckBox,ListItem} from 'native-base'
import { TouchableOpacity } from "react-native-gesture-handler";
import AsyncStorage from '@react-native-community/async-storage';



  const { width: deviceWidth } = Dimensions.get('window');
  const { height: deviceHeigh } = Dimensions.get('window');
  const imageWidth = deviceWidth ;
  const imageHeight = deviceHeigh/4;
  const titleWidth = deviceWidth*0.8;
  const publishDateWidth = deviceWidth*0.10;
  const headerWidth = deviceWidth/2;
  export default class TintucConfig extends React.Component {
   

    componentWillMount = () => {
      this.fetchDonviData()
      this._getDsTinDangKy()
      
     
    };
    fetchDonviData(){
      serviceUrl = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/dvbaiviet';
      fetch(serviceUrl, {
        method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
      }).then(response => response.json())
      .then(responseData => {
        this.setState({nguonTinData:responseData}); 
      })
    }

    _getDsTinDangKy = async () => {
      try {
       await AsyncStorage.getItem('dsTinDangKy')
       .then(values => {
          this.setState({nguonTinDangKy:JSON.parse(values)}) 
       })
       console.log('==================================')
        console.log('LAY DU LIEU THANH CONG')

      } catch (error) {
        console.log('==================================')
        console.log('KHONG LAY DUOC DU LIEU' + error)
      }
    };


    _storeDsTinDangKy = async () => {
      try {
        await AsyncStorage.setItem('dsTinDangKy', JSON.stringify(this.state.nguonTinDangKy));
        console.log('==================================')
        console.log('LUU ASYNC THANH CONG')
      } catch (error) {
        console.log('==================================')
        console.log('KHONG LUU DUOC')
      }
    }

    constructor(props) {
      super(props);
      this.state = {
        nguonTinData:[],
        nguonTinDangKy:[],
      }

    };
    pushNguonTinDangKy(id){
      console.log('Checked')
      const array = this.state.nguonTinDangKy
      if(!array.includes(id)){
      array.push(id)
      this.setState({nguonTinDangKy:array})
      console.log('==================================')
      console.log('PUSH THANH CONG')
      }
    }

    xoaNguonTinDangKy(id){
      console.log('Uncheck')
      const array = this.state.nguonTinDangKy
      if(array.includes(id)){
      array.splice(array.indexOf(id),1)
      this.setState({nguonTinDangKy:array})
      console.log('==================================')
      console.log('XOA THANH CONG')
      }
      
    }
    onSaveAndGoBack(){
      if(this.state.nguonTinDangKy.length>0){
      this._storeDsTinDangKy()
      this.props.navigation.state.params.reloadFromConfig()
      //this.props.navigation.navigate('tintucFollow'); 
      this.props.navigation.navigate('tintucFollow')
    
    }else Alert.alert('Vui lòng chọn ít nhất một nguồn tin')
  }

    render() {
      return (
        <Container>
        {/* Header */}
        <Header hasSegment style={{ backgroundColor: "#0E4AA3",paddingTop:0.1,
    height:40 }}>

            <Left>
             <Icon onPress={() => this.props.navigation.navigate('tintucFollow')} name="md-arrow-back" style={{ color: 'white' }} />
            </Left>
            <Body>
              <Title style={{color:'white',alignSelf:'center'}}>Chọn nguồn cấp tin</Title>
            </Body>
            <Right>
            </Right>
          </Header>


          {/* RENDER CHECK BOX */}
        <Content padder>
          {this.state.nguonTinData.map((item,index) => {
            let checkedValue = false;
            if(this.state.nguonTinDangKy.includes(item.ID)){
             checkedValue=true
            }
            return (
          <CheckBoxNews key={index} name={item.TenDonVi} isChecked={checkedValue} 
            onChecked={()=> this.pushNguonTinDangKy(item.ID)} 
            onUncheck={()=> this.xoaNguonTinDangKy(item.ID)}
          />
          )
          })}
            
          
           </Content> 
           <Button block 
            onPress={() => {
              console.log('-------------------------BEGIN LOG------------------------------------')
              //console.log('===========nguonTinData==============>>>  ' + this.state.nguonTinData)
             // console.log('===========nguonTinDangKy==============>>> ' + this.state.nguonTinDangKy)
              this.onSaveAndGoBack()
              console.log('----------------------END LOG------------------------------------------')
              
              
              
              
              
              
              }}>
              <Text style={{fontWeight:'bold', color:'white',fontFamily:'roboto'}}>ĐỒNG Ý</Text>
            </Button>
        </Container>
        
      );
    }
  
    
    
   
  }
// ===============================Check Box Components =================================//

  class CheckBoxNews extends React.Component {

    componentDidMount(){
      this.setState({check:this.props.isChecked})
    }
    componentDidUpdate(){
      //console.log(this.state.check)
      //this.submitParentOnPress()
    }
    constructor(props) {
      super(props);
      this.state = {
      check:false,
      }
      
    };

    toggleCheck(){
      this.setState({check : !this.state.check})
      this.submitParentOnPress()
      }
    
    submitParentOnPress(){
      if(!this.state.check){
        this.props.onChecked();
      }else{
        this.props.onUncheck()
      }
    }
    
   
    
    render(){
      return(
        
          <ListItem>
            <CheckBox 
            checked={this.state.check}
            onPress={() => {this.toggleCheck()}}
            color='blue'
            isChecked={this.props.isChecked}       
            />
            <Body>
              <Text>{this.props.name}</Text>
            </Body>
          </ListItem>


          
        
      )
    }
  }



  