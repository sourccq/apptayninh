import React, { Component } from 'react';
import {NavigationActions} from 'react-navigation';
import {View, StyleSheet, ImageBackground,Image} from 'react-native';
import { Container, Content, ListItem, Text, Icon, Left, Body, Right,Button } from 'native-base';
import drawerCover from '../assets/drawerCover.jpg'
import logotayninh from '../assets/logo-tayninh2.png'



export default class drawerContentComponents extends Component {
//   navigateToScreen = (route) => () => {
//     const navigateAction = NavigationActions.navigate({
//       routeName: route
//     });
//     this.props.navigation.dispatch(navigateAction);
//   }


  render() {
    return (
        <Container style={styles.container}>   
             
                <ImageBackground source={drawerCover} style={styles.drawerImageBg}>
                    <Image source={logotayninh} style={styles.drawerLogo} >
                    </Image>
                </ImageBackground>

                <Content>
                    <ListItem icon onPress={() => this.props.navigation.navigate('Home')}>
                        <Left>
                            <Button style={{ backgroundColor: "#FF9501" }}>
                                <Icon  name="md-home" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Trang chủ</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>

                    <ListItem icon onPress={() => this.props.navigation.navigate('BarcodeScan')}>
                        <Left>
                        <Button style={{ backgroundColor: "#007AFF" }}>
                            <Icon active name="search" />
                        </Button>
                        </Left>
                        <Body>
                        <Text>Tra cứu hồ sơ</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
                    <ListItem icon onPress={() => this.props.navigation.navigate('EgovPreLoad')}>
                        <Left>
                        <Button style={{ backgroundColor: "#007AFF" }}>
                            <Icon active name="moon" />
                        </Button>
                        </Left>
                        <Body>
                        <Text>Bkav Egov</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
                    <ListItem icon onPress={() => this.props.navigation.navigate('tintucNavigator')}>
                        <Left>
                        <Button style={{ backgroundColor: "#007AFF" }}>
                            <Icon active name="md-paper" />
                        </Button>
                        </Left>
                        <Body>
                        <Text>Tin tức sự kiện</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
                    <ListItem icon onPress={() => this.props.navigation.navigate('HoiDap')}>
                        <Left>
                        <Button style={{ backgroundColor: "#007AFF" }}>
                            <Icon active name="md-people" />
                        </Button>
                        </Left>
                        <Body>
                        <Text>Hỏi đáp</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
                    <ListItem icon onPress={() => this.props.navigation.navigate('HomeHKG')}>
                        <Left>
                        <Button style={{ backgroundColor: "#007AFF" }}>
                            <Icon active name="ios-briefcase" />
                        </Button>
                        </Left>
                        <Body>
                        <Text>Họp không giấy</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
                    <ListItem icon onPress={() => this.props.navigation.navigate('HomeKTXH')}>
                        <Left>
                        <Button style={{ backgroundColor: "#007AFF"}}>
                            <Icon active name="ios-trending-up" />
                        </Button>
                        </Left>
                        <Body>
                        <Text>Kinh tế xã hội</Text>
                        </Body>
                        <Right>
                        </Right>
                    </ListItem>
            </Content>
        </Container>
    )
  }
}

const styles = StyleSheet.create({
    drawerImageBg: {
        height: 180,
        alignItems:'center',
        paddingTop:35,
        width:'100%',
        
    },
    container:{
       
      
    },
    
    drawerLogo:{
        width: 140,
        height:140
    }
   
   
    
    
    
});