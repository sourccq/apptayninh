import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    StatusBar,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Alert,
    WebView,
    ImageBackground,
    YellowBox
  } from "react-native";
  import { Button,Container,Header,Left,Right,Icon,Grid,Col,Row, Item,Input,Title,Body,Label, Content, Tab, Tabs, ScrollableTab ,Card, CardItem,Spinner,TabHeading} from 'native-base';
import { SafeAreaView } from "react-navigation";
import { TouchableOpacity } from "react-native-gesture-handler";
const TintucDetail = React.lazy(() => import('./TintucDetail')); 
import AsyncStorage from '@react-native-community/async-storage';


  export default class tintucFollow extends React.Component {

    componentDidMount(){
      this.fetchNguonTinData();
      this._getDsTinDangKy();
    };

    fetchNguonTinData = async () =>{
      this.setState({loading:true})
      serviceUrl = 'https://motcua-service.tayninh.gov.vn/WebServiceZalo.asmx/dvbaiviet';
      try {
      await fetch(serviceUrl, {
        method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
          },
      }).then(response => response.json())
      .then(responseData => {
        this.setState({nguonTinData:responseData,loading:false}); 
      })
    }catch(error){
      console.log('==================================')
      console.log('KHONG FETCH DUOC DU LIEU' + error)
    }
  }

    constructor(props) {
      //constructor to set default state
      super(props);
      this.tabIndex = 0
      this.state = {
        nguonTinDangKy:[],
        initialPage: 0, 
        activeTab: 0,
        nguonTinData:[],
        loading:false
      };
    }
    _getDsTinDangKy = async () => {
      this.setState({loading:true})
      try {
       await AsyncStorage.getItem('dsTinDangKy')
       .then(values => {
          this.setState({nguonTinDangKy:JSON.parse(values),loading:false}) 
       })
       console.log('==================================')
        console.log('LAY DU LIEU THANH CONG')

      } catch (error) {
        console.log('==================================')
        console.log('KHONG LAY DUOC DU LIEU' + error)
      }
    };

    reloadFromConfig(){
      console.log('------------------------------------')
      console.log('GOBACK FROM CONFIG')
    }

    // renderTabTin = async () => {
    //   try{
    //     await this.state.nguonTinData.map((item) => {
    //       if(this.state.nguonTinDangKy.includes(item.ID)){
    //        return(
    //        <Tab heading={item.TenDonVi} key={item.ID}>
    //        <React.Suspense fallback={<Spinner color='#0E4AA3'/>}>
    //          <TintucDetail ID={item.ID}/>
    //        </React.Suspense>

    //        </Tab> 
    //        )
    //        }
    //      })
    //   }catch(e){
    //     console.log('ERROR WHEN RENDER TAB TIN TUC')
    //   }
    // }


    render(){
      if(this.state.nguonTinDangKy.length > 0 && this.state.nguonTinData.length>0){
      return(
        <Container>
        <Header hasTabs style={styles.header}>
          <Left>  
              <Icon  onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', fontSize:30}} />
          </Left>
          <Body>
          <Title style={{alignSelf:'center',color:'white'}}></Title>
          </Body>
          <Right><Icon  onPress={() => this.props.navigation.navigate('TintucConfig',{reloadFromConfig:() => this._getDsTinDangKy()})} name="md-settings" style={{ color: 'white', fontSize:30}} /></Right>
        </Header>
        {/* TODO ALL */}
        
        <Tabs renderTabBar={()=> <ScrollableTab /> }>
          
          <Tab heading='Tin Tổng Hợp'>
                 
              <React.Suspense fallback={<Spinner color='#0E4AA3'/>}>
              <TintucDetail ID={this.state.nguonTinDangKy.toString()}/>
              </React.Suspense>
  
          </Tab>     
           {    
            this.state.nguonTinData.map((item) => {
           if(this.state.nguonTinDangKy.includes(item.ID)){
           return(
            <Tab heading={item.TenDonVi} key={item.ID}>
            <React.Suspense fallback={<Spinner color='#0E4AA3'/>}>
              <TintucDetail ID={item.ID}/>
            </React.Suspense>

            </Tab> 
            )
            }
          })
            } 
        </Tabs>
        
        </Container>
       
      );

        }
        return(
          <Container>
          <Header hasTabs style={styles.header}>
          <Left>  
              <Icon  onPress={() => this.props.navigation.navigate('Home')} name="md-home" style={{ color: 'white', fontSize:30}} />
          </Left>
          <Body></Body>
          <Right><Icon  onPress={() => this.props.navigation.navigate('TintucConfig',{reloadFromConfig:() => this._getDsTinDangKy()})} name="md-settings" style={{ color: 'white', fontSize:30}} /></Right>
          </Header>
          <Content>
          <Spinner color='#0E4AA3' style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            alignItems: 'center',
            justifyContent: 'center'}}/> 
          </Content>
            
          </Container> 
        )
    }
  
  
  }
    

    
    const styles = StyleSheet.create({
   
      header:{
        backgroundColor: "#0E4AA3",
        height:40,
        justifyContent: 'center',
        paddingTop:0.1   
      },
      
    });