import React, { Component } from "react";
import {
    View,
    StyleSheet,
    Image,
    Text,
    StatusBar,
    Dimensions,
    ScrollView,
    TouchableHighlight,
    Alert,
    AsyncStorage,
    Platform,
    PermissionsAndroid
  } from "react-native";
  import { Button,Container,Header,Left,Right,Icon,Grid,Col,Row, Item,Input,Title,Body, Picker,Spinner } from 'native-base';
  import WebView from 'react-native-webview';

  const { width: deviceWidth } = Dimensions.get('window');
  const { height: deviceHeigh } = Dimensions.get('window');
  const imageWidth = deviceWidth ;
  const imageHeight = deviceHeigh/4;
  const searchHeight = deviceHeigh/20;
  const headerWidth = deviceWidth/2;

  export default class ViewNopHoSo extends React.Component {
   
    constructor(props) {
      //constructor to set default state
      super(props);
      
      // this.state = {
      //   uri:uriParam
      // };
      //this._retrieveData();
      
    }

    componentDidMount(){
      var that = this;

    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA, {
              'title': 'Camera ứng dụng không có quyền',
              'message': 'Ứng dụng cần sử dụng camera, '+
              'cần đồng ý truy cập camera.',
              buttonPositive: 'OK',
            }
          )
          // {
          //   Alert.alert(
          //       'Thông báo',
          //       'Không có quyền truy cập camera',
          //       [
          //       //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
          //         {
          //           text: 'Thoát',
          //           onPress: () => console.log('Cancel Pressed'),
          //           style: 'cancel',
          //         },
          //         {text: 'OK', onPress: () => console.log('OK Pressed')},
          //       ],
          //       {cancelable: false},
          //     );
          // }
        } catch (err) {
          alert("Camera permission err", err);
          console.warn(err);
        }
      }  
      requestCameraPermission();
    } 
    }
    
  //   componentDidMount() {
  //     if (Platform.OS === 'android') {
  //       async function requestCameraPermission() {
  //         try {
  //           const granted = awaits PermissionsAndroid.request(
  //             PermissionsAndroid.PERMISSIONS.CAMERA, {
  //               'title': 'Camera ứng dụng không có quyền',
  //               'message': 'Ứng dụng cần sử dụng camera, '+
  //               'cần đồng ý truy cập camera.',
  //               buttonPositive: 'OK',
  //             }
  //           )
  //           if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //             that.setState({ QR_Code_Value: '' });
  //             that.setState({ Start_Scanner: true });
  //           } else {
  //             Alert.alert(
  //                 'Thông báo',
  //                 'Không có quyền truy cập camera',
  //                 [
  //                 //   {text: 'Ask me later', onPress: () => console.log('Ask me later pressed')},
  //                   {
  //                     text: 'Thoát',
  //                     onPress: () => console.log('Cancel Pressed'),
  //                     style: 'cancel',
  //                   },
  //                   {text: 'OK', onPress: () => console.log('OK Pressed')},
  //                 ],
  //                 {cancelable: false},
  //               );
  //           }
  //         } catch (err) {
  //           alert("Camera permission err", err);
  //           console.warn(err);
  //         }
  //       }  
  //       requestCameraPermission();
  //   }
  // }

    _deleteData = async () => {
      try {
        //this.setState({uri:''});
        await AsyncStorage.removeItem('egovUri');
         //this.setState({uri:''});
      } catch (error) {
         //Error retrieving data
      }
    };
    

    render() {
      let WebViewRef;
      const LoaiHoSoID = this.props.navigation.getParam('LoaiHoSoID');
      const LinhVucID = this.props.navigation.getParam('LinhVucID');
      const DonVi = this.props.navigation.getParam('DonVi');
      const uriParam = `http://nophoso.dichvucong.tayninh.gov.vn/NopHoSoZaLo/CreateAppTayNinh?donvi=${DonVi}&loaihs=${LoaiHoSoID}&linhvuc=${LinhVucID}`;
      console.log(uriParam);
      return (
        <Container>

        <Header hasSegment style={{ backgroundColor: "#0E4AA3",paddingTop:0.1,
    height:40 }}>
              <Left>
             <Icon onPress={() => this.props.navigation.goBack()} name="md-arrow-back" style={{ color: 'white', fontSize:40 }} />
            </Left>
            <Body>
              <Title style={{alignSelf:'center',color:'white', width:headerWidth}}>Nộp hồ sơ</Title>
            </Body>
            <Right>
            </Right>
          </Header>
  
          <Grid>
          {/* Webview */}
          <Row>
          <WebView
                ref={WEBVIEW_REF => (WebViewRef = WEBVIEW_REF)}
                source={{uri: uriParam}} 
                allowsInlineMediaPlayback
                startInLoadingState={true}
                        renderLoading={() => <Spinner color='#0E4AA3' style={{
                          position: 'absolute',
                          left: 0,
                          right: 0,
                          top: 0,
                          bottom: 0,
                          alignItems: 'center',
                          justifyContent: 'center'}}/>} 
                
          />
          </Row>
          </Grid>
        </Container>
        
      );
    }
  }

  