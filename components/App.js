/* eslint-disable object-shorthand */
/* eslint-disable import/no-named-as-default-member */
/* eslint-disable import/no-named-as-default */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
/* eslint-disable react/no-multi-comp */
/* eslint-disable react/prefer-stateless-function */
// eslint-disable-next-line no-unused-vars
import React, { Component } from "react";
import { View, StatusBar, Platform } from "react-native";
import {
  createDrawerNavigator,
  createAppContainer,
  createStackNavigator,
  createSwitchNavigator,
  createBottomTabNavigator,
} from "react-navigation";
import {
  Button,
  Container,
  Header,
  Left,
  Right,
  Icon,
  Text,
} from "native-base";
import { Root } from "native-base";
import Home from "../src/screens/Home";
import drawerComponents from "../src/screens/drawerComponents";
import egov from "../src/screens/egov";
import egovConfig from "../src/screens/egovConfig";
import EgovPreLoad from "../src/screens/EgovPreLoad";
import Tinmoinhat from "../src/screens/tinMoiNhat";
import BarcodeScan from "../src/screens/BarcodeScan";
import tintucFollow from "../src/screens/tintucFollow";
import TintucDetail from "../src/screens/TintucDetail";
import TintucConfig from "../src/screens/TintucConfig";
import NopHoSo from "../src/screens/NopHoSo";
import ListLoaiHoSo from "../src/screens/ListLoaiHoSo";
import tabOne from "../src/screens/tabOne";
import ListHoSo from "../src/screens/ListHoSo";
import ViewNopHoSo from "../src/screens/ViewNopHoSo";
//--------------KTXH-----------------
import HomeKTXH from "../src/screens/KTXH/HomeKTXH";
import LoginKTXH from "../src/screens/KTXH/LoginKTXH";
import ChangePasswordKTXH from "../src/screens/KTXH/ChangePasswordKTXH";
import ThongKe from "../src/screens/KTXH/ThongKe";
import ThongKeDP from "../src/screens/KTXH/ThongKeDP";
import ToanTinh from "../src/screens/KTXH/ToanTinh";
import DuyetCT from "../src/screens/KTXH/DuyetCT";
import DSBaoCao from "../src/screens/KTXH/DSBaoCao";
import ThongKeTre from "../src/screens/KTXH/ThongKeTre";
//-----------------HKG-------------
import HomeHKG from "../src/screens/HKG/HomeHKG";
import ChangePasswordHKG from "../src/screens/HKG/ChangePasswordHKG";
import DayMeeting from "../src/screens/HKG/DayMeeting";
import LoginHKG from "../src/screens/HKG/LoginHKG";
import MeetingSchedule from "../src/screens/HKG/MeetingSchedule";
import DetailMeeting from "../src/screens/HKG/DetailMeeting";
import Search from "../src/screens/HKG/Search";
import ThongBao from "../src/screens/HKG/ThongBao";
import NewMeeting from "../src/screens/HKG/NewMeeting";
import LichCongTac from "../src/screens/HKG/LichCongTac";
import CheckHome from "../src/screens/HKG/CheckHome";
import LayYKien from "../src/screens/HKG/LayYKien";
import ChuDeTraoDoi from "../src/screens/HKG/ChuDeTraoDoi";
import BinhLuan from "../src/screens/HKG/BinhLuan";
//-------------------Hoi dap---------
import HoiDap from "../src/screens/HoiDap/HoiDap";
import TraLoi from "../src/screens/HoiDap/TraLoi";
import DatCauHoi from "../src/screens/HoiDap/DatCauHoi";
// ---------------- Tro cap Covid ----------//
import LoginTroCap from "../src/screens/TroCapCovid/LoginTroCap";
import AddNguoiDan from "../src/screens/TroCapCovid/AddNguoiDan";
import CheckLogin from "../src/screens/TroCapCovid/CheckLogin";
import DanhSachChoTroCap from "../src/screens/TroCapCovid/DanhSachChoTroCap";
import DanhSachDaTroCap from "../src/screens/TroCapCovid/DanhSachDaTroCap";
import CapPhat from "../src/screens/TroCapCovid/CapPhat";
import TimKiem from "../src/screens/TroCapCovid/Timkiem";
import TimKiemDaCap from "../src/screens/TroCapCovid/DanhSachDaTroCap";
import HomeTroCap from "../src/screens/TroCapCovid/HomeTroCap";
import DangKyTroCap from "../src/screens/TroCapCovid/DangKyTroCap";
import CongKhaiDaCap from "../src/screens/TroCapCovid/CongKhaiDaCap";
import TraCuuCongDan from "../src/screens/TroCapCovid/TraCuuCongDan";
import HuongDanSuDung from "../src/screens/TroCapCovid/HuongDanSuDung";
//End of MyNotificationsScreen class

const HomeStack = createStackNavigator(
  {
    Home: Home,
    BarcodeScan: BarcodeScan,
  },
  {
    initialRouteName: "Home",
    defaultNavigationOptions: {
      header: null,
    },
  }
);

const TrangHoiDap = createStackNavigator(
  {
    HoiDap: HoiDap,
    TraLoi: TraLoi,
    DatCauHoi: DatCauHoi,
  },
  {
    initialRouteName: "HoiDap",
  }
);
const KTXH = createStackNavigator(
  {
    HomeKTXH: HomeKTXH,
    LoginKTXH: LoginKTXH,
    ChangePasswordKTXH: ChangePasswordKTXH,
    ThongKe: ThongKe,
    ThongKeDP: ThongKeDP,
    ToanTinh: ToanTinh,
    DuyetCT: DuyetCT,
    DSBaoCao: DSBaoCao,
    ThongKeTre: ThongKeTre,
  },
  {
    initialRouteName: "HomeKTXH",
    defaultNavigationOptions: {
      header: null,
    },
  }
);
const HKGHOME = createStackNavigator(
  {
    HomeHKG: HomeHKG,
    ChangePasswordHKG: ChangePasswordHKG,
    DayMeeting: DayMeeting,
    MeetingSchedule: MeetingSchedule,
    DetailMeeting: DetailMeeting,
    Search: Search,
    ThongBao: ThongBao,
    NewMeeting: NewMeeting,
    LichCongTac: LichCongTac,
    LayYKien: LayYKien,
    ChuDeTraoDoi: ChuDeTraoDoi,
    BinhLuan: BinhLuan,
  },
  {
    initialRouteName: "HomeHKG",
  }
);

const HKG = createSwitchNavigator(
  {
    HKGHOME: HKGHOME,
    CheckHome: CheckHome,

    LoginHKG: LoginHKG,
  },
  {
    initialRouteName: "CheckHome",
  }
);

const eGovNavigator = createSwitchNavigator(
  {
    EgovPreLoad: EgovPreLoad,
    egovConfig: egovConfig,
    egov: egov,
  },
  {
    initialRouteName: "EgovPreLoad",
    defaultNavigationOptions: {
      header: null,
    },
  }
);

const tintucNavigator = createStackNavigator(
  {
    Tinmoinhat: Tinmoinhat,
    tintucFollow: tintucFollow,
    TintucDetail: TintucDetail,
    TintucConfig: TintucConfig,
  },
  {
    initialRouteName: "tintucFollow",
    defaultNavigationOptions: {
      header: null,
    },
  }
);
const NopHoSoNavigator = createStackNavigator(
  {
    NopHoSo: NopHoSo,
    ListLoaiHoSo: ListLoaiHoSo,
    ListHoSo: ListHoSo,
    ViewNopHoSo: ViewNopHoSo,
  },
  {
    initialRouteName: "NopHoSo",
    defaultNavigationOptions: {
      header: null,
    },
  }
);

//  Tro cap Covid 19

const capPhatNavigator = createStackNavigator(
  {
    DanhSachChoTroCap: DanhSachChoTroCap,
    CapPhat: CapPhat,
    TimKiem:TimKiem,
    TimKiemDaCap:TimKiemDaCap
    
  },
  {
    initialRouteName: "DanhSachChoTroCap",
  }
);

const troCapCovidNavigator = createBottomTabNavigator(
  {
    "Chờ cấp": capPhatNavigator,
    // "Thêm mới": AddNguoiDan,
    "Đã cấp": DanhSachDaTroCap,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === "Chờ cấp") {
          iconName = `md-exit`;
        // } else if (routeName === "Thêm mới") {
        //   iconName = `md-person-add`;
        } else if (routeName === "Đã cấp") {
          iconName = `md-checkbox`;
        }
        return <Icon name={iconName} style={{ color: tintColor, fontSize:28}} />;
      },
      tabBarOptions: {
        style: {
          backgroundColor: "#4286f4",
          height: 50
        },
        iconStyle: {
          ...Platform.select({
            ios: {
              height: 35,
              marginBottom: 20,
            },
            android: {
              height: 35,
            },
          }),
        },
        labelStyle: {   
          fontSize: 12,
          fontWeight:"bold"
        },
        activeTintColor: "white",
        inactiveTintColor: "#92C6E9", 
        showLabel: true,
        showIcon: true,
      },
      
    }),
  },
  {
    initialRouteName: "Chờ cấp",
  }
);

const congDanNavigator = createStackNavigator(
  {
    HomeTroCap:HomeTroCap,
    DangKyTroCap:DangKyTroCap,
    CongKhaiDaCap:CongKhaiDaCap,
    TraCuuCongDan:TraCuuCongDan,
    HuongDanSuDung:HuongDanSuDung
    
  },
  {
    initialRouteName: "HomeTroCap", 
    defaultNavigationOptions: {
      header: null,
    },
  }
);

const troCapCovidSwitch = createSwitchNavigator(
  {
    troCapCovidNavigator: troCapCovidNavigator,
    Login: LoginTroCap,
    CheckLogin: CheckLogin,
    congDanNavigator:congDanNavigator
  },
  {
    initialRouteName: "congDanNavigator",
  }
);

const MyDrawerNavigator = createDrawerNavigator(
  {
    //---------------KTXH-----------
    Home: {
      screen: HomeStack,
    },

    KTXH: { 
      screen: KTXH,
    },
    //-------HKG-------
    HKG: {
      screen: HKG,
    },
    //---------Hoi Dap-------
    TrangHoiDap: {
      screen: TrangHoiDap,
    },

    ListLoaiHoSo: {
      screen: ListLoaiHoSo,
    },
    tabOne: {
      screen: tabOne,
    },
    NopHoSoNavigator: {
      screen: NopHoSoNavigator,
    },
    tintucNavigator: {
      screen: tintucNavigator,
    },
    eGovNavigator: {
      screen: eGovNavigator,
    },
    // Tro cap covid
    troCapCovidNavigator: {
      screen: troCapCovidSwitch,
    },
  },

  {
    contentComponent: drawerComponents,
  }
);

const MyApp = createAppContainer(MyDrawerNavigator);

class App extends React.Component {
  render() {
    return (
      <Root>
        <Container>
          <MyApp></MyApp>
          <StatusBar hidden={true} />
        </Container>
      </Root>
    );
  }
} //End of App class

export default App;
